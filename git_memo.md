# Documentation Git pour le projet Smart Mirror
Bienvenue dans le guide Git pour collaborer sur le projet Smart Mirror. Ce document fournit les instructions essentielles pour cloner le dépôt, créer des branches conformément à nos conventions, soumettre des commits et créer des Merge Requests. Suivez attentivement ces directives pour assurer une collaboration efficace et organisée.

## Cloner le dépôt
Pour commencer, clonez le dépôt du projet Smart Mirror en exécutant la commande suivante dans votre terminal :

```bash
git clone https://gitlab.com/apajak/smart-mirror.git
```
Cela créera une copie locale du dépôt sur votre machine.

## Création de branches
Lorsque vous travaillez sur une nouvelle fonctionnalité ou correction, créez une nouvelle branche en suivant notre convention de nommage :

**Fonctionnalités** : feat/[nom-de-la-feature]

**Corrections de bugs**: fix/[nom-du-fix]

Par exemple, pour une fonctionnalité nommée "voice-control", créez votre branche ainsi :

```bash
git checkout -b feat/voice-control
```

## Commit des changements
Utilisez les formats suivants pour vos messages de commit, selon le type de changement effectué :

**Ajouts** : [add]{nom-de-l'ajout}
**Suppressions** : [remove]{élément-supprimé}
**Mises à jour** : [update]{élément-mis-à-jour}

Exemple de commit ajoutant une nouvelle fonctionnalité (depuis le dossier où se trouvent les fichiers modifiés):

```bash
git add .
```
```bash
git commit -m "[add]{reconnaissance vocale}"
```
## Merge Requests
Une fois vos changements prêts, poussez votre branche vers le dépôt distant et créez une Merge Request (MR) :

**Poussez votre branche** : 
```bash
git push origin feat/voice-control.
```

Sur GitLab, naviguez vers le dépôt ["smart-mirror"]https://gitlab.com/apajak/smart-mirror, puis dans l'onglet "*Merge Requests*", cliquez sur "*New merge request*".
Sélectionnez votre branche comme source et main (ou la branche appropriée) comme cible.
Donnez un titre explicatif à votre MR, ajoutez une description si nécessaire.

**Important** : Dans la section "*Assignees*", ajoutez votre nom pour indiquer que vous êtes l'auteur de la MR. Dans la section "Reviewers", ajoutez mon nom pour me désigner comme relecteur.

Soumettez votre MR.

## Bonnes pratiques
- **Mise à jour de votre branche** : Avant de créer votre MR, assurez-vous que votre branche est à jour avec la branche principale (main). Cela peut être fait en fusionnant main dans votre branche ou en rebase votre branche sur main.
- **Revues de code** : Prenez au sérieux le processus de revue de code. Appliquez les modifications suggérées par les relecteurs si elles améliorent la qualité du code.
En suivant ces directives, nous nous assurons que le travail sur le projet Smart Mirror est organisé, traçable et de haute qualité. Merci de contribuer au succès de notre projet!