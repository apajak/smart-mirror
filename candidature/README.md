# Candidature Ydays Event

## Sommaire
1. [Storytelling](#storytelling)
2. [Objectifs](#objectifs)
3. [Membres](#membres)
4. [Déroulé](#déroulé)
5. [Etat d'avancement](#etat-davancement)
6. [Démonstrations](#démonstration)
7. [Et demain](#et-demain)
8. [Technologies](#technologies)
9. [Liens](#liens)

## Storytelling
Il y a quelque temps, un ami et moi avons eu une discussion sur les miroirs connectés et les nouvelles technologies. Nous avons réalisé qu'il y avait une opportunité de créer quelque chose de vraiment innovant en combinant ces deux domaines. C'est ainsi que l'idée de smart_mirror est née.

Après avoir rejoint Ynov l'année dernière, j'ai commencé à acquérir les compétences nécessaires pour réaliser notre vision. J'ai travaillé dur pour apprendre la programmation et la robotique, tout en cherchant des partenaires pour m'aider à réaliser ce projet.

Finalement, grace au Ydays Ynov j'ai pu rassembler une équipe d'étudiants talentueux issus de différentes filières, tels que la robotique et l'informatique. Ensemble, nous avons commencé à travailler sur la conception de smart_mirror.

Depuis le début de l'année, nous avons travaillé d'arrache-pied pour donner vie à notre projet. Nous avons passé des heures à travailler sur la conception du miroir, à programmer les fonctions de reconnaissance vocale et gestuelle, et à intégrer les différentes technologies d'IA.

Notre travail acharné a porté ses fruits et nous serions ravis de présenter smart_mirror aux Ydays Events à Paris en mai prochain. Nous sommes convaincus que notre miroir connecté apportera une valeur ajoutée réelle aux consommateurs, en leur offrant des fonctionnalités innovantes et pratiques pour leur vie quotidienne.

## Objectifs

L'objectif de smart_mirror est de proposer une expérience de miroir connecté unique en son genre, combinant différentes technologies d'intelligence artificielle pour permettre à l'utilisateur de se reconnaître, d'essayer virtuellement des tenues et du maquillage, de consulter son agenda, les conditions météorologiques et de la musique, tout en offrant un contrôle vocal et gestuel. Le miroir s'adapte également à la taille de l'utilisateur pour un confort optimal. L'objectif est de fournir une solution innovante et pratique pour la routine quotidienne des utilisateurs.

L'objectif final est d'avoir un prototype fonctionnel qui peut être utilisé par les utilisateurs finaux. Nous avons déjà réalisé une grande partie du travail.

## Membres

|     Equipe    | Mécatronique & Électronique |   IA Vocale  |     IA Tracking    |        UI       |        3D       |
|:-------------:|:---------------------------:|:------------:|:------------------:|:---------------:|:---------------:|
| chef d'équipe |         KALI Mathieu        | DURAND Erwan |   RODET Baptiste   | CHAMOINRI Ifuja | PAJAK Alexandre |
|       1       |       BOUFFANAIS Hugo       |              |  ROULLAND Roxanne  |  LATORRE Audran |                 |
|       2       |        TORIBIO Alexis       |              | LAROUMANIE Gabriel |                 |                 |
|       3       |        MUSTIÈRE Tommy       |              |                    |                 |                 |


Étudiant Robotique:
- CHAMOINRI Ifuja (Master 2)
- BOUFFANAIS Hugo (Bachelor 3)
- KALI Mathieu (Master 1)
- MUSTIÈRE Tommy (Master 1)

Étudiant Informatique:
- ROULLAND Roxanne (Bachelor 2)
- DURAND Erwan (Bachelor 2)
- RODET Baptiste (Master 1)
- TORIBIO Alexis (Bachelor 2)
- LATORRE Audran (Bachelor 2)
- LAROUMANIE Gabriel (Bachelor 2)
- PAJAK Alexandre (Bachelor 2)

A Paris seront présents les cinq chefs d'équipe.

## Déroulé

Le déroulé d'une journée de travail est le suivant:

- 9h00 - 09h30:
    Réunion d'équipe pour discuter des tâches effectué en dehors des heures de Ydays. Des taches à accomplir a cours terme ainsi que les différents problèmes rencontrés.

- 09h30 - 13h00:
    Travail sur les différentes taches à accomplir. Les chefs d'équipe sont là pour aider les membres de l'équipe à résoudre les problèmes rencontrés.

- 13h00 - 14h00:
    Pause déjeuner

- 14h00 - 17h30:
    Travail sur les différentes taches à accomplir. Les chefs d'équipe sont là pour aider les membres de l'équipe à résoudre les problèmes rencontrés.

- 17h30 - 18h00:
    Réunion d'équipe pour discuter des avancées de la journée et des taches à accomplir pour la journée suivante.

## Etat d'avancement

Aujourd'hui, nous avons déjà réalisé une grande partie du travail. 

Concernant L'intelligence artificielle, nous avons déjà développé:
- Un système de reconnaissance vocale
- Un système de reconnaissance gestuelle
- Un système de reconnaissance faciale
- Un système de détection du corps humain et de suivi des mouvements

Il nous reste donc a terminer le développement des IA de reconnaissance vocale et de detection des mouvements du visage.

Concernant le miroir, nous avons déjà réalisé:
- La modélisation 3D (CAO) du miroir
- La conception Electronique

Il nous reste donc a terminer: 
- la conception mécanique de quelques pièces interne du miroir.
- l'usinage des pièces (a pris du retard en raison des delais de livraison de l'acier)
- l'assemblage du miroir

Concernant l'interface utilisateur, nous avons déjà réalisé:
- La conception graphique de l'interface utilisateur
- Le developpement de l'interface utilisateur

Il nous reste donc a terminer:
- l'intégration de l'interface utilisateur avec les différentes IA
- l'intgration de Unity (vêtements 3D) dans l'interface 

## Démonstration

### Visuel
- Miroir:
[miroir3D](./movies/miroir.mp4)
- Model3D:
[model3D](./image/model3D.png)
- Interface:
[interface](./movies/Video_ui.mp4)
- Détection du corps:
[detection](./movies/bodyDetection.mp4)
- Détection des mouvements:
[mouvements](./movies/video_hand_gesture.mp4)
- Etude de marché:
[etudeDeMarche](./file/%C3%89tudeDeMarch%C3%A9_SM.pdf)

## Et demain

Je ne souhaite pas necessairement continuer à travailler sur ce projet dans le cadre des Ydays.
Je souhaiterais eventuellement continuer à travailler sur ce projet dans le cadre de l'entreprenariat.


## Technologies

Materiel:
- Raspberry Pi 4
- TV led
- ArduCam HD
- Moteur pas à pas
- Capteur de proximité
- Capteur de luminosité

Logiciel:
- Python 3.8
- Unity 2019.4.1f1
- C#
- OpenCV
- Tensorflow
- Keras
- PyAudio
- vosk

## Liens

- [Étude De Marché](./file/%C3%89tudeDeMarch%C3%A9_SM.pdf)
- [Gitlab du projet](https://gitlab.com/apajak/smart-mirror)

- [Trello](https://trello.com/b/H08yDUVB/smart-mirror)

