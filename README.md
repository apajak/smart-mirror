# Projet SMART MIRROR
-------------
![pics](./pics/Logo_SmartMirror_2.png)
-------------
➜ Ici Nous stockeront l'integralitée des avanncées du projet.

## 1. Le Projet 
Le SM@RT-Mirror a pour objectif d’ajouté de la connectivité dans un lieu où certain d’entre nous passe beaucoup de temps. Ce miroir de salle de bain, connecté, permettra de faciliter la difficile tâche du maquillage, grâce à des tutoriels interactifs directement sur votre reflet ! Mais aussi l’essayage de vêtements, coiffures, lunettes ou boucles d’oreilles. En plus de cela vous pourrez retrouver les informations qui vous plaisent telles que la météo ou votre calendrier du jour directement dans votre miroir !

[➜ Pour plus de détails](./files/Projet_Sm@rt-Mirror.pdf) <br/>
[➜ Le Trello](https://trello.com/invite/b/H08yDUVB/ATTId2eb5a2b5cbcf635c829260fe870ba1bB719EAC9/smart-mirror)

## 2. SOMMAIRE
- [Le Projet](#1-le-projet)
- [sommaire](#2-sommaire)
- [Prérequis](#3-prerequis)
- [Mécatronique](#4-mécatronique)
- [Eléctronique](#5-eléctronique)
- [Programmation UI](#6-programmation-ui)
- [Programmation Tracking IA](#7-programmation-tracking)
- [Programmation IA Vocale](#8-programmation-ia-vocale)
- [Modèles & Annimations 3D](#9-modèles-et-annimations-3d)

## 3. Prerequis

Avant de commancé a codé en Python pensez a créer un environement virtuel ça évitera les bugs de OpenCV. (qui est deja assez instable). <br/>
[make Python3 .env](https://docs.python.org/3/library/venv.html)

Pour ceux que ça interessera de faire de la "computer vision" pendant ces Ydays, installez Opencv sur vos machines on gagnera du temps. <br/>
[install-Opencv-Windows](https://docs.opencv.org/4.x/d3/d52/tutorial_windows_install.html) <br/>
[install-Opencv-Mac](https://docs.opencv.org/4.x/d0/db2/tutorial_macos_install.html)<br/>
[install-Opencv-Linux](https://docs.opencv.org/4.x/d7/d9f/tutorial_linux_install.html)<br/>

Pour test si votre install est bonne:
```python
(base) paja@paja-mac ~ % python3 
Python 3.9.7 (default, Sep 16 2021, 08:50:36) 
[Clang 10.0.0 ] :: Anaconda, Inc. on darwin
Type "help", "copyright", "credits" or "license" for more information.
>>> import cv2
>>> print(cv2.__version__)
4.6.0
>>> 
```

## groupes:

| Mécatronique 	| IA Vocale 	| IA Tracking 	| UI     	| 3D        	|
|--------------	|-----------	|-------------	|--------	|-----------	|
| Alexis       	| Erwan     	| Baptiste    	| Ifuja  	| Alexandre 	|
| Hugo         	| Tommy     	| Roxane      	| Audran 	|           	|
|              	|           	| Gabriel     	|        	|           	|


## 4. Mécatronique
### Le repo: https://gitlab.com/apajak/smart-mirror-mecatronique.git
## 5. Eléctronique
### Le repo: https://gitlab.com/apajak/smart-mirror-electronique.git
## 6. Programmation UI

[➜ Récupération de la Météo avec OpenWeather](./weather/main.py)

### Le repo: https://gitlab.com/apajak/smart-mirror-ui.git
## 7. Programmation Tracking
### Le repo: https://gitlab.com/apajak/smart-mirror-computervision.git
## 8. Programmation IA Vocale
### Le repo: https://gitlab.com/apajak/smart-mirror-vocal_ia.git
## 9. Modèles et Annimations 3D
### Le repo: https://gitlab.com/apajak/smart-mirror-3d.git
