# Imports python lib
import os.path

import cv2
import threading
import time

# Imports local features
from src.ai.gesture_recognition.hand_detection import HandRecognizer
from src.ai.face_recognition.faceRecognition import FaceRecognition
from src.user import user_recognition

user_loop: bool = True
timer: bool = False


def start_timer():
    """Start a 10 sec timer in thread

    Args :
        None

    Returns :
        None
    """

    def thread_function():
        global timer
        global user_loop
        timer = True
        time.sleep(10)
        timer = False
        user_loop = False

    thread = threading.Thread(target=thread_function)
    thread.start()


if __name__ == "__main__":
    hd = HandRecognizer()
    fr = FaceRecognition()
    user = None
    registered_or_known = False
    start_timer()

    cap = cv2.VideoCapture(0)
    while True:
        success, frame = cap.read()

        if success:
            if registered_or_known:
                if user_loop:
                    user, user_loop = user_recognition(frame, fr, user, timer)
                    continue

                if user is not None:
                    register_or_known = True
                    print(f"Welcome {user['name']} to Smart Mirror Experience")
                else:
                    print(f'Welcome to Smart Mirror Experience')

            # User detected or does not want to register, the loop keeps going
            # TO DO

        cv2.imshow("App test window", frame)

        if cv2.waitKey(1) & 0xFF == ord('q'):
            cap.release()
            cv2.destroyAllWindows()
            break
