from datetime import datetime

import socketio
import eventlet
import os
from src.api.weather.weather import Weather as weather
import json
from src.api.agenda.Google import GoogleCalendar
from src.sensor.luminosity_sensor_manager_test import LuminositySensor
from src.raspberry_ia.network_controller import WiFiController
from src.ai.face_recognition import faceRecognition as fr
import threading
from aiohttp import web
from threading import Lock
import time
import asyncio


# Fetch the port number from environment variable or use default
PORT = int(os.environ.get('PORT', 3000))

# create a Socket.IO server
sio = socketio.AsyncServer(cors_allowed_origins='*') # Allow all origins for simplicity

# create a WSGI application
app = web.Application()
sio.attach(app)
Weather = weather()
calendar = GoogleCalendar('./api/agenda/client_secret.json')
wifi_controller = WiFiController()

# storage vars
last_luminosity = None
clients = {}

# global vars
auto_brightness = False
network_enabled = True
default_weather_city = "Bordeaux"

#AI
face_recognition = fr.FaceRecognition()

# define sensors
luminosity_sensor = LuminositySensor()  # get value in lux with capteur.readLight()


# event handler for the "connect" event
@sio.event
async def connect(sid, environ):
    print(f'connect {sid}')


# event handler for the "disconnect" event
@sio.event
async def disconnect(sid):
    if sid in clients:
        print(f"Client {clients[sid]} disconnected with sid {sid}")
        del clients[sid]


# event handler for the "message" event
@sio.event
async def message(sid, data):
    print(f'message from {sid}: {data}')
    await sio.emit('response', data)


@sio.event
async def identify_client(sid, client_type):
    clients[sid] = client_type
    print(f"Client {client_type} identified with sid {sid}")


@sio.event
async def weather_request(sid, city_name):
    try:
        # currentWeather test
        if city_name == "":
            city_name = default_weather_city
        current_weather = Weather.get_CurrentWeather(city_name)
        await sio.emit('weather_response', current_weather.to_json(), to=sid)
        print(f"Envoi des données météo au client {sid}")
    except Exception as e:
        print(f"Erreur lors de la récupération des données météo: {e}")
        # Envoyez un message d'erreur au client
        await sio.emit('weather_error', {'error': 'Impossible de récupérer les données météo.'}, to=sid)


@sio.event
async def five_days_weather_request(sid, city_name):
    try:
        # currentWeather test
        if city_name == "":
            city_name = default_weather_city
        current_weather = Weather.get_CurrentWeather(city_name)
        current_forecast = {
            "time": "Now",  # Current time
            "temp": current_weather.get_temp(),
            "icon": current_weather.get_icon(),
            "city": current_weather.get_city_name(),
        }
        five_days_weather = Weather.get_FiveDayForecast(city_name).get_next_five_hours_forecast()

        # Format the response correctly
        five_days_forecast = {
            "current": current_forecast,
            "forecast": five_days_weather  # Directly use the list here
        }

        # Convert the entire dictionary to JSON
        five_days_forecast_json = json.dumps(five_days_forecast)
        await sio.emit('weather_five_days_response', five_days_forecast_json, to=sid)
        print(f"Envoi des données météo au client {sid}")
    except Exception as e:
        print(f"Erreur lors de la récupération des données météo: {e}")
        # Envoyez un message d'erreur au client
        await sio.emit('weather_error', {'error': 'Impossible de récupérer les données météo.'}, to=sid)


@sio.event
async def get_calandar_events(sid):
    try:
        events = calendar.get_events()
        await sio.emit('calendar_response', json.dumps(events), to=sid)
        print(f"Envoi des données de l'agenda au client {sid}")
    except Exception as e:
        print(f"Erreur lors de la récupération des données de l'agenda: {e}")
        # Envoyez un message d'erreur au client
        await sio.emit('calendar_error', {'error': 'Impossible de récupérer les données de l\'agenda.'}, to=sid)


## Brigthness management ##
@sio.event
async def get_luminosity(sid):
    print(f"Requesting luminosity for session {sid}")
    system_client_sid = next((s for s, t in clients.items() if t == 'system'), None)
    if system_client_sid:
        await sio.emit('request_current_brightness', room=system_client_sid)
        print(f"Forwarded luminosity request to system client {system_client_sid}")
    else:
        print("System client is not connected or not identified")
        await sio.emit('luminosity_data', {'error': 'System client not available'}, room=sid)


@sio.event
async def current_brightness(sid, data):
    print(f"Received brightness data from system client: {sid}")
    unity_client_sid = next((s for s, t in clients.items() if t == 'unity'), None)
    if unity_client_sid:
        # Forward the brightness data to the Unity client
        print(f"data: {data}")
        await sio.emit('luminosity_data', {'brightness': data}, room=unity_client_sid)
        print(f"Sent brightness data to Unity client {unity_client_sid}")

@sio.event
async def set_luminosity(sid, value):
    global auto_brightness
    print(f"Set manually the brightness request from: {sid}")
    print(f"Value: {value}")
    system_client_sid = next((s for s, t in clients.items() if t == 'system'), None)
    if value >= 0 or value <= 1:
        auto_brightness = False
        if system_client_sid:
            await sio.emit('set_brightness', value, room=system_client_sid)
            print(f"Forwarded manual brightness setting to system client {system_client_sid}")
        else:
            print("System client is not connected or not identified")
            await sio.emit('luminosity_data', {'error': 'System client not available'}, room=sid)
    else:
        print("Invalid brightness value, must be between 0 and 1")
        await sio.emit('luminosity_data', {'error': 'Invalid brightness value'}, room=sid)


@sio.event
async def set_auto_luminosity(sid, value):
    global auto_brightness
    auto_brightness = value
    print(f"Auto-brightness set to {'on' if value else 'off'} by client {sid}")
    if value:
        threading.Timer(10, lambda: asyncio.run(monitor_luminosity())).start()

@sio.event
async def get_auto_luminosity(sid):
    global auto_brightness
    print(f"auto_brightness: {auto_brightness}")
    unity_client_sid = next((s for s, t in clients.items() if t == 'unity'), None)
    if unity_client_sid:
        await sio.emit('auto_brightness', {'auto_brightness': auto_brightness}, room=unity_client_sid)
        print(f"Sent auto brightness status to Unity client {unity_client_sid}")
    else:
        print("Unity client not found")


async def monitor_luminosity():
    global last_luminosity, auto_brightness
    system_client_sid = next((s for s, t in clients.items() if t == 'system'), None)
    current_luminosity = luminosity_sensor.readLight()
    print(f"Current luminosity: {current_luminosity}"
          f"\nLast luminosity: {last_luminosity}"
          f"\nAuto brightness: {auto_brightness}")
    if last_luminosity is None or abs(current_luminosity - last_luminosity) / last_luminosity > 0.2 and auto_brightness:
        last_luminosity = current_luminosity
        print(f"Current luminosity: {current_luminosity}")
        await sio.emit('lux_data', current_luminosity, room=system_client_sid)
        threading.Timer(10, lambda: asyncio.run(monitor_luminosity())).start()


## Audio management ##
@sio.event
async def set_volume(sid, value):
    system_client_sid = next((s for s, t in clients.items() if t == 'system'), None)
    if system_client_sid:
        await sio.emit('set_volume', value, room=system_client_sid)
        print(f"Forwarded volume setting to system client {system_client_sid}")
    else:
        print("System client is not connected or not identified")
        await sio.emit('luminosity_data', {'error': 'System client not available'}, room=sid)

@sio.event
async def get_volume(sid):
    print(f"Requesting volume for session {sid}")
    system_client_sid = next((s for s, t in clients.items() if t == 'system'), None)
    if system_client_sid:
        await sio.emit('get_volume', room=system_client_sid)
        print(f"Forwarded volume request to system client {system_client_sid}")
    else:
        print("System client is not connected or not identified")
        await sio.emit('volume_data', {'error': 'System client not available'}, room=sid)

@sio.event
async def current_volume(sid, data):
    print(f"Received volume data from system client: {sid}")
    unity_client_sid = next((s for s, t in clients.items() if t == 'unity'), None)
    if unity_client_sid:
        # Forward the volume data to the Unity client
        print(f"data: {data}")
        await sio.emit('volume_data', {'volume': data}, room=unity_client_sid)
        print(f"Sent volume data to Unity client {unity_client_sid}")

## Network management ##
@sio.event
async def get_network_status(sid):
    global network_enabled
    status = wifi_controller.wifi_status()
    network_enabled = status
    unity_client_sid = next((s for s, t in clients.items() if t == 'unity'), None)
    await sio.emit('network_status', {'status': status}, room=unity_client_sid)
    print(f"Sent network status to Unity client {unity_client_sid}")

@sio.event
async def enable_network(sid):
    global network_enabled
    wifi_controller.enable_wifi()
    network_enabled = True
    print(f"Sent network status to Unity client {sid}")

@sio.event
async def disable_network(sid):
    global network_enabled
    wifi_controller.disable_wifi()
    network_enabled = False
    print(f"Sent network status to Unity client {sid}")

## Default Weather ##
@sio.event
async def set_default_weather_city(sid, city_name):
    global default_weather_city
    default_weather_city = city_name
    print(f"Default city set to {city_name} by client {sid}")

@sio.event
async def get_default_weather_city(sid):
    global default_weather_city
    unity_client_sid = next((s for s, t in clients.items() if t == 'unity'), None)
    if unity_client_sid:
        print(f"default_weather_city: {default_weather_city}")
        await sio.emit('default_weather_city', {'city': default_weather_city}, room=unity_client_sid)
        print(f"Sent default weather city to Unity client {unity_client_sid}")
    else:
        print("Unity client not found")



# run the application
if __name__ == '__main__':
    web.run_app(app, port=PORT)

