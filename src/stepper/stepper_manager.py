#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# ----------------------------------------------------------------------------
# Created By  : Pajak Alexandre
# Created Date: 2023/03/07
# version ='1.0'
# ---------------------------------------------------------------------------
""" NEMA 17 (17HS4023) Emulator Class for simulations"""
# ---------------------------------------------------------------------------
# Imports
# ---------------------------------------------------------------------------
from time import sleep
import RPistepper as stp

class Stepper:
    minimal_step = 0
    maximal_step = 1000
    current_step = 0
    current_direction = 0
    motor = None
    gpio_pins = [0,0,0,0]

    def __init__(self,pin1,pin2,pin3,pin4):
        self.gpio_pins = [pin1,pin2,pin3,pin4]
        self.current_step = 0
        self.current_direction = 0
