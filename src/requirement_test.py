# Description: Test if all requirements are installed

# test all imports
def test_imports():
    # test all imports
    try:
        import face_recognition
        import cv2
        import numpy as np
        import os
        import cvzone
        import mediapipe
        import numpy
        import pyautogui
        import mouse
        import vosk
        import PyAudio
        import PySide6
        import dateutil
        import RPi.GPIO
        import threading
        import stepper
    except ImportError:
        print("ImportError")
        return False
    print("All imports are ok")
    return True

# main
def if__name__main():
    test_imports()

