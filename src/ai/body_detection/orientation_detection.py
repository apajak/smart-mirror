#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# ----------------------------------------------------------------------------
# Created By  : Gabriel Laroumanie, Pajak Alexandre
# Created Date: 2022/12/16
# version ='5.0'
# ---------------------------------------------------------------------------
""" Python Body detection and tracking for Unity's 3D model"""
# ---------------------------------------------------------------------------
# Imports
# ---------------------------------------------------------------------------
import cv2
import mediapipe as mp
from mediapipe.framework.formats import landmark_pb2
import time
import socket
import numpy as np
from src.ai.body_detection.landmarks import Landmarks_base_coordinates, Landmarks_coordinates, joint_names
from src.ai.head_detection import FacePoseEstimator
from src.ai.constants_and_connections import CAMERA


def draw_landmarks_on_image(frame, detection_result):
    pose_landmarks_list = detection_result.pose_landmarks
    annotated_image = np.copy(frame)
    for idx in range(len(pose_landmarks_list)):
        pose_landmarks = pose_landmarks_list[idx]
        pose_landmarks_proto = landmark_pb2.NormalizedLandmarkList()
        pose_landmarks_proto.landmark.extend([
            landmark_pb2.NormalizedLandmark(x=landmark.x, y=landmark.y, z=landmark.z) for landmark in pose_landmarks
        ])
        mp.solutions.drawing_utils.draw_landmarks(
            annotated_image,
            pose_landmarks_proto,
            mp.solutions.pose.POSE_CONNECTIONS,
            mp.solutions.drawing_styles.get_default_pose_landmarks_style())

    return annotated_image


class OrientationDetection:
    def __init__(self):
        self.scale = 5
        self.scaleTuple = (self.scale, self.scale, self.scale)
        self.landmarks_coordinates = Landmarks_coordinates
        self.landmarks_base_coordinates = Landmarks_base_coordinates
        self.rotation_degree = (0, 0, 0)
        self.head_pose_estimator = FacePoseEstimator()
        self.head_rotation = (0, 0, 0)
        self.base_dist_rotation = 0
        self.annotated_image = None
        self.BaseOptions = mp.tasks.BaseOptions
        self.PoseLandmarker = mp.tasks.vision.PoseLandmarker
        self.PoseLandmarkerOptions = mp.tasks.vision.PoseLandmarkerOptions
        self.VisionRunningMode = mp.tasks.vision.RunningMode
        self.options = self.PoseLandmarkerOptions(
            base_options=self.BaseOptions(model_asset_path='../ai/body_detection/pose_landmarker_heavy.task'),
            running_mode=self.VisionRunningMode.IMAGE)

        # UDP socket
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.server_address_port = ("127.0.0.1", 5052)

    def _get_coordinates(self, results):
        if results.pose_landmarks:
            for id, lm in enumerate(results.pose_landmarks[0]):
                if id in joint_names:
                    cx, cy, cz = (lm.x),(lm.y),(lm.z)
                    self.landmarks_coordinates[joint_names[id]] = (cx, cy, cz)
                    self.landmarks_base_coordinates[joint_names[id]] = (lm.x, lm.y)
            self._set_grounded()

    def _set_grounded(self):
        minimal_y = min(coord[1] for coord in self.landmarks_coordinates.values())
        self.landmarks_coordinates = {key: (value[0], value[1] - minimal_y, value[2]) if minimal_y < 0 else (
            value[0], value[1] + minimal_y, value[2]) for key, value in self.landmarks_coordinates.items()}

    def _calculate_euler_rotation(self):
        # Conversion des tuples en arrays numpy pour faciliter les calculs
        left_shoulder = np.array(self.landmarks_coordinates['left_shoulder'])
        right_shoulder = np.array(self.landmarks_coordinates['right_shoulder'])
        left_hip = np.array(self.landmarks_coordinates['left_hips'])  # Assurez-vous que cette clé est correcte
        right_hip = np.array(self.landmarks_coordinates['right_hips'])  # Assurez-vous que cette clé est correcte

        # Calcul du roulis (rotation autour de l'axe x) en utilisant les épaules
        shoulder_dx = right_shoulder[0] - left_shoulder[0]
        shoulder_dy = right_shoulder[1] - left_shoulder[1]
        roll = np.arctan2(shoulder_dy, shoulder_dx)
        roll_deg = np.degrees(roll)
        if roll_deg > 90:
            roll_deg -= 180
        elif roll_deg < -90:
            roll_deg += 180

        # Calcul du lacet (rotation autour de l'axe y) en utilisant les hanches
        hip_dx = right_hip[0] - left_hip[0]
        hip_dz = right_hip[2] - left_hip[2]
        yaw = np.arctan2(hip_dx, hip_dz)
        yaw_deg = np.degrees(yaw)
        yaw_deg += 90  # On ajoute 90 degrés pour que le lacet soit à 0° quand le buste est droit
        if yaw_deg < -90:
            yaw_deg = -90
        elif yaw_deg > 90:
            yaw_deg = 90

        # Stocker les angles dans la rotation_degree avec 2 décimales
        self.rotation_degree = (round(yaw_deg, 2), round(roll_deg, 2), self.rotation_degree[2])

    def _send_coordinates(self):
        datas_dict = self.landmarks_coordinates
        datas_dict["scaleTuple"] = self.scaleTuple
        datas_dict["rotation_degree"] = self.rotation_degree
        pelvis = ((datas_dict["left_hips"][0] + datas_dict["right_hips"][0]) / 2, (
                datas_dict["left_hips"][1] + datas_dict["right_hips"][1]) / 2, (
                          datas_dict["left_hips"][2] + datas_dict["right_hips"][2]) / 2)
        middle_shoulder = ((datas_dict["left_shoulder"][0] + datas_dict["right_shoulder"][0]) / 2, (
                datas_dict["left_shoulder"][1] + datas_dict["right_shoulder"][1]) / 2, (
                                   datas_dict["left_shoulder"][2] + datas_dict["right_shoulder"][2]) / 2)
        middle_spine = (middle_shoulder[0], (middle_shoulder[1] + pelvis[1]) / 2, (middle_shoulder[2] + pelvis[2]) / 2)
        dict_to_send = {
            'pelvis': pelvis,
            'left_hips': datas_dict["left_hips"],
            'left_knee': datas_dict["left_knee"],
            'left_foot': datas_dict["left_ankle"],
            'right_hips': datas_dict["right_hips"],
            'right_knee': datas_dict["right_knee"],
            'right_foot': datas_dict["right_ankle"],
            'left_arm': datas_dict["left_shoulder"],
            'left_elbow': datas_dict["left_elbow"],
            'right_arm': datas_dict["right_shoulder"],
            'right_elbow': datas_dict["right_elbow"],
            'middle_spine': middle_spine,
            'head': datas_dict["nose"],
            'left_hand': datas_dict["left_wrist"],
            'right_hand': datas_dict["right_wrist"],
            'scaleTuple': self.scaleTuple,
            'body_angle': self.rotation_degree,
            'head_angle': self.head_rotation
        }
        datas = "|".join(str(v) for v in dict_to_send.values())
        self.sock.sendto(datas.encode(), self.server_address_port)

    def to_string(self):
        return f"rotation: {self.rotation_degree}"

    def process_frame(self, frame, display=False, verbose=False, headless=False):
        if not headless:
            self.head_rotation = self.head_pose_estimator.estimate_pose(frame)
        with self.PoseLandmarker.create_from_options(self.options) as landmarker:
            # S'assure que la frame est valide
            if frame is None:
                print("Frame vide fournie à la méthode run.")
                return

            # Préparation de la frame pour MediaPipe
            frame.flags.writeable = False
            frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
            mp_image = mp.Image(image_format=mp.ImageFormat.SRGB, data=frame)
            results = landmarker.detect(mp_image)

            # Traitement des landmarks s'ils sont détectés
            if results.pose_landmarks:
                self._get_coordinates(results)
                self._calculate_euler_rotation()
                self._send_coordinates()

            if verbose:
                print(self.to_string())

            if display:
                yaw, roll,_ = self.rotation_degree
                # Affichage de la frame avec les annotations
                self.annotated_image = draw_landmarks_on_image(mp_image.numpy_view(), results)
                cv2.putText(self.annotated_image, f"Yaw: {yaw:.2f}", (10, 30), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2)
                cv2.putText(self.annotated_image, f"Roll: {roll:.2f}", (10, 60), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2)
                cv2.imshow('Video', self.annotated_image)
                if cv2.waitKey(1) & 0xFF == ord('q'):
                    cv2.destroyAllWindows()