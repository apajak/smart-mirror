import json

# chaîne JSON
json_str = '{"pose_landmarks": [{"id": 0, "x": 0.49535804986953735, "y": 0.8087036609649658, "z": -1.0430771112442017, "visibility": 0.9954522252082825, "presence": 0.9936163425445557}]}'

# Convertir la chaîne JSON en dictionnaire Python
data = json.loads(json_str)

# Accéder aux données, par exemple, imprimer les landmarks
for landmark in data["pose_landmarks"]:
    print(landmark)