#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# ----------------------------------------------------------------------------
# Created By  : Gabriel Laroumanie, Pajak Alexandre
# Created Date: 2023/03/17
# version ='1.0'
# ---------------------------------------------------------------------------
""" Dictionary of landmarks and their coordinates for the body detection"""
# ---------------------------------------------------------------------------
Landmarks = {
    'nose_id': 0,
    'left_shoulder_id': 11,
    'right_shoulder_id': 12,
    'right_elbow_id': 13,
    'left_elbow_id': 14,
    'right_wrist_id': 15,
    'left_wrist_id': 16,
    'right_hips_id': 24,
    'left_hips_id': 23,
    'right_knee_id': 25,
    'left_knee_id': 26,
    'right_ankle_id': 27,
    'left_ankle_id': 28
}

Landmarks_coordinates = {
    'nose': (0, 0, 0),
    'left_shoulder': (0, 0, 0),
    'right_shoulder': (0, 0, 0),
    'right_elbow': (0, 0, 0),
    'left_elbow': (0, 0, 0),
    'right_wrist': (0, 0, 0),
    'left_wrist': (0, 0, 0),
    'right_hips': (0, 0, 0),
    'left_hips': (0, 0, 0),
    'right_knee': (0, 0, 0),
    'left_knee': (0, 0, 0),
    'right_ankle': (0, 0, 0),
    'left_ankle': (0, 0, 0)
}
Landmarks_base_coordinates = {
    'nose': (0, 0),
    'left_shoulder': (0, 0),
    'right_shoulder': (0, 0),
    'right_elbow': (0, 0),
    'left_elbow': (0, 0),
    'right_wrist': (0, 0),
    'left_wrist': (0, 0),
    'right_hips': (0, 0),
    'left_hips': (0, 0),
    'right_knee': (0, 0),
    'left_knee': (0, 0),
    'right_ankle': (0, 0),
    'left_ankle': (0, 0)
}

joint_names = {
    Landmarks['nose_id']: 'nose',
    Landmarks['left_shoulder_id']: 'left_shoulder',
    Landmarks['right_shoulder_id']: 'right_shoulder',
    Landmarks['right_elbow_id']: 'right_elbow',
    Landmarks['left_elbow_id']: 'left_elbow',
    Landmarks['right_wrist_id']: 'right_wrist',
    Landmarks['left_wrist_id']: 'left_wrist',
    Landmarks['right_hips_id']: 'right_hips',
    Landmarks['left_hips_id']: 'left_hips',
    Landmarks['right_knee_id']: 'right_knee',
    Landmarks['left_knee_id']: 'left_knee',
    Landmarks['right_ankle_id']: 'right_ankle',
    Landmarks['left_ankle_id']: 'left_ankle',
}
