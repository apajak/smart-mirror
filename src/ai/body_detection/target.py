import matplotlib.pyplot as plt
import numpy as np
import random

def random_point():
    return (random.randint(0, 10), random.randint(0, 10))

if __name__ == "__main__":
    print("Smart Mirror test aime point Unity")
    point_list = []
    line_list = []
    # generate random coordinates
    point_1 = random_point()
    point_2 = random_point()

    # print coordinates
    print(f"point_1 : {point_1}")
    print(f"point_2 : {point_2}")
    # add points to list
    point_list.append(point_1)
    point_list.append(point_2)
    # add line to list
    line_list.append((point_1, point_2))

    # find the center of the line
    center = ((point_1[0] + point_2[0]) / 2, (point_1[1] + point_2[1]) / 2)
    # add center to list
    point_list.append(center)

    # find a 3rd point perpendicular to the line and passing through the center of the line and with a distance of 1
    # find the vector from point_1 to point_2
    v = np.array(point_2) - np.array(point_1)
    # find the unit vector perpendicular to v
    u = np.array([-v[1], v[0]]) / np.linalg.norm(v)
    # find the coordinates of the third point
    distance = 1
    point_3 = tuple(center + distance * u)
    # add 3rd point to list
    point_list.append(point_3)
    line_list.append((center, point_3))

    # draw
    fig = plt.figure()
    ax = fig.add_subplot(111)
    for point in point_list:
        ax.scatter(point[0], point[1], color='r')
    for line in line_list:
        ax.plot([line[0][0], line[1][0]], [line[0][1], line[1][1]], color='b')
    plt.show()
