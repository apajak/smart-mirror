# Python Body detection and tracking for Unity's 3D model

This Python script provides body detection and tracking for Unity's 3D model. It uses the Mediapipe library for pose estimation and OpenCV for image processing.

## Requirements
- Python 3.x
- OpenCV
- Mediapipe

## Usage

1. Install the required dependencies:
   ```bash
   pip install opencv-python mediapipe
   ```
   
2. Set up the Unity environment and the 3D model to receive the body tracking data.
Run the script:
```bash
python orientation_detection.py
```
The script will start capturing video from the camera and perform body detection and tracking in real-time. The tracked body coordinates will be sent to the Unity application for further processing.
*Optional:* Adjust the script parameters:
*CAMERA:* Set the camera index or video file path to capture video from.
Other parameters like tolerances and scales can be adjusted based on the specific requirements.

## Examples

### Threaded mode (run in the background):
```python3
orientation_detection = OrientationDetection()
orientation_detection.threaded_start(verbose=True)
sleep(10)
orientation_detection.stop()
```

### Windowed mode (display video and tracking results):
```python3
orientation_detection = OrientationDetection()
orientation_detection.run(display=True, verbose=True)
```
## Credits
Roulland Roxanne 
Laroumanie Gabriel
Pajak Alexandre
