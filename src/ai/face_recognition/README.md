# Face Recognition

## Introduction

This is a Python module for managing user recognition through face detection and recognition. It utilizes the `face_recognition` library for face-related operations and `OpenCV` for video capture and display.

## Features

- Detect and recognize faces in real-time video streams.
- Compare detected faces with known faces to identify users.
- Display the results with bounding boxes and names in an OpenCV window.

## Requirements

- Python 3.8
- `face_recognition` library (1.3.0)
- `OpenCV` library (4.7.0.72)
- `numpy` library (1.24.2)
- `dlib` library (19.24.0)
- `pillow` library (9.4.0)

## Credits
Pajak Alexandre