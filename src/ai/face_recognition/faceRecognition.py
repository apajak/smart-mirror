#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# ----------------------------------------------------------------------------
# Created By  : Pajak Alexandre
# Created Date: 01/10/2022
# version ='1.0'
# ---------------------------------------------------------------------------
""" Module for Manage user recognition. """
from datetime import time

# ---------------------------------------------------------------------------
import face_recognition
from face_recognition import face_locations, face_encodings, compare_faces, face_distance
import os
import cv2
import numpy as np
import time
from src.ai.constants_and_connections import CAMERA


class FaceRecognition:
    """Class to recognize user faces"""
    known_faces = []
    known_face_names = []
    FaceDetected = False
    face_names = []
    FaceName = None
    run = True
    thread = None
    image_dirPath = f'/home/lexit/Ynov/smart-mirror/src/known_faces/'

    def __init__(self):
        """Constructor"""
        self._encode_known_faces()

    def _encode_known_faces(self):
        """Encode known faces in the known_faces list and known_face_names list for future comparison"""
        self.known_faces = []
        self.known_face_names = []
        for file in os.listdir(self.image_dirPath):
            loaded_image = face_recognition.load_image_file(f"{self.image_dirPath}{file}")
            encodings_face = face_recognition.face_encodings(loaded_image)[0]
            self.known_faces.append(encodings_face)
            self.known_face_names.append(file.split(".")[0])
        return self.known_faces, self.known_face_names

    @staticmethod
    def _convert_frame(frame):
        """Convert frame to RGB and resize to 1/4 to speed up face recognition processing"""
        small_frame = cv2.resize(frame, (0, 0), fx=0.25, fy=0.25)
        rgb_small_frame = small_frame[:, :, ::-1]
        return rgb_small_frame

    @staticmethod
    def _get_face_locations(frame):
        """Get face locations in frame"""
        return face_locations(frame)

    @staticmethod
    def _get_face_encodings(frame, face_locations):
        """Get face encodings in frame"""
        return face_encodings(frame, face_locations)

    def _compare_faces(self, face_encodings):
        """Compare faces in frame with known faces"""
        face_names = []
        for face_encoding in face_encodings:
            matches = compare_faces(self.known_faces, face_encoding)
            face_distances = face_distance(self.known_faces, face_encoding)
            best_match_index = np.argmin(face_distances)
            if matches[best_match_index]:
                name = self.known_face_names[best_match_index]
            else:
                name = "Unknown"
            face_names.append(name)
        return face_names

    @staticmethod
    def _display(frame, face_locations, face_names):
        """Display the results in opencv window"""
        for (top, right, bottom, left), name in zip(face_locations, face_names):
            top *= 4
            right *= 4
            bottom *= 4
            left *= 4
            cv2.rectangle(frame, (left, top), (right, bottom), (0, 0, 255), 2)
            cv2.rectangle(frame, (left, bottom - 35), (right, bottom), (0, 0, 255), cv2.FILLED)
            font = cv2.FONT_HERSHEY_DUPLEX
            cv2.putText(frame, name, (left + 6, bottom - 6), font, 1.0, (255, 255, 255), 1)
        return frame

    def run_detection(self, frame, display=False, verbose=False):
        """Run face recognition on a given frame"""
        rgb_small_frame = self._convert_frame(frame)
        face_locations = self._get_face_locations(rgb_small_frame)
        start_time = time.time()
        face_encodings = self._get_face_encodings(rgb_small_frame, face_locations)
        self.face_names = self._compare_faces(face_encodings)
        self.FaceDetected = len(self.face_names) > 0
        if self.FaceDetected:
            self.FaceName = self.face_names[0]
        else:
            self.FaceName = None
        if display:
            frame = self._display(frame, face_locations, self.face_names)
            cv2.imshow('Video', frame)
        if verbose:
            print(f"{self.face_names} detected")
        if cv2.waitKey(1) & 0xFF == ord('q'):
            cv2.destroyAllWindows()
            return False
        return True
