import socket
import cv2
import time
from math import hypot
from mediapipe.python.solutions.hands import Hands
from src.ai import constants_and_connections as cc

class HandRecognizer:
    def __init__(self):
        self.mouse_mode = True
        self.run = True
        self.server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server_socket.bind(("localhost", 9999))
        self.favorite_hands = ["Right", "Left"]
        self.server_socket.listen(1)
        self.sensitivity = 1.5
        self.mouvment_threshold = 20
        self.last_position = (0, 0)
        self.hands = Hands(max_num_hands=2, min_detection_confidence=0.5, min_tracking_confidence=0.5)
        print("Waiting for connection...")
        self.client_socket, _ = self.server_socket.accept()
        print("Client connected")
        self.screen_width, self.screen_height = self.get_screen_size()
        print(f"Screen size: {self.screen_width}x{self.screen_height}")

    def get_screen_size(self):
        data = self.client_socket.recv(1024).decode('utf-8')
        width, height = map(int, data.split(','))
        return width, height

    def send_mouse_commands(self, action, position=(0, 0)):
        if abs(position[0] - self.last_position[0]) < self.mouvment_threshold and abs(position[1] -
                    self.last_position[1]) < self.mouvment_threshold and action == 'move' or action == 'release':
            return
        data = f"{action}:{position[0]:.2f},{position[1]:.2f}\n"
        try:
            self.client_socket.sendall(data.encode('utf-8'))
            self.last_position = position
        except socket.error as e:
            self.reconnect()

    def reconnect(self):
        self.client_socket.close()
        self.client_socket, _ = self.server_socket.accept()

    def process_hands(self, hands, mouse_cursor):
        if self.mouse_mode:
            self.detect_clicks(hands[0], mouse_cursor)
            self.send_mouse_commands('move', (mouse_cursor.x, mouse_cursor.y))

    def calculate_average_distance(self, hands):
        total_distance = sum(hypot(abs(hands[0][f['tip']].x - hands[1][f['tip']].x),
                                   abs(hands[0][f['tip']].y - hands[1][f['tip']].y)) for f in cc.LIST_ID_FINGERS)
        return total_distance / len(cc.LIST_ID_FINGERS)

    def detect_clicks(self, hand, mouse_cursor):
        finger_positions = {f['name']: (hand[f['tip']].x, hand[f['tip']].y) for f in cc.LIST_ID_FINGERS}
        distances = {f: hypot(finger_positions[f][0] - finger_positions['Thumb'][0],
                              finger_positions[f][1] - finger_positions['Thumb'][1]) for f in finger_positions if f != 'Thumb'}
        if distances['Index'] < 0.04:
            self.send_mouse_commands('click', (mouse_cursor.x, mouse_cursor.y))
            print("Click")
        elif distances['Ringfinger'] < 0.04:
            self.send_mouse_commands('press', (mouse_cursor.x, mouse_cursor.y))
            print("Press")
        elif distances['Littlefinger'] < 0.04:
            self.send_mouse_commands('right_click', (mouse_cursor.x, mouse_cursor.y))
            print("Right click")
        else:
            self.send_mouse_commands('release', (mouse_cursor.x, mouse_cursor.y))
            print("Release")

    def run_detection(self, frame):
        frame = cv2.flip(frame, 1)
        frame_rgb = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        results = self.hands.process(frame_rgb)
        right_hand = None
        left_hand = None
        hand_to_control = []
        hands_detected = []

        if results.multi_hand_landmarks:
            for hand_landmarks, handedness in zip(results.multi_hand_landmarks, results.multi_handedness):
                label = handedness.classification[0].label  # Get the label of the hand
                if label in self.favorite_hands:
                    mouse_cursor = hand_landmarks.landmark[cc.ID_MIDDLEFINGER_PHALANX]
                    mouse_cursor.x = self.screen_width * mouse_cursor.x * self.sensitivity
                    mouse_cursor.y = self.screen_height * mouse_cursor.y * self.sensitivity
                    hand_to_control.append(hand_landmarks.landmark)
                    hands_detected.append(hand_landmarks.landmark)

            if hand_to_control:
                self.process_hands(hand_to_control, mouse_cursor)

            if len(hands_detected) == 2:
                self.process_start_stop_conditions(hands_detected)  # Check for start/stop conditions

    def process_start_stop_conditions(self, hands):
        average_distance = self.calculate_average_distance(hands)
        print(f"Number of hands: {len(hands)}")
        print(f"Average distance: {average_distance:.2f}")
        if average_distance < 0.1:  # Example condition
            new_mode = not self.mouse_mode
            self.mouse_mode = new_mode
            print(f"Mouse mode changed to: {'Activated' if new_mode else 'Deactivated'}")
            time.sleep(0.5)


if __name__ == '__main__':
    hand_recognizer = HandRecognizer()
    cap = cv2.VideoCapture(0)
    while hand_recognizer.run:
        ret, frame = cap.read()
        if ret:
            hand_recognizer.run_detection(frame)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
    cap.release()
    cv2.destroyAllWindows()
    hand_recognizer.client_socket.close()
    hand_recognizer.server_socket.close()
