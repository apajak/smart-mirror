import cv2
import dlib
import numpy as np


class FacePoseEstimator:
    def __init__(self):
        # Load pre-trained models and initialize parameters
        self.face_cascade = cv2.CascadeClassifier(cv2.data.haarcascades + 'haarcascade_frontalface_default.xml')
        self.predictor = dlib.shape_predictor("../ai/head_detection/shape_predictor_68_face_landmarks.dat")  # Make sure to have this file
        self.detector = dlib.get_frontal_face_detector()
        self.model_points = np.array([
            (0.0, 0.0, 0.0),  # Nose tip
            (0.0, -330.0, -65.0),  # Chin
            (-225.0, 170.0, -135.0),  # Left eye left corner
            (225.0, 170.0, -135.0),  # Right eye right corner
            (-150.0, -150.0, -125.0),  # Left mouth corner
            (150.0, -150.0, -125.0)  # Right mouth corner
        ])
        self.dist_coeffs = np.zeros((4, 1))  # Assuming no lens distortion

    def estimate_pose(self, frame):
        # Define camera internals
        size = frame.shape[1], frame.shape[0]
        focal_length = size[1]
        center = (size[1] / 2, size[0] / 2)
        camera_matrix = np.array(
            [[focal_length, 0, center[0]],
             [0, focal_length, center[1]],
             [0, 0, 1]], dtype="double"
        )

        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        faces = self.face_cascade.detectMultiScale(gray, 1.1, 4)

        for (x, y, w, h) in faces:
            rect = dlib.rectangle(int(x), int(y), int(x + w), int(y + h))
            shape = self.predictor(gray, rect)
            image_points = np.array([
                (shape.part(30).x, shape.part(30).y),  # Nose tip
                (shape.part(8).x, shape.part(8).y),  # Chin
                (shape.part(36).x, shape.part(36).y),  # Left eye left corner
                (shape.part(45).x, shape.part(45).y),  # Right eye right corner
                (shape.part(48).x, shape.part(48).y),  # Left mouth corner
                (shape.part(54).x, shape.part(54).y)  # Right mouth corner
            ], dtype="double")

            success, rotation_vector, translation_vector = cv2.solvePnP(self.model_points, image_points, camera_matrix, self.dist_coeffs, flags=cv2.SOLVEPNP_ITERATIVE)
            rvec_matrix = cv2.Rodrigues(rotation_vector)[0]
            proj_matrix = np.hstack((rvec_matrix, translation_vector))
            _, _, _, _, _, _, euler_angles = cv2.decomposeProjectionMatrix(proj_matrix)
            pitch, yaw, roll = [float(angle) for angle in euler_angles]

            # Return the estimated pose angles
            return pitch, yaw, roll

        # Return None if no faces are detected
        return None, None, None