import pvporcupine
import os
from pvrecorder import PvRecorder

class HotwordDetector:
    def __init__(self, keywords, sensitivities):
        self.keywords = keywords
        self.porcupine = None
        self.recorder = None
        self.sensitivities = sensitivities

    def setup(self):
        access_key = "5Uuzow+iU8ajoSX2MdZAndYQ+afhSKhEI1dKeP8ufBvj3CyJ7W82Wg=="
        models_path = os.path.join(os.path.dirname(__file__), 'models')

        # Initialize Porcupine
        self.porcupine = pvporcupine.create(
            access_key=access_key,
            keyword_paths=[
                os.path.join(models_path, f"{keyword}_en_linux_v3_0_0.ppn") for keyword in self.keywords
            ],
            sensitivities=self.sensitivities
        )

        # Initialize recorder
        self.recorder = PvRecorder(device_index=-1, frame_length=self.porcupine.frame_length)

    def listen(self, callback):
        self.setup()
        try:
            self.recorder.start()
            while True:
                keyword_index = self.porcupine.process(self.recorder.read())
                if keyword_index >= 0:
                    callback(True)
        except KeyboardInterrupt:
            self.stop()
        finally:
            self.delete()

    def stop(self):
        if self.recorder:
            self.recorder.stop()

    def delete(self):
        if self.porcupine:
            self.porcupine.delete()
        if self.recorder:
            self.recorder.delete()

# test code
if __name__ == '__main__':
    detector = HotwordDetector(["Smart-Mirror"], [0.8])
    def test_callback(keyword):
        print("Hotword Detected: %s" % keyword)

    print("Listening...")
    detector.listen(test_callback)