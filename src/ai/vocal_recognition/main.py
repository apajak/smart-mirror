import pyaudio
import vosk
import json
from src.api.weather import current_weather


model = vosk.Model("./vosk-model-small-fr-0.22")
rec = vosk.KaldiRecognizer(model, 16000)
p = pyaudio.PyAudio()
stream = p.open(format=pyaudio.paInt16, channels=1, rate=16000, input=True, frames_per_buffer=8000)
stream.start_stream()
word = "test"

cities = ["paris", "londres", "berlin", "bordeaux", "madrid", "rome", "amsterdam", "barcelone",
          "budapest", "bruxelles", "copenhague", "dublin", "helsinki", "lisbonne", "ljubljana",
          "londres", "luxembourg", "malte", "oslo", "prague", "reykjavik", "riga", "rome", "sofia",
          "stockholm", "tallinn", "vienne", "vilnius", "zagreb"]

while True:
    data = stream.read(4000)
    if rec.AcceptWaveform(data):
        res = json.loads(rec.Result())
        print(res["text"])
        if word in res["text"]:
            print("je vous ecoute")
            for city in cities:
                if city in res["text"]:
                    print("city found")
                    weather = current_weather.CurrentWeather(city)
                    weather.toString()
                    break
