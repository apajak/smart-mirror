from pathlib import Path
from gtts import gTTS
import os
import glob
from playsound import playsound


class TTS:
    def __init__(self, lang):
        self.language = lang
        self.audiofile_path = Path(__file__).parent / "waitlist_audio" / "audio.mp3"

    def text_to_audiofile(self, text: str) -> None:
        tts = gTTS(text=text, lang=self.language)

        tts.save(str(self.audiofile_path))

    def play_audio(self) -> bool:
        try:
            playsound(self.audiofile_path)
            os.remove(self.audiofile_path)
            return True
        except Exception as e:
            return False

    def run(self, text) -> None:
        print('Processing text to audio...')
        self.text_to_audiofile(text)
        self.play_audio()


# Test code
if __name__ == '__main__':
    tts_obj = TTS('fr')
    tts_obj.run("La couleur du soleil vue depuis Jupiter est assez similaire à celle que nous voyons depuis la Terre, "
                "c'est-à-dire principalement blanche. Cependant, l'atmosphère de Jupiter est différente de celle de la "
                "Terre et peut potentiellement modifier la manière dont nous percevons la couleur du soleil. "
                "L'atmosphère de Jupiter contient principalement de l'hydrogène et de l'hélium, ce qui peut filtrer "
                "certaines longueurs d'ondes de lumière et potentiellement donner une teinte légèrement différente à la"
                " lumière solaire. Cependant, il est difficile de prédire précisément l'effet exact de l'atmosphère de "
                "Jupiter sur la couleur du soleil vue depuis cette planète.")