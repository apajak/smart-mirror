import pyaudio
import vosk
import json


class STT:
    p = pyaudio.PyAudio()

    def __init__(self, lang):
        self.language = lang
        self.rec = vosk.KaldiRecognizer(vosk.Model("./vosk-model-small-fr-0.22"), 16000)
        self.stream = pyaudio.PyAudio().open(format=pyaudio.paInt16, channels=1, rate=16000,
                                             input=True, frames_per_buffer=8000)

    def run(self) -> str:
        self.stream.start_stream()
        print("Listening...")
        res = ""

        # Loop on the audio stream
        while self.stream.is_active():
            data = self.stream.read(4000)

            # If there is any sound provided
            if self.rec.AcceptWaveform(data):
                res += json.loads(self.rec.Result())["text"]
            else:
                # If we didn't get any sound from the beginning of the stream
                if res == "":
                    continue
                else:
                    print("Stop the recording")
                    break

        return res


if __name__ == "__main__":
    stt = STT("fr")
    print(stt.run())