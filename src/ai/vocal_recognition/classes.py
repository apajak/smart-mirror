couleurs = ["rouge", "bleu", "vert", "jaune", "noir", "blanc", "violet", "rose", "orange", "gris"]
color = ["red", "blue", "green", "yellow", "black", "white", "violet", "pink", "orange", "grey"]

jours = ["lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi", "dimanche"]
day = ["monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"]

mois = ["janvier", "février", "mars", "avril", "mai", "juin", "juillet", "août", "septembre", "octobre", "novembre", "décembre", "aujourd'hui", "demain", "après-demain"]
month = ["january", "february", "march", "april", "may", "june", "july", "august", "september", "october", "november", "december", "today", "tomorrow", "after tomorrow"]

vetements = ["pantalon", "pantalons", "robe", "robes", "t-shirt", "t-shirts", "pull", "pulls", "veste", "vestes", "casquette", "casquettes", "bonnet", "bonnets", "lunette"," lunettes"]
clothes = ["trousers", "trousers", "dress", "dresses", "t-shirt", "t-shirts", "sweater", "sweaters", "jacket", "jackets", "cap", "caps", "hat", "hats", "glasses", "glasses"]

météo = ["météo", "temps"]
weather = ["weather"]

WAKE = ["hey mirror", "hey mirroir"]

francais = [couleurs, jours, mois, vetements, météo]
english = [color, day, month, clothes, weather]

key_words = [francais, english]
