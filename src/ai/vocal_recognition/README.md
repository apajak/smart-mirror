# Vocal recognition 

The goal here is to wait for an hot word to be spoken
by the user to start the vocal recognition, when the user
asked his question, we ask gpt for an answer and transform 
that answer into an audio file that will be listened

# Steps 

1. Wait the hot word 
2. Start vocal recognition if stop word detected go to step 1.
3. Ask GTP for an answer to the question 
4. Transform GPT's answer into audio
5. Play audio
6. Go to step 2.
