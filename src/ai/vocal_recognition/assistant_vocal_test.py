from src.ai.vocal_recognition.tts import TTS
from src.ai.vocal_recognition.stt import STT
from src.ai.vocal_recognition.hot_word_detection.hotword_detection import HotwordDetector
from src.api.openai.gpt import GPT
import time


class VocalAssistant:
    """
    This class is a vocal assistant that can take for input audio from the microphone turns it into text
    the  answer by audio, it uses TTS model from Google, STT model from Google, GPT from OpenAI
    """
    def __init__(self, lang: str, keywords: list = ["Smart-Mirror"], sensitivities: list = [0.8]):
        self.text_to_speech = TTS(lang)
        self.speech_to_text = STT(lang)
        self.hot_word_detector = HotwordDetector(keywords=keywords, sensitivities=sensitivities)
        self.gpt = GPT()
        self.stop_words = "stop"
        self.online = False

    def hot_word_detection(self):
        self.hot_word_detector.listen(self.run)

    def run(self, detected=False):
        while True:
            text = self.speech_to_text.run()
            if text in self.stop_words:
                break
            response = self.gpt.ask_gpt(text)
            self.text_to_speech.run(response)
            time.sleep(1.5)


if __name__ == "__main__":
    assistant = VocalAssistant('fr')
    assistant.run()
