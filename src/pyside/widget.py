﻿import sys
from src.api.weather.weather import Weather
from src.pyside.pages.main import MainWindow

from PySide6 import QtCore
from PySide6.QtWidgets import (
    QApplication, QLabel,
    QVBoxLayout, QWidget)

weather = Weather()


class Screen2(QWidget):
    def __init__(self):
        super().__init__()
        # La fenêtre se met en arrière plan
        self.setWindowFlags(QtCore.Qt.FramelessWindowHint)
        self.layout =  QVBoxLayout()
        self.video_frame = QLabel()
        self.video_frame.setScaledContents(True)
        self.layout.addWidget(self.video_frame)
        self.setLayout(self.layout)


if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = MainWindow()
    window.show()
    sys.exit(app.exec())
