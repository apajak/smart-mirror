from PySide6.QtWidgets import (
    QVBoxLayout, QWidget)


class Home(QWidget):
    def __init__(self):
        super().__init__()
        self.layout = QVBoxLayout()
        self.setLayout(self.layout)
