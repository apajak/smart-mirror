import requests

from src.api.weather.weather import Weather

from PySide6 import QtCore
from PySide6.QtWidgets import (
    QLabel, QVBoxLayout, QPushButton, QWidget,
    QGridLayout, QToolButton)
from PySide6.QtCore import *
from PySide6.QtGui import *


class Vetements(QWidget):
    def __init__(self, main_window):
        super().__init__()
        self.main_window = main_window

        layout = QVBoxLayout()
        self.setLayout(layout)

        self.stacked_widget = main_window.stacked_widget

# Ajout météo
        weather = Weather()
        currentWeather = weather.get_CurrentWeather("Bordeaux")
        icon = currentWeather.get_weather().weather[0].icon
        url = "http://openweathermap.org/img/wn/" + icon + "@2x.png"
        response = requests.get(url)
        img = QImage()
        img.loadFromData(response.content)
        pixmap_meteo = QPixmap.fromImage(img)
        self.button_meteo = QToolButton()
        self.button_meteo.setIcon(QIcon(pixmap_meteo))
        self.button_meteo.clicked.connect(self.go_to_meteo)
        self.button_meteo.setStyleSheet("background: transparent")
        self.button_meteo.setIconSize(QtCore.QSize(80, 80))
        self.button_meteo.setGeometry(6, 6, 80, 80)
        layout.addWidget(self.button_meteo)

        temp_actuelle = str(round(currentWeather.get_temp())) + "°C"
        self.temp1 = QLabel(temp_actuelle)
        self.temp1.setStyleSheet("color: white; font-size: 14px; font-weight: bold")
        self.temp1.setGeometry(75, 0, 50, 50)
        layout.addWidget(self.temp1)

# Ajout de l'heure :
        self.time_label = QLabel()
        self.time_label.setGeometry(380, 6, 50, 50)
        self.time_label.setStyleSheet("QLabel { background: none; color: white; font-size: 14pt; font-weight: bold}")
        layout.addWidget(self.time_label, alignment=QtCore.Qt.AlignTop | QtCore.Qt.AlignCenter)
        self.timer = QTimer(self)
        self.timer.timeout.connect(self.update_time)
        self.timer.start()

# Les boutons qui composent la page des vêtements
        # Ajout du GridLayout des vêtements
        self.grid_vetements = QGridLayout()

        # Ajout du bouton vêtements
        self.bouton_vetements = QPushButton("Vêtements")
        font = self.bouton_vetements.font()
        font.setBold(True)  # Mettre en gras le texte
        self.bouton_vetements.clicked.connect(self.go_to_home)
        self.bouton_vetements.setStyleSheet("background: transparent; color: white; border: 2px solid white; border-radius: 10px")
        self.bouton_vetements.setGeometry(725, 6, 70, 50)
        layout.addWidget(self.bouton_vetements)

        # Ajout du bouton casquette
        self.bouton_casquette = QToolButton()
        self.bouton_casquette.setIcon(QIcon("../asset/icone_casquette.png"))
        self.bouton_casquette.setStyleSheet("background: transparent; border: 2px solid white; border-radius: 10px")
        self.bouton_casquette.setIconSize(QtCore.QSize(40, 40))
        self.bouton_casquette.setGeometry(725, 67, 70, 50)
        layout.addWidget(self.bouton_casquette)

        # Ajout du bouton lunettes
        self.bouton_lunettes = QToolButton()
        self.bouton_lunettes.setIcon(QIcon("../asset/icone_lunette.jpg"))
        self.bouton_lunettes.setStyleSheet("background: transparent; border: 2px solid white; border-radius: 10px")
        self.bouton_lunettes.setIconSize(QtCore.QSize(40, 40))
        self.bouton_lunettes.setGeometry(725, 130, 70, 50)
        layout.addWidget(self.bouton_lunettes)

        # Ajout du bouton pantalons
        self.bouton_pantalon = QToolButton()
        self.bouton_pantalon.setIcon(QIcon("../asset/icone_pantalon.jpg"))
        self.bouton_pantalon.setStyleSheet("background: transparent; border: 2px solid white; border-radius: 10px")
        self.bouton_pantalon.setIconSize(QtCore.QSize(25, 25))
        self.bouton_pantalon.setGeometry(725, 195, 70, 50)
        layout.addWidget(self.bouton_pantalon)

        # Ajout du bouton tshirts
        self.bouton_tshirt = QToolButton()
        self.bouton_tshirt.setIcon(QIcon("../asset/icone_tshirt.jpg"))
        self.bouton_tshirt.setStyleSheet("background: transparent; border: 2px solid white; border-radius: 10px")
        self.bouton_tshirt.setIconSize(QtCore.QSize(25, 25))
        self.bouton_tshirt.setGeometry(725, 260, 70, 50)
        layout.addWidget(self.bouton_tshirt)


        # Ajout des boutons au grid vêtements
        self.grid_vetements.addWidget(self.button_meteo)
        self.grid_vetements.addWidget(self.temp1)
        self.grid_vetements.addWidget(self.bouton_vetements)
        self.grid_vetements.addWidget(self.bouton_casquette)
        self.grid_vetements.addWidget(self.bouton_lunettes)
        self.grid_vetements.addWidget(self.bouton_pantalon)
        self.grid_vetements.addWidget(self.bouton_tshirt)
        self.grid_vetements.addWidget(self.time_label)

    def update_time(self):
        current_time = QTime.currentTime()
        self.time_label.setText(current_time.toString("hh:mm"))

    def go_to_home(self):
        self.main_window.stacked_widget.setCurrentWidget(self.main_window.home)

    def go_to_meteo(self):
        self.main_window.stacked_widget.setCurrentWidget(self.main_window.meteo)
