import requests

from src.api.weather.weather import Weather
from src.pyside.Toggle import Toggle

from PySide6 import QtCore
from PySide6.QtWidgets import (
    QLabel, QWidget, QVBoxLayout, QPushButton, QSlider, QCheckBox,
    QGridLayout, QToolButton)
from PySide6.QtCore import *
from PySide6.QtGui import *


class Parametres(QWidget):
    def __init__(self, main_window):
        super().__init__()
        self.main_window = main_window

        layout = QVBoxLayout()
        self.setLayout(layout)

        self.stacked_widget = main_window.stacked_widget

# Les boutons qui composent la page réglages
        # Ajout du GridLayout des réglages
        self.grid_reglages = QGridLayout()

        weather = Weather()
        currentWeather = weather.get_CurrentWeather("Bordeaux")
        icon = currentWeather.get_weather().weather[0].icon
        url = "http://openweathermap.org/img/wn/" + icon + "@2x.png"
        response = requests.get(url)
        img = QImage()
        img.loadFromData(response.content)
        pixmap_meteo = QPixmap.fromImage(img)
        self.button_meteo = QToolButton()
        self.button_meteo.setIcon(QIcon(pixmap_meteo))
        self.button_meteo.clicked.connect(self.go_to_meteo)
        self.button_meteo.setStyleSheet("background: transparent")
        self.button_meteo.setIconSize(QtCore.QSize(80, 80))
        self.button_meteo.setGeometry(6, 6, 80, 80)
        layout.addWidget(self.button_meteo)

        temp_actuelle = str(round(currentWeather.get_temp())) + "°C"
        self.temp1 = QLabel(temp_actuelle)
        self.temp1.setStyleSheet("color: white; font-size: 14px; font-weight: bold")
        self.temp1.setGeometry(75, 0, 50, 50)
        layout.addWidget(self.temp1)

# Ajout de l'heure :
        self.grid_time = QGridLayout()
        self.time_label = QLabel()
        self.time_label.setGeometry(380, 6, 50, 50)
        self.time_label.setStyleSheet("QLabel { background: none; color: white; font-size: 14pt; font-weight: bold}")
        layout.addWidget(self.time_label, alignment=QtCore.Qt.AlignTop | QtCore.Qt.AlignCenter)
        self.timer = QTimer(self)
        self.timer.timeout.connect(self.update_time)
        self.timer.start()

        # Ajout du bouton vêtements
        self.bouton_reglages = QPushButton("Réglages")
        font = self.bouton_reglages.font()
        font.setBold(True)  # Mettre en gras le texte
        self.bouton_reglages.clicked.connect(self.go_to_home)
        self.bouton_reglages.setStyleSheet("background: black; color: white; border: 2px solid white; border-radius: 10px")
        self.bouton_reglages.setGeometry(725, 6, 70, 50)
        layout.addWidget(self.bouton_reglages)

        # Ajout du label pour le niveau de luminosité
        self.label_luminosité = QLabel('0')
        self.label_luminosité.setStyleSheet("color: white")
        self.label_luminosité.setGeometry(658, 51, 20, 50)
        layout.addWidget(self.label_luminosité)

        # Ajout de l'icône luminosité
        self.icone_luminosité = QPushButton()
        self.icone_luminosité.setIcon(QIcon("../asset/icone_luminosité.png"))
        self.icone_luminosité.setIconSize(QtCore.QSize(27, 27))
        self.icone_luminosité.setGeometry(660, 51, 70, 50)
        self.icone_luminosité.setStyleSheet("background: transparent; color: white")
        self.icone_luminosité.clicked.connect(self.go_to_home)
        layout.addWidget(self.icone_luminosité)

        # Ajout de la barre de progression pour la luminosité
        self.slider_luminosité = QSlider(Qt.Horizontal)
        self.slider_luminosité.setRange(0, 100)
        self.slider_luminosité.setGeometry(725, 67, 70, 20)
        self.slider_luminosité.setSingleStep(1)
        self.slider_luminosité.valueChanged.connect(self.updateLuminositéLabel)
        layout.addWidget(self.slider_luminosité)

        # Ajout du label pour le niveau du son
        self.label_son = QLabel('0')
        self.label_son.setStyleSheet("color: white")
        self.label_son.setGeometry(658, 115, 20, 50)
        layout.addWidget(self.label_son)

        # Ajout de l'icône son
        self.icone_son = QPushButton()
        self.icone_son.setIcon(QIcon("../asset/icone_son.png"))
        self.icone_son.setIconSize(QtCore.QSize(26, 26))
        self.icone_son.setGeometry(660, 115, 70, 50)
        self.icone_son.setStyleSheet("background: transparent; color: white")
        self.icone_son.clicked.connect(self.go_to_home)
        layout.addWidget(self.icone_son)

        # Ajout de la barre de progression du son
        self.slider_son = QSlider(Qt.Horizontal)
        self.slider_son.setRange(0, 100)
        self.slider_son.setGeometry(725, 130, 70, 20)
        self.slider_son.setSingleStep(1)
        self.slider_son.setStyleSheet("border-radius: 10px; height: 1px; margin: 0px; background: black")
        self.slider_son.valueChanged.connect(self.updateSonLabel)
        layout.addWidget(self.slider_son)

        # Ajout de l'icône wifi
        self.icone_wifi = QPushButton()
        self.icone_wifi.setIcon(QIcon("../asset/icone_wifi.png"))
        self.icone_wifi.setIconSize(QtCore.QSize(30, 30))
        self.icone_wifi.setGeometry(660, 176, 70, 50)
        self.icone_wifi.setStyleSheet("background: transparent; color: white")
        self.icone_wifi.clicked.connect(self.go_to_home)
        layout.addWidget(self.icone_wifi)

        # Ajout du toggle pour le wifi
        self.toggle_1 = Toggle(
            animation_curve=QEasingCurve.InOutQuint,
            active_color="green")
        self.toggle_1.setStyleSheet("background: transparent; color: blue")
        self.toggle_1.setCursor(Qt.PointingHandCursor)
        self.toggle_1.setGeometry(725, 195, 70, 20)
        layout.addWidget(self.toggle_1)

        # Ajout de l'icône "nuage"
        self.icone_nuage = QPushButton()
        self.icone_nuage.setIcon(QIcon("../asset/icone_meteo.png"))
        self.icone_nuage.setIconSize(QtCore.QSize(30, 30))
        self.icone_nuage.setGeometry(660, 244, 70, 50)
        self.icone_nuage.setStyleSheet("background: transparent; color: white")
        self.icone_nuage.clicked.connect(self.go_to_home)
        layout.addWidget(self.icone_nuage)

        # Ajout de la barre de progression "nuage"
        self.slider_nuage = QSlider(Qt.Horizontal)
        self.slider_nuage.setRange(0, 100)
        self.slider_nuage.setStyleSheet("color : white")
        self.slider_nuage.setGeometry(725, 260, 70, 20)
        layout.addWidget(self.slider_nuage)

        # Ajout de l'icône "initialisation"
        self.icone_initialisation = QPushButton()
        self.icone_initialisation.setIcon(QIcon("../asset/icone_initialisation.png"))
        self.icone_initialisation.setIconSize(QtCore.QSize(25, 25))
        self.icone_initialisation.setGeometry(660, 293, 70, 60)
        self.icone_initialisation.setStyleSheet("background: transparent; color: white")
        self.icone_initialisation.clicked.connect(self.go_to_home)
        layout.addWidget(self.icone_initialisation)

        # Ajout de la check box pour l'initialisation
        self.check_box = QCheckBox()
        self.check_box.setStyleSheet("background: transparent; color: white")
        self.check_box.setCursor(Qt.PointingHandCursor)
        self.check_box.setGeometry(725, 313, 70, 20)
        layout.addWidget(self.check_box)


        # Ajout des boutons luminosité, son, wifi, meteo
        self.grid_reglages.addWidget(self.button_meteo)
        self.grid_reglages.addWidget(self.temp1)
        self.grid_reglages.addWidget(self.bouton_reglages)
        self.grid_reglages.addWidget(self.icone_luminosité)
        self.grid_reglages.addWidget(self.slider_luminosité)
        self.grid_reglages.addWidget(self.label_luminosité)
        self.grid_reglages.addWidget(self.icone_son)
        self.grid_reglages.addWidget(self.slider_son)
        self.grid_reglages.addWidget(self.label_son)
        self.grid_reglages.addWidget(self.icone_wifi)
        self.grid_reglages.addWidget(self.toggle_1)
        self.grid_reglages.addWidget(self.icone_nuage)
        self.grid_reglages.addWidget(self.slider_nuage)
        self.grid_reglages.addWidget(self.icone_initialisation)
        self.grid_reglages.addWidget(self.check_box)
        self.grid_reglages.addWidget(self.time_label)

    def update_time(self):
        current_time = QTime.currentTime()
        self.time_label.setText(current_time.toString("hh:mm"))

    def go_to_home(self):
        self.main_window.stacked_widget.setCurrentWidget(self.main_window.home)

    def updateLuminositéLabel(self):
        size = self.slider_luminosité.value()
        self.label_luminosité.setText(str(size))

    def updateSonLabel(self):
        size = self.slider_son.value()
        self.label_son.setText(str(size))

    def go_to_meteo(self):
        self.main_window.stacked_widget.setCurrentWidget(self.main_window.meteo)

