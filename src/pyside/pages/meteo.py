# This Python file uses the following encoding: utf-8
from PySide6 import QtCore
from PySide6.QtWidgets import (
    QLabel, QLineEdit, QPushButton,
    QVBoxLayout, QHBoxLayout, QGridLayout, QWidget)
from PySide6.QtGui import QImage, QPixmap
from PySide6.QtCore import QTime, QTimer

import requests
from src.api.weather.weather import Weather

class weather_table_icon():
    icon = ""
    desc = ""
    temp = float
    time = ""
    def __init__(self, icon, desc, temp, time):
        self.icon = icon
        self.desc = desc
        self.temp = temp
        self.time = time


class Weather_table_icon:
    icon = ""
    desc = ""
    temp = float
    time = ""

    def __init__(self, icon, desc, temp, time):
        self.icon = icon
        self.desc = desc
        self.temp = temp
        self.time = time


'''
class Meteo_page(QtWidgets.QWidget):
    city_meteo = "Bordeaux"
    five_day_weather = Weather.get_FiveDayForecast(city_meteo)
    weather_tab = []
    for i in range(0, 3):
<<<<<<< HEAD:src/pyside/pages/meteo.py
        weather_tab.append(Weather_table_icon(five_day_weather.weather.list[i].weather[0].icon,
                                              five_day_weather.weather.list[i].weather[0].description,
                                              five_day_weather.weather.list[i].main.temp,
                                              str(five_day_weather.weather.list[i].dt_txt.hour) + ":" + str(
                                                  five_day_weather.weather.list[i].dt_txt.minute) + "0"))
'''


class Meteo_page(QWidget):
    city_meteo = "Bordeaux"

    weather = Weather()
    five_day_weather = weather.get_FiveDayForecast(city_meteo)
    h0_icon = five_day_weather.weather.list[0].weather[0].icon
    h0_desc = five_day_weather.weather.list[0].weather[0].description
    h0_temp = str(round(five_day_weather.weather.list[0].main.temp))
    h0_time = str(five_day_weather.weather.list[0].dt_txt.hour) + ":" + str(five_day_weather.weather.list[0].dt_txt.minute) + "0"

# Météo H+1 --------------------------------------------------------
    h1_icon = five_day_weather.weather.list[1].weather[0].icon
    h1_desc = five_day_weather.weather.list[1].weather[0].description
    h1_temp = str(round(five_day_weather.weather.list[1].main.temp))
    h1_time = str(five_day_weather.weather.list[1].dt_txt.hour) + ":" + str(five_day_weather.weather.list[1].dt_txt.minute) + "0"
# Météo H+2 --------------------------------------------------------
    h2_icon = five_day_weather.weather.list[2].weather[0].icon
    h2_desc = five_day_weather.weather.list[2].weather[0].description
    h2_temp = str(round(five_day_weather.weather.list[2].main.temp))
    h2_time = str(five_day_weather.weather.list[2].dt_txt.hour) + ":" + str(five_day_weather.weather.list[2].dt_txt.minute) + "0"
# Météo H+3 --------------------------------------------------------
    h3_icon = five_day_weather.weather.list[3].weather[0].icon
    h3_desc = five_day_weather.weather.list[3].weather[0].description
    h3_temp = str(round(five_day_weather.weather.list[3].main.temp))
    h3_time = str(five_day_weather.weather.list[3].dt_txt.hour) + ":" + str(five_day_weather.weather.list[3].dt_txt.minute) + "0"
=======
        weather_tab.append(weather_table_icon(five_day_weather.weather.list[i].weather[0].icon, five_day_weather.weather.list[i].weather[0].description, five_day_weather.weather.list[i].main.temp, str(five_day_weather.weather.list[i].dt_txt.hour) + ":" + str(five_day_weather.weather.list[i].dt_txt.minute) + "0"))
>>>>>>> 5f574fb3c663fdaf219ae1397b00c54fdcb717ab:src/pyside/meteo_page.py

    def __init__(self, main_window):
        super().__init__()
        self.main_window = main_window
        layout = QVBoxLayout()
        self.setLayout(layout)
        self.grid_prevision = QGridLayout()
        self.stacked_widget = main_window.stacked_widget

        # flèche retour
        self.grid_back = QGridLayout()
        self.back_button = QPushButton("<", self)
        self.back_button.setStyleSheet("background: black; color: white; font-size: 28px; font-weight: bold; border: none;")
        self.back_button.setGeometry(10, 9, 20, 20)
        self.back_button.clicked.connect(self.go_to_home)
        layout.addWidget(self.back_button)
        self.grid_back.addWidget(self.back_button)

        # Barre de recherche
        search_layout = QHBoxLayout()
        self.search_input = QLineEdit(self)
        self.search_input.setAlignment(QtCore.Qt.AlignCenter)
        self.search_input.setStyleSheet("color: white; font-weight: bold; font-size: 16px; border: 2px solid white; border-radius: 5px;")
        self.search_input.setFixedSize(250, 30)
        search_layout.addWidget(self.search_input)
        self.search_button = QPushButton("", self)
        self.search_button.setStyleSheet("background: black; border-radius: 5px; border: 2px solid white")
        self.search_button.setGeometry(530, 9, 30, 31)
        layout.addLayout(search_layout)
        layout.setAlignment(search_layout, QtCore.Qt.AlignTop | QtCore.Qt.AlignCenter)
        self.search_button.clicked.connect(self.search)

        # Ajout de l'heure :
        self.grid_time = QGridLayout()
        self.time_label = QLabel()
        self.time_label.setGeometry(635, 0, 50, 50)
        self.time_label.setStyleSheet("QLabel { background: none; color: white; font-size: 14pt; font-weight: bold}")
        layout.addWidget(self.time_label)
        self.timer = QTimer(self)
        self.timer.timeout.connect(self.update_time)
        self.timer.start()
        self.grid_time.addWidget(self.time_label)

        # Météo actuelle :
        url = "http://openweathermap.org/img/wn/" + self.h0_icon + "@2x.png"
        response = requests.get(url)
        img = QImage()
        img.loadFromData(response.content)
        pixmap_meteo = QPixmap.fromImage(img)
        self.img_meteo_direct = QLabel()
        self.img_meteo_direct.setPixmap(pixmap_meteo)
        self.img_meteo_direct.setGeometry(36, 50, 90, 50)
        layout.addWidget(self.img_meteo_direct)

        direct_temp = self.h0_temp + "°C"
        self.temp_meteo_direct = QLabel(direct_temp)
        self.temp_meteo_direct.setStyleSheet("color: white; font-size: 14px; font-weight: bold")
        self.temp_meteo_direct.setGeometry(125, 40, 50, 50)
        layout.addWidget(self.temp_meteo_direct)

        self.time = QLabel(self.h0_time)
        self.time.setStyleSheet("color: white; font-size: 14px; font-weight: bold")
        self.time.setGeometry(65, 100, 90, 90)
        layout.addWidget(self.time)

        self.grid_prevision.addWidget(self.img_meteo_direct)
        self.grid_prevision.addWidget(self.temp_meteo_direct)
        self.grid_prevision.addWidget(self.time)

        # Metéo H+1 :
        url1 = "http://openweathermap.org/img/wn/" + self.h1_icon + "@2x.png"
        response = requests.get(url1)
        img1 = QImage()
        img1.loadFromData(response.content)
        pixmap_meteo_1 = QPixmap.fromImage(img1)
        self.img_meteo_1 = QLabel()
        self.img_meteo_1.setPixmap(pixmap_meteo_1)
        self.img_meteo_1.setGeometry(236, 50, 90, 50)
        layout.addWidget(self.img_meteo_1)

        temp_1 = self.h1_temp + "°C"
        self.temp_meteo_1 = QLabel(temp_1)
        self.temp_meteo_1.setStyleSheet("color: white; font-size: 14px; font-weight: bold")
        self.temp_meteo_1.setGeometry(325, 40, 50, 50)
        layout.addWidget(self.temp_meteo_1)

        self.time_1 = QLabel(self.h1_time)
        self.time_1.setStyleSheet("color: white; font-size: 14px; font-weight: bold")
        self.time_1.setGeometry(265, 100, 90, 90)
        layout.addWidget(self.time_1)

        self.grid_prevision.addWidget(self.img_meteo_1)
        self.grid_prevision.addWidget(self.temp_meteo_1)
        self.grid_prevision.addWidget(self.time_1)

        # Metéo H+2 :
        url2 = "http://openweathermap.org/img/wn/" + self.h2_icon + "@2x.png"
        response = requests.get(url2)
        img2 = QImage()
        img2.loadFromData(response.content)
        pixmap_meteo_2 = QPixmap.fromImage(img2)
        self.img_meteo_2 = QLabel()
        self.img_meteo_2.setPixmap(pixmap_meteo_2)
        self.img_meteo_2.setGeometry(436, 50, 90, 50)
        layout.addWidget(self.img_meteo_2)

        temp_2 = self.h2_temp + "°C"
        self.temp_meteo_2 = QLabel(temp_2)
        self.temp_meteo_2.setStyleSheet("color: white; font-size: 14px; font-weight: bold")
        self.temp_meteo_2.setGeometry(525, 40, 50, 50)
        layout.addWidget(self.temp_meteo_2)

        self.time_2 = QLabel(self.h2_time)
        self.time_2.setStyleSheet("color: white; font-size: 14px; font-weight: bold")
        self.time_2.setGeometry(465, 100, 90, 90)
        layout.addWidget(self.time_2)

        self.grid_prevision.addWidget(self.img_meteo_2)
        self.grid_prevision.addWidget(self.temp_meteo_2)
        self.grid_prevision.addWidget(self.time_2)

        # Metéo H+3 :

        url3 = "http://openweathermap.org/img/wn/" + self.h3_icon + "@2x.png"
        response = requests.get(url3)
        img3 = QImage()
        img3.loadFromData(response.content)
        pixmap_meteo_3 = QPixmap.fromImage(img3)
        self.img_meteo_3 = QLabel()
        self.img_meteo_3.setPixmap(pixmap_meteo_3)
        self.img_meteo_3.setGeometry(636, 50, 90, 50)
        layout.addWidget(self.img_meteo_3)

        temp_3 = self.h3_temp + "°C"
        self.temp_meteo_3 = QLabel(temp_3)
        self.temp_meteo_3.setStyleSheet("color: white; font-size: 14px; font-weight: bold")
        self.temp_meteo_3.setGeometry(725, 40, 50, 50)
        layout.addWidget(self.temp_meteo_3)

        self.time_3 = QLabel(self.h3_time)
        self.time_3.setStyleSheet("color: white; font-size: 14px; font-weight: bold")
        self.time_3.setGeometry(665, 100, 90, 90)
        layout.addWidget(self.time_3)

        self.grid_prevision.addWidget(self.img_meteo_3)
        self.grid_prevision.addWidget(self.temp_meteo_3)
        self.grid_prevision.addWidget(self.time_3)


# Mise à jour de l'heure :
    def update_time(self):
        current_time = QTime.currentTime()
        self.time_label.setText(current_time.toString("hh:mm"))

    def update_weather(self):
        self.h0_icon = self.five_day_weather.weather.list[0].weather[0].icon
        self.h0_desc = self.five_day_weather.weather.list[0].weather[0].description
        self.h0_temp = str(round(self.five_day_weather.weather.list[0].main.temp))
        self.h0_time = str(self.five_day_weather.weather.list[0].dt_txt.hour) + ":" + str(self.five_day_weather.weather.list[0].dt_txt.minute) + "0"

        self.h1_icon = self.five_day_weather.weather.list[1].weather[0].icon
        self.h1_desc = self.five_day_weather.weather.list[1].weather[0].description
        self.h1_temp = str(round(self.five_day_weather.weather.list[1].main.temp))
        self.h1_time = str(self.five_day_weather.weather.list[1].dt_txt.hour) + ":" + str(self.five_day_weather.weather.list[1].dt_txt.minute) + "0"

        self.h2_icon = self.five_day_weather.weather.list[2].weather[0].icon
        self.h2_desc = self.five_day_weather.weather.list[2].weather[0].description
        self.h2_temp = str(round(self.five_day_weather.weather.list[2].main.temp))
        self.h2_time = str(self.five_day_weather.weather.list[2].dt_txt.hour) + ":" + str(self.five_day_weather.weather.list[2].dt_txt.minute) + "0"

        self.h3_icon = self.five_day_weather.weather.list[3].weather[0].icon
        self.h3_desc = self.five_day_weather.weather.list[3].weather[0].description
        self.h3_temp = str(round(self.five_day_weather.weather.list[3].main.temp))
        self.h3_time = str(self.five_day_weather.weather.list[3].dt_txt.hour) + ":" + str(self.five_day_weather.weather.list[3].dt_txt.minute) + "0"

        # update météo 0 :
        url = "http://openweathermap.org/img/wn/" + self.h0_icon + "@2x.png"
        response = requests.get(url)
        img = QImage()
        img.loadFromData(response.content)
        pixmap_meteo = QPixmap.fromImage(img)
        self.img_meteo_direct.setPixmap(pixmap_meteo)
        direct_temp = self.h0_temp + "°C"
        self.temp_meteo_direct.setText(direct_temp)
        self.time.setText(self.h0_time)

        # update météo 1 :
        url1 = "http://openweathermap.org/img/wn/" + self.h1_icon + "@2x.png"
        response = requests.get(url1)
        img1 = QImage()
        img1.loadFromData(response.content)
        pixmap_meteo_1 = QPixmap.fromImage(img1)
        self.img_meteo_1.setPixmap(pixmap_meteo_1)
        temp_1 = self.h1_temp + "°C"
        self.temp_meteo_1.setText(temp_1)
        self.time_1.setText(self.h1_time)

        # update météo 2 :
        url2 = "http://openweathermap.org/img/wn/" + self.h2_icon + "@2x.png"
        response = requests.get(url2)
        img2 = QImage()
        img2.loadFromData(response.content)
        pixmap_meteo_2 = QPixmap.fromImage(img2)
        self.img_meteo_2.setPixmap(pixmap_meteo_2)
        temp_2 = self.h2_temp + "°C"
        self.temp_meteo_2.setText(temp_2)
        self.time_2.setText(self.h2_time)

        # update météo 3 :
        url3 = "http://openweathermap.org/img/wn/" + self.h3_icon + "@2x.png"
        response = requests.get(url3)
        img3 = QImage()
        img3.loadFromData(response.content)
        pixmap_meteo_3 = QPixmap.fromImage(img3)
        self.img_meteo_3.setPixmap(pixmap_meteo_3)
        temp_3 = self.h3_temp + "°C"
        self.temp_meteo_3.setText(temp_3)
        self.time_3.setText(self.h3_time)

    def search(self):
        self.city_meteo = self.search_input.text()
        self.five_day_weather = Weather.get_FiveDayForecast(self, cityName=self.city_meteo)
        self.search_input.clear()
        self.update_weather()

    def go_to_home(self):
        self.main_window.stacked_widget.setCurrentWidget(self.main_window.home)
