import cv2
import requests

from src.api.weather.weather import Weather

from src.pyside.pages.meteo import Meteo_page
from src.pyside.pages.calendar import Calendrier
from src.pyside.pages.home import Home
from src.pyside.pages.clothes import Vetements
from src.pyside.pages.parameters import Parametres


from PySide6 import QtCore
from PySide6.QtWidgets import (
    QLabel, QGridLayout, QStackedWidget,
    QMainWindow, QToolButton)
from PySide6.QtCore import *
from PySide6.QtGui import *

weather = Weather()

class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()
# Style de la Window
        self.setGeometry(200, 200, 800, 600)
        self.setWindowTitle("Smart Mirror")
        self.setWindowIcon(QIcon("logo_equipe.png"))
        self.setStyleSheet("background: black")

        self.stacked_widget = QStackedWidget()
        self.setCentralWidget(self.stacked_widget)

# Création des pages + ajout au QStackedWidget :
        self.home = Home()
        #self.screen2 = Screen2()
        self.vetements = Vetements(self)
        self.calendrier = Calendrier(self)
        self.parametres = Parametres(self)
        self.meteo = Meteo_page(self)
        self.stacked_widget.addWidget(self.home)
        #self.stacked_widget.addWidget(self.screen2)
        self.stacked_widget.addWidget(self.vetements)
        self.stacked_widget.addWidget(self.calendrier)
        self.stacked_widget.addWidget(self.parametres)
        self.stacked_widget.addWidget(self.meteo)
        self.stacked_widget.setCurrentWidget(self.home)


# Ajout de la météo du jour
        self.grid_meteo = QGridLayout()

        currentWeather = weather.get_CurrentWeather("Bordeaux")
        icon = currentWeather.get_weather().weather[0].icon
        url = "http://openweathermap.org/img/wn/" + icon + "@2x.png"
        response = requests.get(url)
        img = QImage()
        img.loadFromData(response.content)
        pixmap_meteo = QPixmap.fromImage(img)
        self.button_meteo = QToolButton()
        self.button_meteo.setIcon(QIcon(pixmap_meteo))
        self.button_meteo.clicked.connect(self.go_to_meteo)
        self.button_meteo.setStyleSheet("background: transparent")
        self.button_meteo.setIconSize(QtCore.QSize(80, 80))
        self.home.layout.addWidget(self.button_meteo)
        self.button_meteo.setGeometry(6, 6, 80, 80)

        temp_actuelle = str(round(currentWeather.get_temp())) + "°C"
        self.temp1 = QLabel(temp_actuelle)
        self.temp1.setStyleSheet("color: white; font-size: 14px; font-weight: bold")
        self.temp1.setGeometry(75, 0, 50, 50)
        self.home.layout.addWidget(self.temp1)

        self.grid_meteo.addWidget(self.button_meteo)
        self.grid_meteo.addWidget(self.temp1)


# Ajout de l'heure :
        self.grid_time = QGridLayout()
        self.time_label = QLabel()
        self.time_label.setGeometry(380, 6, 50, 50)
        self.time_label.setStyleSheet("QLabel { background: none; color: white; font-size: 14pt; font-weight: bold}")
        self.home.layout.addWidget(self.time_label, alignment=QtCore.Qt.AlignTop | QtCore.Qt.AlignCenter)
        self.timer = QTimer(self)
        self.timer.timeout.connect(self.update_time)
        self.timer.start()
        self.grid_time.addWidget(self.time_label)


# Les 3 boutons qui composent le menu :
        # Ajout du bouton paramètres sur la page Home :
        self.grid_menu = QGridLayout()

        self.bouton_parametres = QToolButton()
        self.bouton_parametres.setIcon(QIcon("./asset/icone_reglages.jpg"))
        self.bouton_parametres.clicked.connect(self.go_to_parametres)
        self.bouton_parametres.setStyleSheet("background: transparent; border: 2px solid white; border-radius: 10px")
        self.bouton_parametres.setIconSize(QtCore.QSize(40, 40))
        self.bouton_parametres.setGeometry(745, 18, 50, 50)
        self.home.layout.addWidget(self.bouton_parametres)

        # Ajout du bouton vêtements sur la page Home :
        self.bouton_vetements = QToolButton()
        self.bouton_vetements.setIcon(QIcon("./asset/icone_vetement.webp"))
        self.bouton_vetements.clicked.connect(self.go_to_vetements)
        self.bouton_vetements.setStyleSheet("background: transparent; border: 2px solid white; border-radius: 10px")
        self.bouton_vetements.setIconSize(QtCore.QSize(40, 40))
        self.bouton_vetements.setGeometry(745, 79, 50, 50)
        self.home.layout.addWidget(self.bouton_vetements)

        # Ajout du bouton calendrier sur la page Home :
        self.bouton_calendrier = QToolButton()
        self.bouton_calendrier.setIcon(QIcon("./asset/icone_calendrier.png"))
        self.bouton_calendrier.clicked.connect(self.go_to_calendrier)
        self.bouton_calendrier.setStyleSheet("background: transparent; border: 2px solid white; border-radius: 10px")
        self.bouton_calendrier.setIconSize(QtCore.QSize(40, 40))
        self.bouton_calendrier.setGeometry(745, 140, 50, 50)
        self.home.layout.addWidget(self.bouton_calendrier)

        self.grid_menu.addWidget(self.bouton_parametres)
        self.grid_menu.addWidget(self.bouton_vetements)
        self.grid_menu.addWidget(self.bouton_calendrier)



        # Ajout du contenu de la page Home :
        #self.button = QPushButton("Page suivante")
        #self.button.clicked.connect(self.go_to_screen2)
        #self.button.setStyleSheet("background: grey")
        #self.home.layout.addWidget(self.button, alignment=QtCore.Qt.AlignBottom)

        # Ajout du GridLayout pour le bouton précèdent sur la page caméra
        #self.grid4 = QGridLayout()
        # Ajout du contenu de la page Screen2 :
        #self.button_previous = QPushButton("Page précèdente")
        #self.button_previous.setGeometry(0, 0, 100, 100)
        #self.button_previous.clicked.connect(self.go_to_home)
        #self.button_previous.setStyleSheet("background: grey")
        #self.screen2.layout.addWidget(self.button_previous, alignment=QtCore.Qt.AlignBottom)

        # Ajout du bouton précèdent au 4ème GridLayout sur la page caméra
        #self.grid4.addWidget(self.button_previous)

# -----------------------------------------------------

# -----------------------------------------------------
        # Activation de la capture de la webcam :
        self.cap = cv2.VideoCapture(0)

    def go_to_screen2(self):
        self.stacked_widget.setCurrentWidget(self.screen2)

        # Allumage de la caméra lors du changement de page
        self.timer = QtCore.QTimer(self)
        self.timer.timeout.connect(self.update_frame)
        self.timer.start(1000/30)  # 30 FPS

    def go_to_vetements(self):
        self.stacked_widget.setCurrentWidget(self.vetements)

    def go_to_calendrier(self):
        self.stacked_widget.setCurrentWidget(self.calendrier)

    def go_to_parametres(self):
        self.stacked_widget.setCurrentWidget(self.parametres)

    def go_to_meteo(self):
        self.stacked_widget.setCurrentWidget(self.meteo)

    # Mise à jour de l'heure :
    def update_time(self):
        current_time = QTime.currentTime()
        self.time_label.setText(current_time.toString("hh:mm"))

    # ---------------------------------------------Partie caméra (image en live)--------------------------------------------------------------------
    def update_frame(self):
        ret, frame = self.cap.read()
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        frame = cv2.flip(frame, 1)
        img = QImage(frame, frame.shape[1], frame.shape[0], QImage.Format_RGB888)
        pix = QPixmap.fromImage(img)
        self.screen2.video_frame.setGeometry(0, 0, 800, 600)
        self.screen2.video_frame.setPixmap(pix)
    # ----------------------------------------------------------------------------------------------------------------------------------------------

    def go_to_home(self):
        self.stacked_widget.setCurrentWidget(self.home)

