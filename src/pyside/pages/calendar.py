# This Python file uses the following encoding: utf-8
from src.api.weather.weather import Weather
from src.api.agenda.quickstart import google_event

import requests
import locale
from PySide6 import QtCore
from PySide6.QtWidgets import (
    QLabel, QVBoxLayout, QPushButton, QWidget,
    QGridLayout, QToolButton, QHBoxLayout)
from PySide6.QtCore import *
from PySide6.QtGui import *
from datetime import datetime


class Calendrier(QWidget):
    def __init__(self, main_window):
        super().__init__()

        self.main_window = main_window
        self.stacked_widget = main_window.stacked_widget

        layout = QVBoxLayout()
        self.setLayout(layout)

        # Ajout météo
        weather = Weather()
        currentWeather = weather.get_CurrentWeather("Bordeaux")
        icon = currentWeather.get_weather().weather[0].icon
        url = "http://openweathermap.org/img/wn/" + icon + "@2x.png"
        response = requests.get(url)
        img = QImage()
        img.loadFromData(response.content)
        pixmap_meteo = QPixmap.fromImage(img)
        self.button_meteo = QToolButton()
        self.button_meteo.setIcon(QIcon(pixmap_meteo))
        self.button_meteo.clicked.connect(self.go_to_meteo)
        self.button_meteo.setStyleSheet("background: transparent")
        self.button_meteo.setIconSize(QtCore.QSize(80, 80))
        self.button_meteo.setGeometry(6, 6, 80, 80)
        layout.addWidget(self.button_meteo)

        temp_actuelle = str(round(currentWeather.get_temp())) + "°C"
        self.temp1 = QLabel(temp_actuelle)
        self.temp1.setStyleSheet("color: white; font-size: 14px; font-weight: bold")
        self.temp1.setGeometry(75, 0, 50, 50)
        layout.addWidget(self.temp1)

# Ajout de l'heure :
        self.time_label = QLabel()
        self.time_label.setGeometry(380, 6, 50, 50)
        self.time_label.setStyleSheet("QLabel { background: none; color: white; font-size: 14pt; font-weight: bold}")
        layout.addWidget(self.time_label, alignment=QtCore.Qt.AlignTop | QtCore.Qt.AlignCenter)
        self.timer = QTimer(self)
        self.timer.timeout.connect(self.update_time)
        self.timer.start()

# Ajout d'un menu déroulant
        # Créez un layout vertical pour contenir les boutons
        self.vbox_layout = QVBoxLayout()
        self.vbox_layout.setContentsMargins(0, 0, 0, 0)
        self.vbox_layout.setSpacing(5)
        self.vbox_layout.setAlignment(Qt.AlignTop | Qt.AlignRight)
        layout.addLayout(self.vbox_layout)

        first_element = google_event()
        print(first_element)

        for i in range(len(first_element)):
            date_element = datetime.strptime(first_element[i][0].split(" ")[0], '%Y-%m-%d')
            if date_element.date() == datetime.now().date():
                date = "Aujourd'hui"
            else:
                locale.setlocale(locale.LC_ALL, 'fr_FR.UTF-8')
                date = date_element.strftime("%A") + " " + str(date_element.day) + " " + date_element.strftime("%B")

            # Créez un bouton pour afficher le menu déroulant
            button = QPushButton(date)
            button.setStyleSheet(
                "background: transparent; color: white; font-weight: bold; border: 2px solid white; border-radius: 10px")
            button.setFixedWidth(140)
            button.setFixedHeight(40)
            button.clicked.connect(lambda checked, date=first_element[i][0]: self.show_events(date))
            self.vbox_layout.addWidget(button)

            self.button = button

        # Créez le menu déroulant
        self.menu = QWidget(self)
        self.menu.setStyleSheet("background: transparent; border: 2px solid white; border-radius: 10px")
        self.menu.setGeometry(100, 90, 525, 100)
        self.menu.close()

        # Création d'un layout pour contenir les labels
        self.hbox_layout = QHBoxLayout()
        self.hbox_layout.setContentsMargins(0, 0, 0, 0)
        self.hbox_layout.setSpacing(5)
        self.menu.setLayout(self.hbox_layout)

        # Créez un QLabel vide pour afficher les événements
        self.events_label = QLabel(self.menu)
        self.events_label.setStyleSheet("color: white; font-size: 14px; font-weight: bold; border: None")
        self.events_label.setFixedSize(100, 100)
        self.events_label.setAlignment(Qt.AlignCenter)
        self.events_label.setWordWrap(True)
        self.hbox_layout.addWidget(self.events_label)

        self.grid_calendar = QGridLayout()

        # Ajout des boutons au grid vêtements
        self.grid_calendar.addWidget(self.button_meteo)
        self.grid_calendar.addWidget(self.temp1)
        self.grid_calendar.addWidget(self.time_label)
        self.grid_calendar.addWidget(self.button)

        def show_events(self, date):
            events = []
            for event_list in google_event():
                if event_list[0].startswith(date):
                    events = event_list[1]
                    break

            events_text = ""
            for event in events[:4]:
                event_name = event[16:].replace(" ", "\n")
                events_text += f"<center>{event[11:16]}{event_name}</center><br>"

            self.events_label.setText(events_text)

    def update_time(self):
        current_time = QTime.currentTime()
        self.time_label.setText(current_time.toString("hh:mm"))

    def go_to_meteo(self):
        self.main_window.stacked_widget.setCurrentWidget(self.main_window.meteo)
