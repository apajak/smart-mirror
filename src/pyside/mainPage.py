import sys
import cv2
import requests

from src.api.weather import weather
# from meteo_page import Meteo_page
#from calendrier_page import Calendrier
from Toggle import Toggle

from PySide6 import QtCore
from PySide6.QtWidgets import (
    QApplication, QLabel,
    QVBoxLayout, QPushButton, QWidget,
    QGridLayout, QStackedWidget,
    QMainWindow, QToolButton, QSlider, QCheckBox, QHBoxLayout, QLineEdit)
from PySide6.QtCore import *
from PySide6.QtGui import *
from PySide6.QtUiTools import *

from qtpy.QtCore import Qt, QFileSystemWatcher, QSettings, Signal


class Home(QWidget):
    def __init__(self):
        super().__init__()
        self.layout = QVBoxLayout()
        self.setLayout(self.layout)


class Screen2(QWidget):
    def __init__(self):
        super().__init__()
        # La fenêtre se met en arrière plan
        self.setWindowFlags(QtCore.Qt.FramelessWindowHint)
        self.layout = QVBoxLayout()
        self.video_frame = QLabel()
        self.video_frame.setScaledContents(True)
        self.layout.addWidget(self.video_frame)
        self.setLayout(self.layout)


class Vetements(QWidget):
    def __init__(self, main_window):
        super().__init__()
        self.main_window = main_window

        layout = QVBoxLayout()
        self.setLayout(layout)

        self.stacked_widget = main_window.stacked_widget

# Ajout météo
        currentWeather = Weather.get_CurrentWeather("Bordeaux")
        icon = currentWeather.get_weather().weather[0].icon
        url = "http://openweathermap.org/img/wn/" + icon + "@2x.png"
        response = requests.get(url)
        img = QImage()
        img.loadFromData(response.content)
        pixmap_meteo = QPixmap.fromImage(img)
        self.button_meteo = QToolButton()
        self.button_meteo.setIcon(QIcon(pixmap_meteo))
        self.button_meteo.clicked.connect(self.go_to_meteo)
        self.button_meteo.setStyleSheet("background: transparent")
        self.button_meteo.setIconSize(QtCore.QSize(80, 80))
        self.button_meteo.setGeometry(6, 6, 80, 80)
        layout.addWidget(self.button_meteo)

        temp_actuelle = str(round(currentWeather.get_temp())) + "°C"
        self.temp1 = QLabel(temp_actuelle)
        self.temp1.setStyleSheet("color: white; font-size: 14px; font-weight: bold")
        self.temp1.setGeometry(75, 0, 50, 50)
        layout.addWidget(self.temp1)

# Ajout de l'heure :
        self.grid_time = QGridLayout()
        self.time_label = QLabel()
        self.time_label.setGeometry(380, 6, 50, 50)
        self.time_label.setStyleSheet("QLabel { background: none; color: white; font-size: 14pt; font-weight: bold}")
        layout.addWidget(self.time_label, alignment=QtCore.Qt.AlignTop | QtCore.Qt.AlignCenter)
        self.timer = QTimer(self)
        self.timer.timeout.connect(self.update_time)
        self.timer.start()

# Les boutons qui composent la page des vêtements
        # Ajout du GridLayout des vêtements
        self.grid_vetements = QGridLayout()

        # Ajout du bouton vêtements
        self.bouton_vetements = QPushButton("Vêtements")
        font = self.bouton_vetements.font()
        font.setBold(True)  # Mettre en gras le texte
        self.bouton_vetements.clicked.connect(self.go_to_home)
        self.bouton_vetements.setStyleSheet("background: transparent; color: white; border: 2px solid white; border-radius: 10px")
        self.bouton_vetements.setGeometry(725, 6, 70, 50)
        layout.addWidget(self.bouton_vetements)

        # Ajout du bouton casquette
        self.bouton_casquette = QToolButton()
        self.bouton_casquette.setIcon(QIcon("./icones/icone_casquette.png"))
        self.bouton_casquette.setStyleSheet("background: transparent; border: 2px solid white; border-radius: 10px")
        self.bouton_casquette.setIconSize(QtCore.QSize(40, 40))
        self.bouton_casquette.setGeometry(725, 67, 70, 50)
        layout.addWidget(self.bouton_casquette)

        # Ajout du bouton lunettes
        self.bouton_lunettes = QToolButton()
        self.bouton_lunettes.setIcon(QIcon("./icones/icone_lunette.jpg"))
        self.bouton_lunettes.setStyleSheet("background: transparent; border: 2px solid white; border-radius: 10px")
        self.bouton_lunettes.setIconSize(QtCore.QSize(40, 40))
        self.bouton_lunettes.setGeometry(725, 130, 70, 50)
        layout.addWidget(self.bouton_lunettes)

        # Ajout du bouton pantalons
        self.bouton_pantalon = QToolButton()
        self.bouton_pantalon.setIcon(QIcon("./icones/icone_pantalon.jpg"))
        self.bouton_pantalon.setStyleSheet("background: transparent; border: 2px solid white; border-radius: 10px")
        self.bouton_pantalon.setIconSize(QtCore.QSize(25, 25))
        self.bouton_pantalon.setGeometry(725, 195, 70, 50)
        layout.addWidget(self.bouton_pantalon)

        # Ajout du bouton tshirts
        self.bouton_tshirt = QToolButton()
        self.bouton_tshirt.setIcon(QIcon("./icones/icone_tshirt.jpg"))
        self.bouton_tshirt.setStyleSheet("background: transparent; border: 2px solid white; border-radius: 10px")
        self.bouton_tshirt.setIconSize(QtCore.QSize(25, 25))
        self.bouton_tshirt.setGeometry(725, 260, 70, 50)
        layout.addWidget(self.bouton_tshirt)


        # Ajout des boutons au grid vêtements
        self.grid_vetements.addWidget(self.button_meteo)
        self.grid_vetements.addWidget(self.temp1)
        self.grid_vetements.addWidget(self.bouton_vetements)
        self.grid_vetements.addWidget(self.bouton_casquette)
        self.grid_vetements.addWidget(self.bouton_lunettes)
        self.grid_vetements.addWidget(self.bouton_pantalon)
        self.grid_vetements.addWidget(self.bouton_tshirt)
        self.grid_vetements.addWidget(self.time_label)

    def update_time(self):
        current_time = QTime.currentTime()
        self.time_label.setText(current_time.toString("hh:mm"))

    def go_to_home(self):
        self.main_window.stacked_widget.setCurrentWidget(self.main_window.home)

    def go_to_meteo(self):
        self.main_window.stacked_widget.setCurrentWidget(self.main_window.meteo)        


class Parametres(QWidget):
    def __init__(self, main_window):
        super().__init__()
        self.main_window = main_window

        layout = QVBoxLayout()
        self.setLayout(layout)

        self.stacked_widget = main_window.stacked_widget

# Les boutons qui composent la page réglages
        # Ajout du GridLayout des réglages
        self.grid_reglages = QGridLayout()

        currentWeather = Weather.get_CurrentWeather("Bordeaux")
        icon = currentWeather.get_weather().weather[0].icon
        url = "http://openweathermap.org/img/wn/" + icon + "@2x.png"
        response = requests.get(url)
        img = QImage()
        img.loadFromData(response.content)
        pixmap_meteo = QPixmap.fromImage(img)
        self.button_meteo = QToolButton()
        self.button_meteo.setIcon(QIcon(pixmap_meteo))
        self.button_meteo.clicked.connect(self.go_to_meteo)
        self.button_meteo.setStyleSheet("background: transparent")
        self.button_meteo.setIconSize(QtCore.QSize(80, 80))
        self.button_meteo.setGeometry(6, 6, 80, 80)
        layout.addWidget(self.button_meteo)

        temp_actuelle = str(round(currentWeather.get_temp())) + "°C"
        self.temp1 = QLabel(temp_actuelle)
        self.temp1.setStyleSheet("color: white; font-size: 14px; font-weight: bold")
        self.temp1.setGeometry(75, 0, 50, 50)
        layout.addWidget(self.temp1)

# Ajout de l'heure :
        self.grid_time = QGridLayout()
        self.time_label = QLabel()
        self.time_label.setGeometry(380, 6, 50, 50)
        self.time_label.setStyleSheet("QLabel { background: none; color: white; font-size: 14pt; font-weight: bold}")
        layout.addWidget(self.time_label, alignment=QtCore.Qt.AlignTop | QtCore.Qt.AlignCenter)
        self.timer = QTimer(self)
        self.timer.timeout.connect(self.update_time)
        self.timer.start()

        # Ajout du bouton vêtements
        self.bouton_reglages = QPushButton("Réglages")
        font = self.bouton_reglages.font()
        font.setBold(True)  # Mettre en gras le texte
        self.bouton_reglages.clicked.connect(self.go_to_home)
        self.bouton_reglages.setStyleSheet("background: black; color: white; border: 2px solid white; border-radius: 10px")
        self.bouton_reglages.setGeometry(725, 6, 70, 50)
        layout.addWidget(self.bouton_reglages)

        # Ajout du label pour le niveau de luminosité
        self.label_luminosité = QLabel('0')
        self.label_luminosité.setStyleSheet("color: white")
        self.label_luminosité.setGeometry(658, 51, 20, 50)
        layout.addWidget(self.label_luminosité)

        # Ajout de l'icône luminosité
        self.icone_luminosité = QPushButton()
        self.icone_luminosité.setIcon(QIcon("./icones/icone_luminosité.png"))
        self.icone_luminosité.setIconSize(QtCore.QSize(27, 27))
        self.icone_luminosité.setGeometry(660, 51, 70, 50)
        self.icone_luminosité.setStyleSheet("background: transparent; color: white")
        self.icone_luminosité.clicked.connect(self.go_to_home)
        layout.addWidget(self.icone_luminosité)

        # Ajout de la barre de progression pour la luminosité
        self.slider_luminosité = QSlider(Qt.Horizontal)
        self.slider_luminosité.setRange(0, 100)
        self.slider_luminosité.setGeometry(725, 67, 70, 20)
        self.slider_luminosité.setSingleStep(1)
        self.slider_luminosité.valueChanged.connect(self.updateLuminositéLabel)
        layout.addWidget(self.slider_luminosité)

        # Ajout du label pour le niveau du son
        self.label_son = QLabel('0')
        self.label_son.setStyleSheet("color: white")
        self.label_son.setGeometry(658, 115, 20, 50)
        layout.addWidget(self.label_son)

        # Ajout de l'icône son
        self.icone_son = QPushButton()
        self.icone_son.setIcon(QIcon("./icones/icone_son.png"))
        self.icone_son.setIconSize(QtCore.QSize(26, 26))
        self.icone_son.setGeometry(660, 115, 70, 50)
        self.icone_son.setStyleSheet("background: transparent; color: white")
        self.icone_son.clicked.connect(self.go_to_home)
        layout.addWidget(self.icone_son)

        # Ajout de la barre de progression du son
        self.slider_son = QSlider(Qt.Horizontal)
        self.slider_son.setRange(0, 100)
        self.slider_son.setGeometry(725, 130, 70, 20)
        self.slider_son.setSingleStep(1)
        self.slider_son.setStyleSheet("border-radius: 10px; height: 1px; margin: 0px; background: black")
        self.slider_son.valueChanged.connect(self.updateSonLabel)
        layout.addWidget(self.slider_son)

        # Ajout de l'icône wifi
        self.icone_wifi = QPushButton()
        self.icone_wifi.setIcon(QIcon("./icones/icone_wifi.png"))
        self.icone_wifi.setIconSize(QtCore.QSize(30, 30))
        self.icone_wifi.setGeometry(660, 176, 70, 50)
        self.icone_wifi.setStyleSheet("background: transparent; color: white")
        self.icone_wifi.clicked.connect(self.go_to_home)
        layout.addWidget(self.icone_wifi)

        # Ajout du toggle pour le wifi
        self.toggle_1 = Toggle(
            animation_curve=QEasingCurve.InOutQuint,
            active_color="green")
        self.toggle_1.setStyleSheet("background: transparent; color: blue")
        self.toggle_1.setCursor(Qt.PointingHandCursor)
        self.toggle_1.setGeometry(725, 195, 70, 20)
        layout.addWidget(self.toggle_1)

        # Ajout de l'icône "nuage"
        self.icone_nuage = QPushButton()
        self.icone_nuage.setIcon(QIcon("./icones/icone_meteo.png"))
        self.icone_nuage.setIconSize(QtCore.QSize(30, 30))
        self.icone_nuage.setGeometry(660, 244, 70, 50)
        self.icone_nuage.setStyleSheet("background: transparent; color: white")
        self.icone_nuage.clicked.connect(self.go_to_home)
        layout.addWidget(self.icone_nuage)

        # Ajout de la barre de progression "nuage"
        self.slider_nuage = QSlider(Qt.Horizontal)
        self.slider_nuage.setRange(0, 100)
        self.slider_nuage.setStyleSheet("color : white")
        self.slider_nuage.setGeometry(725, 260, 70, 20)
        layout.addWidget(self.slider_nuage)

        # Ajout de l'icône "initialisation"
        self.icone_initialisation = QPushButton()
        self.icone_initialisation.setIcon(QIcon("./icones/icone_initialisation.png"))
        self.icone_initialisation.setIconSize(QtCore.QSize(25, 25))
        self.icone_initialisation.setGeometry(660, 293, 70, 60)
        self.icone_initialisation.setStyleSheet("background: transparent; color: white")
        self.icone_initialisation.clicked.connect(self.go_to_home)
        layout.addWidget(self.icone_initialisation)

        # Ajout de la check box pour l'initialisation
        self.check_box = QCheckBox()
        self.check_box.setStyleSheet("background: transparent; color: white")
        self.check_box.setCursor(Qt.PointingHandCursor)
        self.check_box.setGeometry(725, 313, 70, 20)
        layout.addWidget(self.check_box)


        # Ajout des boutons luminosité, son, wifi, meteo
        self.grid_reglages.addWidget(self.button_meteo)
        self.grid_reglages.addWidget(self.temp1)
        self.grid_reglages.addWidget(self.bouton_reglages)
        self.grid_reglages.addWidget(self.icone_luminosité)
        self.grid_reglages.addWidget(self.slider_luminosité)
        self.grid_reglages.addWidget(self.label_luminosité)
        self.grid_reglages.addWidget(self.icone_son)
        self.grid_reglages.addWidget(self.slider_son)
        self.grid_reglages.addWidget(self.label_son)
        self.grid_reglages.addWidget(self.icone_wifi)
        self.grid_reglages.addWidget(self.toggle_1)
        self.grid_reglages.addWidget(self.icone_nuage)
        self.grid_reglages.addWidget(self.slider_nuage)
        self.grid_reglages.addWidget(self.icone_initialisation)
        self.grid_reglages.addWidget(self.check_box)
        self.grid_reglages.addWidget(self.time_label)

    def update_time(self):
        current_time = QTime.currentTime()
        self.time_label.setText(current_time.toString("hh:mm"))

    def go_to_home(self):
        self.main_window.stacked_widget.setCurrentWidget(self.main_window.home)

    def updateLuminositéLabel(self):
        size = self.slider_luminosité.value()
        self.label_luminosité.setText(str(size))

    def updateSonLabel(self):
        size = self.slider_son.value()
        self.label_son.setText(str(size))

    def go_to_meteo(self):
        self.main_window.stacked_widget.setCurrentWidget(self.main_window.meteo)


class Meteo_page(QWidget):
    city_meteo = "Bordeaux"

    five_day_weather = Weather.get_FiveDayForecast(city_meteo)
    h0_icon = five_day_weather.weather.list[0].weather[0].icon
    h0_desc = five_day_weather.weather.list[0].weather[0].description
    h0_temp = str(round(five_day_weather.weather.list[0].main.temp))
    h0_time = str(five_day_weather.weather.list[0].dt_txt.hour) + ":" + str(five_day_weather.weather.list[0].dt_txt.minute) + "0"

# Météo H+1 --------------------------------------------------------
    h1_icon = five_day_weather.weather.list[1].weather[0].icon
    h1_desc = five_day_weather.weather.list[1].weather[0].description
    h1_temp = str(round(five_day_weather.weather.list[1].main.temp))
    h1_time = str(five_day_weather.weather.list[1].dt_txt.hour) + ":" + str(five_day_weather.weather.list[1].dt_txt.minute) + "0"
# Météo H+2 --------------------------------------------------------
    h2_icon = five_day_weather.weather.list[2].weather[0].icon
    h2_desc = five_day_weather.weather.list[2].weather[0].description
    h2_temp = str(round(five_day_weather.weather.list[2].main.temp))
    h2_time = str(five_day_weather.weather.list[2].dt_txt.hour) + ":" + str(five_day_weather.weather.list[2].dt_txt.minute) + "0"
# Météo H+3 --------------------------------------------------------
    h3_icon = five_day_weather.weather.list[3].weather[0].icon
    h3_desc = five_day_weather.weather.list[3].weather[0].description
    h3_temp = str(round(five_day_weather.weather.list[3].main.temp))
    h3_time = str(five_day_weather.weather.list[3].dt_txt.hour) + ":" + str(five_day_weather.weather.list[3].dt_txt.minute) + "0"

    def __init__(self, main_window):
        super().__init__()
        self.main_window = main_window
        layout = QVBoxLayout()
        self.setLayout(layout)
        self.grid_prevision = QGridLayout()
        self.stacked_widget = main_window.stacked_widget

        # flèche retour
        self.grid_back = QGridLayout()
        self.back_button = QPushButton("<", self)
        self.back_button.setStyleSheet("background: black; color: white; font-size: 28px; font-weight: bold; border: none;")
        self.back_button.setGeometry(10, 9, 20, 20)
        self.back_button.clicked.connect(self.go_to_home)
        layout.addWidget(self.back_button)
        self.grid_back.addWidget(self.back_button)

        # Barre de recherche
        search_layout = QHBoxLayout()
        self.search_input = QLineEdit(self)
        self.search_input.setAlignment(QtCore.Qt.AlignCenter)
        self.search_input.setStyleSheet("color: white; font-weight: bold; font-size: 16px; border: 2px solid white; border-radius: 5px;")
        self.search_input.setFixedSize(250, 30)
        search_layout.addWidget(self.search_input)
        self.search_button = QPushButton("", self)
        self.search_button.setStyleSheet("background: black; border-radius: 5px; border: 2px solid white")
        self.search_button.setGeometry(530, 9, 30, 31)
        layout.addLayout(search_layout)
        layout.setAlignment(search_layout, QtCore.Qt.AlignTop | QtCore.Qt.AlignCenter)
        self.search_button.clicked.connect(self.search)

        # Ajout de l'heure :
        self.grid_time = QGridLayout()
        self.time_label = QLabel()
        self.time_label.setGeometry(635, 0, 50, 50)
        self.time_label.setStyleSheet("QLabel { background: none; color: white; font-size: 14pt; font-weight: bold}")
        layout.addWidget(self.time_label)
        self.timer = QTimer(self)
        self.timer.timeout.connect(self.update_time)
        self.timer.start()
        self.grid_time.addWidget(self.time_label)

        # Météo actuelle :
        url = "http://openweathermap.org/img/wn/" + self.h0_icon + "@2x.png"
        response = requests.get(url)
        img = QImage()
        img.loadFromData(response.content)
        pixmap_meteo = QPixmap.fromImage(img)
        self.img_meteo_direct = QLabel()
        self.img_meteo_direct.setPixmap(pixmap_meteo)
        self.img_meteo_direct.setGeometry(36, 50, 90, 50)
        layout.addWidget(self.img_meteo_direct)

        direct_temp = self.h0_temp + "°C"
        self.temp_meteo_direct = QLabel(direct_temp)
        self.temp_meteo_direct.setStyleSheet("color: white; font-size: 14px; font-weight: bold")
        self.temp_meteo_direct.setGeometry(125, 40, 50, 50)
        layout.addWidget(self.temp_meteo_direct)

        self.time = QLabel(self.h0_time)
        self.time.setStyleSheet("color: white; font-size: 14px; font-weight: bold")
        self.time.setGeometry(65, 100, 90, 90)
        layout.addWidget(self.time)

        self.grid_prevision.addWidget(self.img_meteo_direct)
        self.grid_prevision.addWidget(self.temp_meteo_direct)
        self.grid_prevision.addWidget(self.time)

        # Metéo H+1 :
        url1 = "http://openweathermap.org/img/wn/" + self.h1_icon + "@2x.png"
        response = requests.get(url1)
        img1 = QImage()
        img1.loadFromData(response.content)
        pixmap_meteo_1 = QPixmap.fromImage(img1)
        self.img_meteo_1 = QLabel()
        self.img_meteo_1.setPixmap(pixmap_meteo_1)
        self.img_meteo_1.setGeometry(236, 50, 90, 50)
        layout.addWidget(self.img_meteo_1)

        temp_1 = self.h1_temp + "°C"
        self.temp_meteo_1 = QLabel(temp_1)
        self.temp_meteo_1.setStyleSheet("color: white; font-size: 14px; font-weight: bold")
        self.temp_meteo_1.setGeometry(325, 40, 50, 50)
        layout.addWidget(self.temp_meteo_1)

        self.time_1 = QLabel(self.h1_time)
        self.time_1.setStyleSheet("color: white; font-size: 14px; font-weight: bold")
        self.time_1.setGeometry(265, 100, 90, 90)
        layout.addWidget(self.time_1)

        self.grid_prevision.addWidget(self.img_meteo_1)
        self.grid_prevision.addWidget(self.temp_meteo_1)
        self.grid_prevision.addWidget(self.time_1)

        # Metéo H+2 :
        url2 = "http://openweathermap.org/img/wn/" + self.h2_icon + "@2x.png"
        response = requests.get(url2)
        img2 = QImage()
        img2.loadFromData(response.content)
        pixmap_meteo_2 = QPixmap.fromImage(img2)
        self.img_meteo_2 = QLabel()
        self.img_meteo_2.setPixmap(pixmap_meteo_2)
        self.img_meteo_2.setGeometry(436, 50, 90, 50)
        layout.addWidget(self.img_meteo_2)

        temp_2 = self.h2_temp + "°C"
        self.temp_meteo_2 = QLabel(temp_2)
        self.temp_meteo_2.setStyleSheet("color: white; font-size: 14px; font-weight: bold")
        self.temp_meteo_2.setGeometry(525, 40, 50, 50)
        layout.addWidget(self.temp_meteo_2)

        self.time_2 = QLabel(self.h2_time)
        self.time_2.setStyleSheet("color: white; font-size: 14px; font-weight: bold")
        self.time_2.setGeometry(465, 100, 90, 90)
        layout.addWidget(self.time_2)

        self.grid_prevision.addWidget(self.img_meteo_2)
        self.grid_prevision.addWidget(self.temp_meteo_2)
        self.grid_prevision.addWidget(self.time_2)

        # Metéo H+3 :
        url3 = "http://openweathermap.org/img/wn/" + self.h3_icon + "@2x.png"
        response = requests.get(url3)
        img3 = QImage()
        img3.loadFromData(response.content)
        pixmap_meteo_3 = QPixmap.fromImage(img3)
        self.img_meteo_3 = QLabel()
        self.img_meteo_3.setPixmap(pixmap_meteo_3)
        self.img_meteo_3.setGeometry(636, 50, 90, 50)
        layout.addWidget(self.img_meteo_3)

        temp_3 = self.h3_temp + "°C"
        self.temp_meteo_3 = QLabel(temp_3)
        self.temp_meteo_3.setStyleSheet("color: white; font-size: 14px; font-weight: bold")
        self.temp_meteo_3.setGeometry(725, 40, 50, 50)
        layout.addWidget(self.temp_meteo_3)

        self.time_3 = QLabel(self.h3_time)
        self.time_3.setStyleSheet("color: white; font-size: 14px; font-weight: bold")
        self.time_3.setGeometry(665, 100, 90, 90)
        layout.addWidget(self.time_3)

        self.grid_prevision.addWidget(self.img_meteo_3)
        self.grid_prevision.addWidget(self.temp_meteo_3)
        self.grid_prevision.addWidget(self.time_3)

# Mise à jour de l'heure :
    def update_time(self):
        current_time = QTime.currentTime()
        self.time_label.setText(current_time.toString("hh:mm"))

    def update_weather(self):
        self.h0_icon = self.five_day_weather.weather.list[0].weather[0].icon
        self.h0_desc = self.five_day_weather.weather.list[0].weather[0].description
        self.h0_temp = str(round(self.five_day_weather.weather.list[0].main.temp))
        self.h0_time = str(self.five_day_weather.weather.list[0].dt_txt.hour) + ":" + str(self.five_day_weather.weather.list[0].dt_txt.minute) + "0"

        self.h1_icon = self.five_day_weather.weather.list[1].weather[0].icon
        self.h1_desc = self.five_day_weather.weather.list[1].weather[0].description
        self.h1_temp = str(round(self.five_day_weather.weather.list[1].main.temp))
        self.h1_time = str(self.five_day_weather.weather.list[1].dt_txt.hour) + ":" + str(self.five_day_weather.weather.list[1].dt_txt.minute) + "0"

        self.h2_icon = self.five_day_weather.weather.list[2].weather[0].icon
        self.h2_desc = self.five_day_weather.weather.list[2].weather[0].description
        self.h2_temp = str(round(self.five_day_weather.weather.list[2].main.temp))
        self.h2_time = str(self.five_day_weather.weather.list[2].dt_txt.hour) + ":" + str(self.five_day_weather.weather.list[2].dt_txt.minute) + "0"

        self.h3_icon = self.five_day_weather.weather.list[3].weather[0].icon
        self.h3_desc = self.five_day_weather.weather.list[3].weather[0].description
        self.h3_temp = str(round(self.five_day_weather.weather.list[3].main.temp))
        self.h3_time = str(self.five_day_weather.weather.list[3].dt_txt.hour) + ":" + str(self.five_day_weather.weather.list[3].dt_txt.minute) + "0"

        # update météo 0 :
        url = "http://openweathermap.org/img/wn/" + self.h0_icon + "@2x.png"
        response = requests.get(url)
        img = QImage()
        img.loadFromData(response.content)
        pixmap_meteo = QPixmap.fromImage(img)
        self.img_meteo_direct.setPixmap(pixmap_meteo)
        direct_temp = self.h0_temp + "°C"
        self.temp_meteo_direct.setText(direct_temp)
        self.time.setText(self.h0_time)

        # update météo 1 :
        url1 = "http://openweathermap.org/img/wn/" + self.h1_icon + "@2x.png"
        response = requests.get(url1)
        img1 = QImage()
        img1.loadFromData(response.content)
        pixmap_meteo_1 = QPixmap.fromImage(img1)
        self.img_meteo_1.setPixmap(pixmap_meteo_1)
        temp_1 = self.h1_temp + "°C"
        self.temp_meteo_1.setText(temp_1)
        self.time_1.setText(self.h1_time)

        # update météo 2 :
        url2 = "http://openweathermap.org/img/wn/" + self.h2_icon + "@2x.png"
        response = requests.get(url2)
        img2 = QImage()
        img2.loadFromData(response.content)
        pixmap_meteo_2 = QPixmap.fromImage(img2)
        self.img_meteo_2.setPixmap(pixmap_meteo_2)
        temp_2 = self.h2_temp + "°C"
        self.temp_meteo_2.setText(temp_2)
        self.time_2.setText(self.h2_time)

        # update météo 3 :
        url3 = "http://openweathermap.org/img/wn/" + self.h3_icon + "@2x.png"
        response = requests.get(url3)
        img3 = QImage()
        img3.loadFromData(response.content)
        pixmap_meteo_3 = QPixmap.fromImage(img3)
        self.img_meteo_3.setPixmap(pixmap_meteo_3)
        temp_3 = self.h3_temp + "°C"
        self.temp_meteo_3.setText(temp_3)
        self.time_3.setText(self.h3_time)

    def search(self):
        self.city_meteo = self.search_input.text()
        self.five_day_weather = Weather.get_FiveDayForecast(self.city_meteo)
        self.search_input.clear()
        self.update_weather()

    def go_to_home(self):
        self.main_window.stacked_widget.setCurrentWidget(self.main_window.home)


class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()
# Style de la Window
        self.setGeometry(200, 200, 800, 600)
        self.setWindowTitle("Smart Mirror")
        self.setWindowIcon(QIcon("logo_equipe.png"))
        self.setStyleSheet("background: black")

        self.stacked_widget = QStackedWidget()
        self.setCentralWidget(self.stacked_widget)

# Création des pages + ajout au QStackedWidget :
        self.home = Home()
        self.screen2 = Screen2()
        self.vetements = Vetements(self)
        self.calendrier = Calendrier()
        self.parametres = Parametres(self)
        self.meteo = Meteo_page(self)
        self.stacked_widget.addWidget(self.home)
        self.stacked_widget.addWidget(self.screen2)
        self.stacked_widget.addWidget(self.vetements)
        self.stacked_widget.addWidget(self.calendrier)
        self.stacked_widget.addWidget(self.parametres)
        self.stacked_widget.addWidget(self.meteo)
        self.stacked_widget.setCurrentWidget(self.home)


# Ajout de la météo du jour
        self.grid_meteo = QGridLayout()

        currentWeather = Weather.get_CurrentWeather("Bordeaux")
        icon = currentWeather.get_weather().weather[0].icon
        url = "http://openweathermap.org/img/wn/" + icon + "@2x.png"
        response = requests.get(url)
        img = QImage()
        img.loadFromData(response.content)
        pixmap_meteo = QPixmap.fromImage(img)
        self.button_meteo = QToolButton()
        self.button_meteo.setIcon(QIcon(pixmap_meteo))
        self.button_meteo.clicked.connect(self.go_to_meteo)
        self.button_meteo.setStyleSheet("background: transparent")
        self.button_meteo.setIconSize(QtCore.QSize(80, 80))
        self.home.layout.addWidget(self.button_meteo)
        self.button_meteo.setGeometry(6, 6, 80, 80)

        temp_actuelle = str(round(currentWeather.get_temp())) + "°C"
        self.temp1 = QLabel(temp_actuelle)
        self.temp1.setStyleSheet("color: white; font-size: 14px; font-weight: bold")
        self.temp1.setGeometry(75, 0, 50, 50)
        self.home.layout.addWidget(self.temp1)

        self.grid_meteo.addWidget(self.button_meteo)
        self.grid_meteo.addWidget(self.temp1)


# Ajout de l'heure :
        self.grid_time = QGridLayout()
        self.time_label = QLabel()
        self.time_label.setGeometry(380, 6, 50, 50)
        self.time_label.setStyleSheet("QLabel { background: none; color: white; font-size: 14pt; font-weight: bold}")
        self.home.layout.addWidget(self.time_label, alignment=QtCore.Qt.AlignTop | QtCore.Qt.AlignCenter)
        self.timer = QTimer(self)
        self.timer.timeout.connect(self.update_time)
        self.timer.start()
        self.grid_time.addWidget(self.time_label)


# Les 3 boutons qui composent le menu :
        # Ajout du bouton paramètres sur la page Home :
        self.grid_menu = QGridLayout()

        self.bouton_parametres = QToolButton()
        self.bouton_parametres.setIcon(QIcon("icone_reglages.jpg"))
        self.bouton_parametres.clicked.connect(self.go_to_parametres)
        self.bouton_parametres.setStyleSheet("background: transparent; border: 2px solid white; border-radius: 10px")
        self.bouton_parametres.setIconSize(QtCore.QSize(40, 40))
        self.bouton_parametres.setGeometry(745, 18, 50, 50)
        self.home.layout.addWidget(self.bouton_parametres)

        # Ajout du bouton vêtements sur la page Home :
        self.bouton_vetements = QToolButton()
        self.bouton_vetements.setIcon(QIcon("icone_vetement.webp"))
        self.bouton_vetements.clicked.connect(self.go_to_vetements)
        self.bouton_vetements.setStyleSheet("background: transparent; border: 2px solid white; border-radius: 10px")
        self.bouton_vetements.setIconSize(QtCore.QSize(40, 40))
        self.bouton_vetements.setGeometry(745, 79, 50, 50)
        self.home.layout.addWidget(self.bouton_vetements)

        # Ajout du bouton calendrier sur la page Home :
        self.bouton_calendrier = QToolButton()
        self.bouton_calendrier.setIcon(QIcon("icone_calendrier.png"))
        self.bouton_calendrier.clicked.connect(self.go_to_calendrier)
        self.bouton_calendrier.setStyleSheet("background: transparent; border: 2px solid white; border-radius: 10px")
        self.bouton_calendrier.setIconSize(QtCore.QSize(40, 40))
        self.bouton_calendrier.setGeometry(745, 140, 50, 50)
        self.home.layout.addWidget(self.bouton_calendrier)

        self.grid_menu.addWidget(self.bouton_parametres)
        self.grid_menu.addWidget(self.bouton_vetements)
        self.grid_menu.addWidget(self.bouton_calendrier)



        # Ajout du contenu de la page Home :
        #self.button = QPushButton("Page suivante")
        #self.button.clicked.connect(self.go_to_screen2)
        #self.button.setStyleSheet("background: grey")
        #self.home.layout.addWidget(self.button, alignment=QtCore.Qt.AlignBottom)

        # Ajout du GridLayout pour le bouton précèdent sur la page caméra
        #self.grid4 = QGridLayout()
        # Ajout du contenu de la page Screen2 :
        #self.button_previous = QPushButton("Page précèdente")
        #self.button_previous.setGeometry(0, 0, 100, 100)
        #self.button_previous.clicked.connect(self.go_to_home)
        #self.button_previous.setStyleSheet("background: grey")
        #self.screen2.layout.addWidget(self.button_previous, alignment=QtCore.Qt.AlignBottom)

        # Ajout du bouton précèdent au 4ème GridLayout sur la page caméra
        #self.grid4.addWidget(self.button_previous)

# -----------------------------------------------------

# -----------------------------------------------------
        # Activation de la capture de la webcam :
        self.cap = cv2.VideoCapture(0)

    def go_to_screen2(self):
        self.stacked_widget.setCurrentWidget(self.screen2)

        # Allumage de la caméra lors du changement de page
        self.timer = QtCore.QTimer(self)
        self.timer.timeout.connect(self.update_frame)
        self.timer.start(1000/30)  # 30 FPS

    def go_to_vetements(self):
        self.stacked_widget.setCurrentWidget(self.vetements)

    def go_to_calendrier(self):
        self.stacked_widget.setCurrentWidget(self.calendrier)

    def go_to_parametres(self):
        self.stacked_widget.setCurrentWidget(self.parametres)

    def go_to_meteo(self):
        self.stacked_widget.setCurrentWidget(self.meteo)

    # Mise à jour de l'heure :
    def update_time(self):
        current_time = QTime.currentTime()
        self.time_label.setText(current_time.toString("hh:mm"))

    # ---------------------------------------------Partie caméra (image en live)--------------------------------------------------------------------
    def update_frame(self):
        ret, frame = self.cap.read()
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        frame = cv2.flip(frame, 1)
        img = QImage(frame, frame.shape[1], frame.shape[0], QImage.Format_RGB888)
        pix = QPixmap.fromImage(img)
        self.screen2.video_frame.setGeometry(0, 0, 800, 600)
        self.screen2.video_frame.setPixmap(pix)
    # ----------------------------------------------------------------------------------------------------------------------------------------------

    def go_to_home(self):
        self.stacked_widget.setCurrentWidget(self.home)


if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = MainWindow()
    window.show()
    sys.exit(app.exec())