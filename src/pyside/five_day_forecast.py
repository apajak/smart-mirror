#!/usr/bin/env python3 
# -*- coding: utf-8 -*-
#----------------------------------------------------------------------------
# Created By  : Pajak Alexandre
# Created Date: 01/10/2021
# version ='1.0'
# ---------------------------------------------------------------------------
""" Module for Manage OpenWeather Five day Weather prediction. """
# ---------------------------------------------------------------------------
# Imports
# ---------------------------------------------------------------------------
from dotenv import load_dotenv
import os
import requests
import Direct_Geocoding
from typing import Any, List, TypeVar, Type, cast, Callable
from datetime import datetime
import dateutil.parser


# global variables
load_dotenv()
API_KEY = os.getenv("API_KEY")


T = TypeVar("T")


def from_float(x: Any) -> float:
    assert isinstance(x, (float, int)) and not isinstance(x, bool)
    return float(x)


def to_float(x: Any) -> float:
    assert isinstance(x, float)
    return x


def from_int(x: Any) -> int:
    assert isinstance(x, int) and not isinstance(x, bool)
    return x


def from_str(x: Any) -> str:
    assert isinstance(x, str)
    return x


def to_class(c: Type[T], x: Any) -> dict:
    assert isinstance(x, c)
    return cast(Any, x).to_dict()


def from_list(f: Callable[[Any], T], x: Any) -> List[T]:
    assert isinstance(x, list)
    return [f(y) for y in x]


def from_datetime(x: Any) -> datetime:
    return dateutil.parser.parse(x)


class Coord:
    lat: float
    lon: float

    def __init__(self, lat: float, lon: float) -> None:
        self.lat = lat
        self.lon = lon

    @staticmethod
    def from_dict(obj: Any) -> "Coord":
        assert isinstance(obj, dict)
        lat = from_float(obj.get("lat"))
        lon = from_float(obj.get("lon"))
        return Coord(lat, lon)

    def to_dict(self) -> dict:
        result: dict = {}
        result["lat"] = to_float(self.lat)
        result["lon"] = to_float(self.lon)
        return result


class City:
    id: int
    name: str
    coord: Coord
    country: str
    population: int
    timezone: int
    sunrise: int
    sunset: int

    def __init__(
        self,
        id: int,
        name: str,
        coord: Coord,
        country: str,
        population: int,
        timezone: int,
        sunrise: int,
        sunset: int,
    ) -> None:
        self.id = id
        self.name = name
        self.coord = coord
        self.country = country
        self.population = population
        self.timezone = timezone
        self.sunrise = sunrise
        self.sunset = sunset

    @staticmethod
    def from_dict(obj: Any) -> "City":
        assert isinstance(obj, dict)
        id = from_int(obj.get("id"))
        name = from_str(obj.get("name"))
        coord = Coord.from_dict(obj.get("coord"))
        country = from_str(obj.get("country"))
        population = from_int(obj.get("population"))
        timezone = from_int(obj.get("timezone"))
        sunrise = from_int(obj.get("sunrise"))
        sunset = from_int(obj.get("sunset"))
        return City(id, name, coord, country, population, timezone, sunrise, sunset)

    def to_dict(self) -> dict:
        result: dict = {}
        result["id"] = from_int(self.id)
        result["name"] = from_str(self.name)
        result["coord"] = to_class(Coord, self.coord)
        result["country"] = from_str(self.country)
        result["population"] = from_int(self.population)
        result["timezone"] = from_int(self.timezone)
        result["sunrise"] = from_int(self.sunrise)
        result["sunset"] = from_int(self.sunset)
        return result


class Clouds:
    all: int

    def __init__(self, all: int) -> None:
        self.all = all

    @staticmethod
    def from_dict(obj: Any) -> "Clouds":
        assert isinstance(obj, dict)
        all = from_int(obj.get("all"))
        return Clouds(all)

    def to_dict(self) -> dict:
        result: dict = {}
        result["all"] = from_int(self.all)
        return result


class Main:
    temp: float
    feels_like: float
    temp_min: float
    temp_max: float
    pressure: int
    sea_level: int
    grnd_level: int
    humidity: int
    temp_kf: float

    def __init__(
        self,
        temp: float,
        feels_like: float,
        temp_min: float,
        temp_max: float,
        pressure: int,
        sea_level: int,
        grnd_level: int,
        humidity: int,
        temp_kf: float,
    ) -> None:
        self.temp = temp
        self.feels_like = feels_like
        self.temp_min = temp_min
        self.temp_max = temp_max
        self.pressure = pressure
        self.sea_level = sea_level
        self.grnd_level = grnd_level
        self.humidity = humidity
        self.temp_kf = temp_kf

    @staticmethod
    def from_dict(obj: Any) -> "Main":
        assert isinstance(obj, dict)
        temp = from_float(obj.get("temp"))
        feels_like = from_float(obj.get("feels_like"))
        temp_min = from_float(obj.get("temp_min"))
        temp_max = from_float(obj.get("temp_max"))
        pressure = from_int(obj.get("pressure"))
        sea_level = from_int(obj.get("sea_level"))
        grnd_level = from_int(obj.get("grnd_level"))
        humidity = from_int(obj.get("humidity"))
        temp_kf = from_float(obj.get("temp_kf"))
        return Main(
            temp,
            feels_like,
            temp_min,
            temp_max,
            pressure,
            sea_level,
            grnd_level,
            humidity,
            temp_kf,
        )

    def to_dict(self) -> dict:
        result: dict = {}
        result["temp"] = to_float(self.temp)
        result["feels_like"] = to_float(self.feels_like)
        result["temp_min"] = to_float(self.temp_min)
        result["temp_max"] = to_float(self.temp_max)
        result["pressure"] = from_int(self.pressure)
        result["sea_level"] = from_int(self.sea_level)
        result["grnd_level"] = from_int(self.grnd_level)
        result["humidity"] = from_int(self.humidity)
        result["temp_kf"] = to_float(self.temp_kf)
        return result


class Sys:
    pod: str

    def __init__(self, pod: str) -> None:
        self.pod = pod

    @staticmethod
    def from_dict(obj: Any) -> "Sys":
        assert isinstance(obj, dict)
        pod = from_str(obj.get("pod"))
        return Sys(pod)

    def to_dict(self) -> dict:
        result: dict = {}
        result["pod"] = from_str(self.pod)
        return result


class Weather:
    id: int
    main: str
    description: str
    icon: str

    def __init__(self, id: int, main: str, description: str, icon: str) -> None:
        self.id = id
        self.main = main
        self.description = description
        self.icon = icon

    @staticmethod
    def from_dict(obj: Any) -> "Weather":
        assert isinstance(obj, dict)
        id = from_int(obj.get("id"))
        main = from_str(obj.get("main"))
        description = from_str(obj.get("description"))
        icon = from_str(obj.get("icon"))
        return Weather(id, main, description, icon)

    def to_dict(self) -> dict:
        result: dict = {}
        result["id"] = from_int(self.id)
        result["main"] = from_str(self.main)
        result["description"] = from_str(self.description)
        result["icon"] = from_str(self.icon)
        return result


class Wind:
    speed: float
    deg: int
    gust: float

    def __init__(self, speed: float, deg: int, gust: float) -> None:
        self.speed = speed
        self.deg = deg
        self.gust = gust

    @staticmethod
    def from_dict(obj: Any) -> "Wind":
        assert isinstance(obj, dict)
        speed = from_float(obj.get("speed"))
        deg = from_int(obj.get("deg"))
        gust = from_float(obj.get("gust"))
        return Wind(speed, deg, gust)

    def to_dict(self) -> dict:
        result: dict = {}
        result["speed"] = to_float(self.speed)
        result["deg"] = from_int(self.deg)
        result["gust"] = to_float(self.gust)
        return result


class ListElement:
    dt: int
    main: Main
    weather: List[Weather]
    clouds: Clouds
    wind: Wind
    visibility: int
    pop: float
    sys: Sys
    dt_txt: datetime

    def __init__(
        self,
        dt: int,
        main: Main,
        weather: List[Weather],
        clouds: Clouds,
        wind: Wind,
        visibility: int,
        pop: float,
        sys: Sys,
        dt_txt: datetime,
    ) -> None:
        self.dt = dt
        self.main = main
        self.weather = weather
        self.clouds = clouds
        self.wind = wind
        self.visibility = visibility
        self.pop = pop
        self.sys = sys
        self.dt_txt = dt_txt

    @staticmethod
    def from_dict(obj: Any) -> "ListElement":
        assert isinstance(obj, dict)
        dt = from_int(obj.get("dt"))
        main = Main.from_dict(obj.get("main"))
        weather = from_list(Weather.from_dict, obj.get("weather"))
        clouds = Clouds.from_dict(obj.get("clouds"))
        wind = Wind.from_dict(obj.get("wind"))
        visibility = from_int(obj.get("visibility"))
        pop = from_float(obj.get("pop"))
        sys = Sys.from_dict(obj.get("sys"))
        dt_txt = from_datetime(obj.get("dt_txt"))
        return ListElement(
            dt, main, weather, clouds, wind, visibility, pop, sys, dt_txt
        )

    def to_dict(self) -> dict:
        result: dict = {}
        result["dt"] = from_int(self.dt)
        result["main"] = to_class(Main, self.main)
        result["weather"] = from_list(lambda x: to_class(Weather, x), self.weather)
        result["clouds"] = to_class(Clouds, self.clouds)
        result["wind"] = to_class(Wind, self.wind)
        result["visibility"] = from_int(self.visibility)
        result["pop"] = to_float(self.pop)
        result["sys"] = to_class(Sys, self.sys)
        result["dt_txt"] = self.dt_txt.isoformat()
        return result


class Root:
    cod: int
    message: int
    cnt: int
    list: List[ListElement]
    city: City

    def __init__(
        self, cod: int, message: int, cnt: int, list: List[ListElement], city: City
    ) -> None:
        self.cod = cod
        self.message = message
        self.cnt = cnt
        self.list = list
        self.city = city

    @staticmethod
    def from_dict(obj: Any) -> "Root":
        assert isinstance(obj, dict)
        cod = int(from_str(obj.get("cod")))
        message = from_int(obj.get("message"))
        cnt = from_int(obj.get("cnt"))
        list = from_list(ListElement.from_dict, obj.get("list"))
        city = City.from_dict(obj.get("city"))
        return Root(cod, message, cnt, list, city)

    def to_dict(self) -> dict:
        result: dict = {}
        result["cod"] = from_str(str(self.cod))
        result["message"] = from_int(self.message)
        result["cnt"] = from_int(self.cnt)
        result["list"] = from_list(lambda x: to_class(ListElement, x), self.list)
        result["city"] = to_class(City, self.city)
        return result


def root_from_dict(s: Any) -> Root:
    return Root.from_dict(s)


class FiveDayForecast:
    weather: Root
    api_key = API_KEY
    city_name: str
    units: str
    lang: str
    cnt: int

    def __init__(
        self, city_name: str, units: str = "metric", lang: str = "fr", cnt: int = 3
    ) -> None:
        self.city_name = city_name
        self.units = units
        self.lang = lang
        self.cnt = cnt
        self.weather = self.get_Five_day_weather(city_name, units, lang)

    def get_Five_day_weather(self, city_name, units="metric", lang="fr") -> Root:
        city = Direct_Geocoding(city_name)
        url = f"https://api.openweathermap.org/data/2.5/forecast?lat={city.get_Lat()}&lon={city.get_Lon()}&cnt={self.cnt}&units={units}&lang={lang}&appid={self.api_key}"
        response = requests.get(url)
        if response.status_code == 200:
            return Root.from_dict(response.json())
        else:
            print("Error: ", response.status_code)
            return None

    def toString(self):
        print(f"ville: {self.city_name}")
        for i in range(self.cnt):
            print(
                f"Date: {self.weather.list[i].dt_txt} - Température: {self.weather.list[i].main.temp} - Description: {self.weather.list[i].weather[0].description}"
            )

    def get_list(self):
        return self.weather.list

    def get_city(self):
        return self.weather.city

    def get_weather_at_day(self, day):
        return self.weather.list[day]

    def get_weather_at_time(self, time):
        for i in range(self.cnt):
            if self.weather.list[i].dt_txt.hour == time.hour:
                return self.weather.list[i]


# --------------------------------------------------------------
# Example Usage
# --------------------------------------------------------------
# fiveDayWeather = FiveDayForecast("Bordeaux", "metric", "fr", 10)
# print(f"TEMPERATURE= {fiveDayWeather.weather.list[0].main.temp}")
# fiveDayWeather.toString()
# print(fiveDayWeather.get_weather_at_time(datetime(2021, 11, 2, 12, 0, 0)).weather[0].description)
# --------------------------------------------------------------
