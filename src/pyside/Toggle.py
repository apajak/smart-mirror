from PySide6.QtGui import (QPainter, QColor)
from PySide6.QtCore import (QEasingCurve, QPoint, Qt,
                            QPropertyAnimation, QRect)
from PySide6.QtWidgets import (QCheckBox)


class Toggle(QCheckBox):
    def __init__(
        self,
        width=70,  # longueur du Toggle
        bg_color="#777",
        circle_color="white",
        active_color="blue",
        animation_curve=QEasingCurve.OutBounce
    ):
        QCheckBox.__init__(self)

        # SET DEFAULT PARAMETERS
        self.setFixedSize(width, 15)  # largeur du Toggle
        self.setCursor(Qt.PointingHandCursor)

        # COLORS
        self._bg_color = bg_color
        self._circle_color = circle_color
        self._active_color = active_color

        # CREATE ANIMATION
        self._circle_position = 3
        self.animation = QPropertyAnimation(self, b"circle_position", self)
        self.animation.setEasingCurve(animation_curve)
        self.animation.setDuration(500)

        # CONNECT STATE CHANGED
        self.stateChanged.connect(self.debug)

    def debug(self):
        print(f"Status: {self.isChecked()}")

    def circle_position(self, pos):
        self._circle_position = pos
        self.update()

    def hitButton(self, pos: QPoint):
        return self.contentsRect().contains(pos)

    def debstart_transition(self, value):
        self.animation.stop()
        if value:
            self.animation.setEndValue(self.width() - 26)
        else:
            self.animation.setEndValue(3)

        self.animation.start()

        print(f"Status: (self.isChecked())")

    def paintEvent(self, e):
        p = QPainter(self)
        p.setRenderHint(QPainter.Antialiasing)

        p.setPen(Qt.NoPen)

        rect = QRect(0, 0, self.width(), self.height())

        if not self.isChecked():
            p.setBrush(QColor(self._bg_color))
            p.drawRoundedRect(0, 0, rect.width(), self.height(),
                              self.height() / 2,
                              self.height() / 2)

            p.setBrush(QColor(self._circle_color))
            p.drawEllipse(3, 3, 9, 9)  # Taille du cercle initiale
        else:
            p.setBrush(QColor(self._active_color))
            p.drawRoundedRect(0, 0, rect.width(), self.height(),
                              self.height() / 2,
                              self.height() / 2)

            p.setBrush(QColor(self._circle_color))

            # Taille du cercle en activant le Toggle
            p.drawEllipse(self.width() - 13, 3, 10, 10)

            p.end()
