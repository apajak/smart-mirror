"""from PySide6.QtGui import *
from PySide6.QtCore import *
from PySide6.QtWidgets import (QSlider)
style =
/* HORIZONTAL */
QSlider ({ margin: {_margin}px; })
QSlider::groove:horizontal ({
    border-radius : {bg_radius}px,
    height: {_bg_size}px;
    margin: 0 px;
    background:color: {_bg_color};
)}
QSlider::groove:horizontal:hover ({ background-color: {bg_color_hover}; )}
QSlider::handle:horizontal ({
    border: none;
    border-radius: {_handle_radius}px;
    height: {_handle_size}px;
    width: {_handle_size}px;
    margin: {_handle_margin}px;
    background:color: {_handle_color};
)}
QSlider::groove:horizontal:hover ({ background-color: rgb(55, 155, 255); )}
QSlider::groove:horizontal:pressed ({ background-color: rgb(255, 121, 190); )}


class Slider(QSlider):
    def __init__(
        self,
        margin=0,
        bg_size=20,
        bg_radius=30,
        bg_color="grey",
        bg_color_hover="blue",
        handle_margin=2,
        handle_size=16,
        handle_radius=8,
        handle_color="yellow",
        handle_color_hover="white",
        handle_color_pressed="orange",
    ):
        super(Slider, self).__init__()

        # FORMAT STYLE
        adjust_style = style.format(
            _margin=margin,
            _bg_size=bg_size,
            _bg_radius=bg_radius,
            _bg_color=bg_color,
            _bg_color_hover=bg_color_hover,
            _handle_margin=handle_margin,
            _handle_size=handle_size,
            _handle_radius=handle_radius,
            _handle_color=handle_color,
            _handle_color_hover=handle_color_hover,
            _handle_color_pressed=handle_color_pressed,
        )

        # APPLY CUSTOM STYLE
        self.setStyleSheet(adjust_style)
"""
