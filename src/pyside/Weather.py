#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# ----------------------------------------------------------------------------
# Created By  : Pajak Alexandre
# Created Date: 01/10/2021
# version ='1.0'
# ---------------------------------------------------------------------------
""" Module for Manage all OpenWeather API. """
# ---------------------------------------------------------------------------
# Imports
# ---------------------------------------------------------------------------

import CurrentWeather
import five_day_forecast as FiveDayForecast
import Direct_Geocoding
import Reverse_geocoding


class Weather:
    def get_CurrentWeather(cityName: str):
        return CurrentWeather.CurrentWeather(cityName)

    def get_FiveDayForecast(cityName: str):
        return FiveDayForecast.FiveDayForecast(cityName)

    def get_DirectGeocoding(cityName: str):
        return Direct_Geocoding.Direct_Geocoding(cityName)

    def get_ReverseGeocoding(lat: float, lon: float):
        return Reverse_geocoding.Reverse_Geocoding(lat, lon)


# --------------------------------------------------------------
# Example Usage
# --------------------------------------------------------------
# current_weather = Weather.get_CurrentWeather("Bordeaux")
# print(current_weather.toString())
# five_day_weather = Weather.get_FiveDayForecast("Bordeaux")
# print(five_day_weather.toString())
# direct_geocoding = Weather.get_DirectGeocoding("Bordeaux")
# print(direct_geocoding.toString())
# reverse_geocoding = Weather.get_ReverseGeocoding(44.84, -0.58)
# print(reverse_geocoding.toString())
# --------------------------------------------------------------
