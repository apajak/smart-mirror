import socket
from pynput.mouse import Controller, Button
import pyautogui

def main():
    mouse = Controller()
    screen_width, screen_height = pyautogui.size()
    try:
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as client_socket:
            client_socket.connect(("localhost", 9999))
            client_socket.sendall(f"{screen_width},{screen_height}\n".encode('utf-8'))

            print("Connected to server.")
            buffer = ""
            while True:
                data = client_socket.recv(1024).decode('utf-8')
                if not data:
                    break
                buffer += data
                while '\n' in buffer:
                    message, buffer = buffer.split('\n', 1)
                    action, position = message.split(':')
                    x, y = position.split(',')
                    x = int(round(float(x)))
                    y = int(round(float(y)))

                    if action == 'move':
                        mouse.position = (x, y)
                    elif action == 'press':
                        mouse.press(Button.left)
                    elif action == 'release':
                        mouse.release(Button.left)
                    elif action == 'click':
                        mouse.click(Button.left, 1)
                    elif action == 'right_click':
                        mouse.click(Button.right, 1)

    except socket.error as e:
        print(f"Connection failed: {e}")

if __name__ == '__main__':
    main()
