import os
import time

class ScreenBrightnessController:
    def __init__(self):
        self.output = self.get_connected_monitor()

    def set_brightness(self, brightness):
        """Set the brightness of the screen to a specified level."""
        os.system(f"xrandr --output {self.output} --brightness {brightness}")

    def get_current_brightness(self):
        """Get the current brightness level of the screen."""
        output_info = os.popen(f"xrandr --verbose --current | grep -A 10 '{self.output} connected' | grep Brightness").read()
        try:
            return float(output_info.split()[1])
        except IndexError:
            return 1.0  # Return default brightness if unable to parse

    def get_connected_monitor(self):
        """Retrieve the currently connected monitor."""
        output = os.popen("xrandr | grep ' connected'").read().strip()
        monitor_name = output.split()[0]
        return monitor_name

    def gradually_adjust_brightness(self, target_brightness, duration=0.5, steps=50):
        """Gradually adjust the screen brightness over a specified duration and steps."""
        current_brightness = self.get_current_brightness()
        brightness_step = (target_brightness - current_brightness) / steps
        delay = duration / steps

        for step in range(steps):
            current_brightness += brightness_step
            self.set_brightness(current_brightness)
            time.sleep(delay)

        # Ensure the target brightness is set at the end
        self.set_brightness(target_brightness)

    def adjust_brightness_based_on_lux(self, lux_value):
        """Adjust the screen brightness based on a lux value from a light sensor."""
        # Simple example conversion: Adjust screen brightness directly proportional to lux
        # Example: 0 lux = 0.0 (min brightness), 500 lux = 1.0 (max brightness)
        # Adjust the maximum lux value to match your environment's typical maximum illumination
        max_lux = 500
        target_brightness = min(lux_value / max_lux, 1.0)
        self.gradually_adjust_brightness(target_brightness)