import subprocess

class VolumeController:
    def set_volume(self, volume):
        # Définit le volume sur Linux (0 à 100)
        subprocess.run(["amixer", "sset", "Master", f"{volume}%"])

    def get_volume(self):
        # Obtient le volume actuel sur Linux
        result = subprocess.run(["amixer", "sget", "Master"], capture_output=True, text=True)
        lines = result.stdout.splitlines()
        last_line = str(lines[-1])
        # Parse la sortie pour obtenir le volume
        percent_pos = last_line.find('%')
        start = last_line.rfind('[', 0, percent_pos) + 1
        return int(last_line[start:percent_pos])

    def mute(self):
        # Muet
        subprocess.run(["amixer", "sset", "Master", "mute"])

    def unmute(self):
        # Unmute
        subprocess.run(["amixer", "sset", "Master", "unmute"])

# Usage de la classe
# if __name__ == "__main__":
#     vc = VolumeControllerLinux()
#     print(f"Volume actuel: {vc.get_volume()}")
#     vc.set_volume(50)
#     print(f"Volume actuel: {vc.get_volume()}")