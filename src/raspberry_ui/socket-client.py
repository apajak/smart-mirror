import socketio
# from src.raspberry_ui.screen_brightness_controller import ScreenBrightnessController
from src.raspberry_ui.screen_brightness_controller import ScreenBrightnessController
from src.raspberry_ui.volume_controller import VolumeController
import json
from socketio.exceptions import ConnectionError
import time

# Créer une instance du client
sio = socketio.Client()
client_type = 'system'
brightness_controller = ScreenBrightnessController()
volumle_controller = VolumeController()

# Événements pour la connexion et la déconnexion
@sio.event
def connect():
    print("UI Raspberry Pi Python client is connected")
    sio.emit('identify_client', client_type)

@sio.event
def disconnect():
    print("UI Raspberry Pi Python client is disconnected")


## Brightness controlls ##
@sio.on('lux_data')
def on_lux_data(data):
    print(f"Received lux data: {data}")
    lux_value = data
    brightness_controller.adjust_brightness_based_on_lux(lux_value)


# Écouter les ajustements manuels de la luminosité
@sio.on('set_brightness')
def on_set_brightness(data):
    print("Received manual brightness setting:", data)
    brightness_controller.set_brightness(data)

# Méthode pour envoyer la luminosité actuelle de l'écran au serveur
@sio.on('request_current_brightness')
def send_current_brightness():
    current_brightness = brightness_controller.get_current_brightness()
    sio.emit('current_brightness', current_brightness)
    print("Sent current brightness:", current_brightness)

## Audio controlls ##
@sio.on('set_volume')
def on_set_volume(data):
    print("Received volume setting:", data)
    volumle_controller.set_volume(data)

@sio.on('get_volume')
def on_get_volume():
    volume = volumle_controller.get_volume()
    sio.emit('current_volume', volume)
    print("Sent current volume:", volume)

if __name__ == '__main__':
    while True:
        try:
            sio.connect('http://localhost:3000')
            sio.wait()
        except ConnectionError as e:
            print(f"Connection failed: {e}, retrying...")
            time.sleep(5)
        except KeyboardInterrupt:
            break