import pickle, uuid, cv2, os
import numpy as np
from src.ai.face_recognition.faceRecognition import FaceRecognition

CURRENT_PATH = os.path.abspath(os.getcwd())
USER_PKL_PATH = f'{CURRENT_PATH}\\user.pkl'
KNOWN_FACES_PATH = f'{CURRENT_PATH}\\known_faces\\'


class User:
    """
    User's class of the Smart-Mirror
    This class also update the pkl file where all users are saved
    """

    def __init__(self, name: str, city: str, strong_hand: str = "Left", google_token: str = None,
                 spotify_token: str = None) -> None:
        self.id = str(uuid.uuid1())  # Unique ID
        self.picture_path = str(f"{KNOWN_FACES_PATH}{str(self.id)}")  # Path to the picture of the user
        self.name = name
        self.city = city
        self.strong_hand = strong_hand
        self.google_token = google_token
        self.spotify_token = spotify_token

        ### Update pickle file as we create a new user
        # Open the pkl file and write our existing users in a dict
        try:
            with open(USER_PKL_PATH, "rb") as file:
                users = pickle.load(file)
        except:
            users = {}

        # Create the dict of our user
        user = {self.id: {'name': name, 'city': city, 'google_token': google_token, 'spotify_token': spotify_token}}
        # Update our database
        users.update(user)

        # Open the pkl file and print our updated database of users
        with open(USER_PKL_PATH, "wb") as file:
            pickle.dump(users, file)

    def update_pkl(self) -> None:
        """Update pkl file where all users are currently saved

        Args :
            - self: User

        Returns :
            None
        """
        users = get_all_users()

        # Updating dict
        user_to_load = {
            'name': self.name,
            'city': self.city,
            'strong_hand': self.strong_hand,
            'picture_path': self.picture_path,
            'google_token': self.google_token,
            'spotify_token': self.spotify_token
        }

        users[self.id] = {key: value for key, value in user_to_load.items()}

        # Print the updated users database
        with open(USER_PKL_PATH, "wb") as file:
            pickle.dump(users, file)

    def save_picture(self, frame) -> None:
        """Save the frame as a picture named src/known_faces/id.jpg

        Args :
            - frame: picture took with the camera

        Returns :
            None
        """

        def rotate_image(image, angle: int = 20):
            """Rotate an image by a certain angle

            Args:
                - image: np.array() image we want to rotate
                - angle: rotation

            Returns:
                np.array() rotated image
            """
            image_center = tuple(np.array(image.shape[1::-1]) / 2)
            rot_mat = cv2.getRotationMatrix2D(image_center, angle, 1.0)
            result = cv2.warpAffine(image, rot_mat, image.shape[1::-1], flags=cv2.INTER_LINEAR)
            return result

        angle = 20
        try:
            print(f'{self.picture_path}1.jpg')
            cv2.imwrite(f'{self.picture_path}1.jpg', frame)
            cv2.imwrite(f'{self.picture_path}2.jpg', rotate_image(frame, angle))
            cv2.imwrite(f'{self.picture_path}3.jpg', rotate_image(frame, -angle))
        except:
            raise print('An error occurred, please try again')


def get_all_users() -> dict:
    """Returns all users saved at the moment

    Returns:
        dict -> dictionary of all users saved in pickle file
    """
    try:
        with open(USER_PKL_PATH, "rb") as file:
            return pickle.load(file)
    except:
        return {}


def user_recognition(frame, fr: FaceRecognition(), user: User, timer: bool) -> (User, bool):
    """Get the user from a frame of the user's face

    Args:
        - frame -> frame from the camera currently running
        - face_recognizer -> face recognition class

    Returns:
        User: user object detected by the face recognition class in the frame
    """

    def find_user(fr: FaceRecognition()) -> dict:
        """Get the user.id from the frame of the user's face

        Args:
            - frame -> frame from the camera currently running

        Returns:
            dict -> user recognized by the face_recognition file
                    or None if unknown
        """
        users = get_all_users()
        try:
            user_id = fr.FaceName[:-1]
            return users[user_id]
        except KeyError:
            unknown_user = {'name': "Unknown"}
            return unknown_user

    def new_user() -> User:
        """Register a new user

        Args :
            None

        Returns :
            User: Name and city of the new user
        """
        name = input("Name : ")
        city = input("City : ")
        strong_hand = input("Left-Handed or Right-Handed ? : ")
        return User(name=name, city=city, strong_hand=strong_hand)

    # Running the face_recognition model
    fr.run_detection(frame, False, False)

    # Check if our face_recognition model find a face. If not we return None value for user and True for user_check
    if fr.FaceDetected:
        # If user is None and our timer is running we try to find if we know the face on the frame
        if user is None and timer:
            user = find_user(fr)
            return user, True

        # If user detected
        elif user is not None:
            return user, False

        # If the face is unknown and the timer is out we try to register the user as new user
        elif user is None and not timer:
            response = input('Would you like to register as a Smart Mirror user ? (Y/n)')

            # If user answer with Y/Yes/or nothing we create the user
            if response == "Y" or response == "Yes" or response == "":
                user = new_user()
                user.update_pkl()
                user.save_picture(frame)
                print(f"New user : {user.name} saved in database")
                return user, False
            else:
                return None, False
    return None, True
