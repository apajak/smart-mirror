using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Model_movement : MonoBehaviour
{
    // Start is called before the first frame update

    public UDPReceive udpReceive;
    public GameObject[] Body;

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        string data = udpReceive.data;
        string[] info = data.Split('|');
        // init a tuple array to store the data
        Tuple<float, float, float>[] tupleArray = new Tuple<float, float, float>[info.Length];
        foreach (string s in info)
        {
            // remove the parentheses
            string s2 = s.Remove(0, 1);
            s2 = s2.Remove(s2.Length - 1, 1);
            // split the string into x, y, and area
            string[] info2 = s2.Split(',');
            // convert the strings to floats
            float x = 5 - float.Parse(info[0]) / 100;
            float y = float.Parse(info[1]) / 100;
            float z = -10 + float.Parse(info[2]) / 100;
            // make a tuple of the floats
            Tuple<float, float, float> t = new Tuple<float, float, float>(x, y, z);
            // add the tuple to the data list
            tupleArray[Array.IndexOf(info, s)] = t;
        }
        // init var right_shoulder_coords at tupleArray[0]
        var right_shoulder_coords = tupleArray[0];
        var left_shoulder_coords = tupleArray[1];
        var right_elbow_coords = tupleArray[2];
        var left_elbow_coords = tupleArray[3];
        var right_wrist_coords = tupleArray[4];
        var left_wrist_coords = tupleArray[5];
        var right_hip_coords = tupleArray[6];
        var left_hip_coords = tupleArray[7];
        var right_knee_coords = tupleArray[8];
        var left_knee_coords = tupleArray[9];
        var right_ankle_coords = tupleArray[10];
        var left_ankle_coords = tupleArray[11];

        // move the parts
        for (int i = 0; i < Body.Length; i++)
        {
            Body[i].gameObject.transform.localPosition = new Vector3(tupleArray[i].Item1, tupleArray[i].Item2, tupleArray[i].Item3);
        }
    }
}
