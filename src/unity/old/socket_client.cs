using System.Net.Sockets;
using System.Text;
using UnityEngine;

// Socket client for Unity communication with the Raspberry Pi

public class SocketClient : MonoBehaviour
{
    void Start()
    {
        TcpClient client = new TcpClient("YOUR_PI_IP_ADDRESS", PORT);  // Replace with the server's IP and the same port number
        NetworkStream stream = client.GetStream();

        byte[] receivedBytes = new byte[1024];
        int bytesRead = stream.Read(receivedBytes, 0, receivedBytes.Length);
        string receivedData = Encoding.ASCII.GetString(receivedBytes, 0, bytesRead);
        Debug.Log("Received data: " + receivedData);

        stream.Close();
        client.Close();
    }
}
