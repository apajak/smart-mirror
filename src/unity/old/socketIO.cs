using SocketIOClient;
using System;
using System.Threading.Tasks;

class Program
{
    static async Task Main(string[] args)
    {
        // Créez une instance de SocketIOClient.SocketIO et configurez-la pour se connecter à l'URL de votre serveur
        var client = new SocketIOClient.SocketIO("http://localhost:3000/");

        // Écoutez l'événement 'response' pour recevoir des messages du serveur
        client.On("response", response =>
        {
            string text = response.GetValue<string>();
            Console.WriteLine($"Message from server: {text}");
        });

        // Gérez l'événement de connexion pour émettre un message au serveur une fois connecté
        client.OnConnected += async (sender, e) =>
        {
            Console.WriteLine("Connected to server.");
            // Émettez un événement 'message' avec un payload vers le serveur
            await client.EmitAsync("message", "Hello from C# client!");
        };

        // Connectez-vous au serveur Socket.IO
        await client.ConnectAsync();

        // Maintenez l'application en cours d'exécution ou ajoutez une logique pour la fermer correctement
        Console.ReadLine(); // Gardez la console ouverte jusqu'à ce que l'utilisateur appuie sur Entrée
    }
}