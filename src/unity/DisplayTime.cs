using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DisplayTime : MonoBehaviour
{
    public TMP_Text timeText;
    public TMP_Text timeText_2;
    public TMP_Text timeText_3;
    
    public TMP_Text timeText_4;

    void Update()
    {
        timeText.text = System.DateTime.Now.ToString("HH:mm:ss");
        timeText_2.text = System.DateTime.Now.ToString("HH:mm:ss");
        timeText_3.text = System.DateTime.Now.ToString("HH:mm:ss");
        timeText_4.text = System.DateTime.Now.ToString("HH:mm:ss");
    }
}
