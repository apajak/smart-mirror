using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PageManager : MonoBehaviour
{
    public GameObject[] pages; // Assignez vos pages ici dans l'inspecteur
    public SocketManager socketManager; // Mise à jour pour utiliser SocketManager

    void Start()
    {
        // Optionnel : Envoyez un message lorsque l'application démarre
        if (socketManager != null)
        {
            socketManager.EmitSimpleMessage("Application démarrée");
        }
        foreach (GameObject page in pages)
        {
            page.SetActive(false); // Désactive toutes les pages
        }

        pages[0].SetActive(true); // Active la page demandée
    }

    public void ChangePage(int pageIndex)
    {
        foreach (GameObject page in pages)
        {
            page.SetActive(false); // Désactive toutes les pages
        }

        pages[pageIndex].SetActive(true); // Active la page demandée

        // Envoyez un message au serveur à chaque changement de page
        if (socketManager != null)
        {
            socketManager.EmitSimpleMessage($"Changement de page vers l'index {pageIndex}");
            socketManager.GetWeatherData("");
            socketManager.GetWeatherDataFiveDays("");
            if (pageIndex == 2)
            {
                socketManager.get_calandar_events();
            }
            else if (pageIndex == 3)
            {
                socketManager.GetBrightness();
                socketManager.GetAutoBrightness();
                socketManager.GetVolume();
                socketManager.get_network_status();
                socketManager.get_default_weather_city();
            }
        }
    }
}
