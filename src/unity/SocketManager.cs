using System;
using System.Collections.Generic;
using SocketIOClient;
using SocketIOClient.Newtonsoft.Json;
using UnityEngine;
using UnityEngine.UI;
using Newtonsoft.Json.Linq;
using System.Collections;
using TMPro;
using System.Threading;
using Debug = System.Diagnostics.Debug;

public class SocketManager : MonoBehaviour
{
    public SocketIOUnity socket;
    public WeatherTextUpdater weatherTextUpdater;
    public ImageChanger imageChanger;
    public EventDisplayManager eventDisplayManager;
    
    // Start is called before the first frame update
    void Awake() {
        UnityEngine.Debug.Log("Initializing SocketManager");
        var uri = new Uri("http://127.0.0.1:3000");
        socket = new SocketIOUnity(uri);
        socket.JsonSerializer = new NewtonsoftJsonSerializer();
    }

    void Start() {
        // Connectez les événements et démarrez la connexion ici.
        AttachEvents();
        UnityEngine.Debug.Log("Connecting...");
        socket.Connect();
    }

    private void AttachEvents() {
        socket.OnConnected += (sender, e) => {
            UnityEngine.Debug.Log("socket.OnConnected");
            IdentifyClient("unity");  // Identify the client type right after connecting
        };
        socket.OnDisconnected += (sender, e) => { UnityEngine.Debug.Log("disconnect: " + e); };
        socket.OnReconnectAttempt += (sender, e) => { UnityEngine.Debug.Log($"{DateTime.Now} Reconnecting: attempt = {e}"); };

        socket.On("weather_response", HandleWeatherResponse);
        socket.On("weather_five_days_response", HandleFiveDaysWeatherResponse);
        socket.On("calendar_response", HandleCalendarResponse);
        socket.On("luminosity_data", HandleLuminosityDataReceived);
        socket.On("auto_brightness", HandleAutoBrightness);
        socket.On("volume_data", HandmeVolumeDataReceived);
        socket.On("network_status", HandleNetworkStatus);
        socket.On("default_weather_city", HandleDefaultWeatherCity);
    }
    private void IdentifyClient(string clientType)
    {
        UnityEngine.Debug.Log($"Identifying client as {clientType}");
        socket.Emit("identify_client", clientType);
    }
    private void HandleCalendarResponse(SocketIOResponse response)
    {
        try
        {
            string jsonResponse = response.GetValue<string>();
            JArray dataArray = JArray.Parse(jsonResponse);

            List<CalendarDay> calendarDays = new List<CalendarDay>();
            foreach (JObject dayEvent in dataArray)
            {
                string date = dayEvent["date"].ToString();
                List<string> events = new List<string>();
                JArray eventsArray = (JArray)dayEvent["events"];
                foreach (JObject eventDetail in eventsArray)
                {
                    string eventTime = eventDetail["date"].ToString();
                    string summary = eventDetail["summary"].ToString();
                    // Extraire seulement l'heure
                    DateTime parsedDate = DateTime.Parse(eventTime);
                    string eventHour = parsedDate.ToString("HH:mm");
                    events.Add($"{eventHour}: {summary}");
                }
                calendarDays.Add(new CalendarDay { date = date, events = events });
            }

            actions.Enqueue(() => eventDisplayManager.UpdateCalendar(calendarDays));
        }
        catch (Exception ex)
        {
            UnityEngine.Debug.LogError("Error processing calendar data: " + ex.Message);
        }
    }
    
    private void HandleWeatherResponse(SocketIOResponse response) {
        try {
            string jsonResponse = response.GetValue<string>();

            var data = JObject.Parse(jsonResponse);
            string cityName = data["city_name"].ToString();
            float temp = float.Parse(data["temp"].ToString()); // Utilisez float.TryParse pour plus de sécurité
            string iconId = data["icon"].ToString();

            // Enfiler la mise à jour de l'UI pour être exécutée dans le thread principal
            actions.Enqueue(() => UpdateUI(cityName, temp, iconId));
        } catch (Exception ex) {
            UnityEngine.Debug.LogError("Error processing weather data: " + ex.Message);
        }
    }

    private void HandleFiveDaysWeatherResponse(SocketIOResponse response) {
        try {
            string jsonResponse = response.GetValue<string>();

            // Parse the JSON response
            JObject dataObject = JObject.Parse(jsonResponse);

            // Extract the current weather data
            JObject currentData = dataObject["current"].ToObject<JObject>();
            string currentTime = currentData["time"].ToString();
            float currentTemp = currentData["temp"].ToObject<float>();
            string currentIcon = currentData["icon"].ToString();
            string cityName = currentData["city"].ToString();

            // Extract the forecast data
            JArray forecastArray = JArray.Parse(dataObject["forecast"].ToString());

            // Initialize lists to store time, temperature, and icon data
            List<string> times = new List<string>();
            List<float> temps = new List<float>();
            List<string> icons = new List<string>();

            // Add current weather data to the lists
            times.Add(currentTime);
            temps.Add(currentTemp);
            icons.Add(currentIcon);

            // Iterate through the JSON array and extract forecast data
            foreach (JObject data in forecastArray) {
                string time = data["time"].ToString();
                float temp = data["temp"].ToObject<float>();
                string icon = data["icon"].ToString();

                times.Add(time);
                temps.Add(temp);
                icons.Add(icon);
            }

            // Debug log the extracted data
            UnityEngine.Debug.Log("Times: " + string.Join(", ", times));
            UnityEngine.Debug.Log("Temperatures: " + string.Join(", ", temps));
            UnityEngine.Debug.Log("Icons: " + string.Join(", ", icons));

            // Update your UI or perform other actions with the extracted data
            actions.Enqueue(() => UpdateFiveDayForecast(times, temps, icons, cityName));

        } catch (Exception ex) {
            UnityEngine.Debug.LogError("Error processing weather data: " + ex.Message);
        }
    }

    public static bool IsJSON(string str)
    {
        if (string.IsNullOrWhiteSpace(str)) { return false; }
        str = str.Trim();
        if ((str.StartsWith("{") && str.EndsWith("}")) || //For object
            (str.StartsWith("[") && str.EndsWith("]"))) //For array
        {
            try
            {
                var obj = JToken.Parse(str);
                return true;
            }catch (Exception ex) //some other exception
            {
                Console.WriteLine(ex.ToString());
                return false;
            }
        }
        else
        {
            return false;
        }
    }
    private Queue<Action> actions = new Queue<Action>();

    private void Update()
    {
        while (actions.Count > 0)
        {
            var action = actions.Dequeue();
            action();
        }
    }
    void UpdateUI(string cityName, float temp, string iconId)
    {
        weatherTextUpdater.UpdateCityName(cityName);
        weatherTextUpdater.UpdateTemperatureInfo(temp);
        imageChanger.SetImage(iconId);
    }
    
    void UpdateFiveDayForecast(List<string> times, List<float> temps, List<string> icons, string cityName)
    {
        // Update the UI with the extracted data
        weatherTextUpdater.UpdateCityName_2(cityName);
        weatherTextUpdater.UpdateFiveDayForecast(times, temps);
        imageChanger.SetFiveImage(icons);
    }

    
    
    public void EmitSimpleMessage(string message)
    {
        UnityEngine.Debug.Log($"Emitting message: {message}");
        socket.Emit("message", message); // Assurez-vous que cela correspond à l'événement côté serveur
    }

    public void GetWeatherData(String city)
    {
        UnityEngine.Debug.Log($"Requesting weather data for {city}");
        socket.Emit("weather_request", city);
    }
    public void GetWeatherDataFiveDays(String city)
    {
        UnityEngine.Debug.Log($"Requesting weather data for {city}");
        socket.Emit("five_days_weather_request", city);
    }


    public void EmitSimpleJSONMessage(string message)
    {
        UnityEngine.Debug.Log($"Emitting JSON message: {message}");
        socket.EmitStringAsJSON("simple message", message);
    }

    public void get_calandar_events()
    {
        UnityEngine.Debug.Log($"Requesting calandar future events");
        socket.Emit("get_calandar_events");
    }
    
    public void GetBrightness()
    {
        if (socket.Connected)
        {
            UnityEngine.Debug.Log("Requesting brightness data.");
            socket.Emit("get_luminosity");
        }
        else
        {
            UnityEngine.Debug.Log("Socket is not connected. Cannot request brightness data.");
        }
    }
    private void HandleLuminosityDataReceived(SocketIOResponse response) {
        try {
            var data = response.GetValue<Dictionary<string, object>>();
            if (data.ContainsKey("error")) {
                UnityEngine.Debug.LogError($"Error received: {data["error"]}");
            } else {
                float brightness = Convert.ToSingle(data["brightness"]);
                UnityEngine.Debug.Log($"Received brightness: {brightness}");
                // Enqueue the update to be done on the main thread
                actions.Enqueue(() => {
                    if (SettingsPage.Instance != null) {
                        SettingsPage.Instance.SetBrightness(brightness);
                    }
                });
            }
        } catch (Exception e) {
            UnityEngine.Debug.LogError($"Exception when handling luminosity data: {e.Message}");
        }
    }
    
    private void HandleAutoBrightness(SocketIOResponse response) {
        UnityEngine.Debug.Log("Received auto-brightness data.");
        UnityEngine.Debug.Log($"Response: {response.GetValue<string>()}");
        try {
            var jsonResponse = response.GetValue<string>();  // Récupérer la réponse sous forme de chaîne JSON
            UnityEngine.Debug.Log($"JSON Response: {jsonResponse}");

            var data = JObject.Parse(jsonResponse);  // Utilisez JObject.Parse pour parser la chaîne JSON
            if (data.ContainsKey("auto_brightness")) {
                bool isOn = data["auto_brightness"].Value<bool>();  // Accéder à la valeur booléenne directement
                UnityEngine.Debug.Log($"Received auto-brightness: {isOn}");

                actions.Enqueue(() => {
                    if (SettingsPage.Instance != null) {
                        SettingsPage.Instance.SetAutoBrightness(isOn);
                    }
                });
            } else {
                UnityEngine.Debug.LogError("Received auto-brightness without auto_brightness field");
            }
        } catch (Exception e) {
            UnityEngine.Debug.LogError($"Exception when handling auto-brightness data: {e.Message}");
        }
    }

    
    public void SetBrightness(float brightness) {
        if (brightness < 0 || brightness > 1) {
            UnityEngine.Debug.LogError("Brightness value must be between 0 and 1");
            return;
        }
        UnityEngine.Debug.Log($"Sending new brightness value: {brightness}");
        socket.Emit("set_luminosity", brightness);
    }

    public void SetAutoBrightness(bool isOn) {
        UnityEngine.Debug.Log($"Sending auto-brightness setting to server: {isOn}");
        socket.Emit("set_auto_luminosity", isOn);
    }
    
    public void GetAutoBrightness() {
        if (socket.Connected) {
            UnityEngine.Debug.Log("Requesting auto-brightness data.");
            socket.Emit("get_auto_luminosity");
        } else {
            UnityEngine.Debug.Log("Socket is not connected. Cannot request auto-brightness data.");
        }
    }
    
    public void SetVolume(float volume) {
        UnityEngine.Debug.Log($"Sending new volume value: {volume}");
        socket.Emit("set_volume", volume);
    }
    
    public void GetVolume() {
        if (socket.Connected) {
            UnityEngine.Debug.Log("Requesting volume data.");
            socket.Emit("get_volume");
        } else {
            UnityEngine.Debug.Log("Socket is not connected. Cannot request volume data.");
        }
    }
    
    private void HandmeVolumeDataReceived(SocketIOResponse response) {
        try {
            var data = response.GetValue<Dictionary<string, object>>();
            if (data.ContainsKey("error")) {
                UnityEngine.Debug.LogError($"Error received: {data["error"]}");
            } else {
                float volume = Convert.ToSingle(data["volume"]);
                UnityEngine.Debug.Log($"Received volume: {volume}");
                // Enqueue the update to be done on the main thread
                actions.Enqueue(() => {
                    if (SettingsPage.Instance != null) {
                        SettingsPage.Instance.SetVolume(volume);
                    }
                });
            }
        } catch (Exception e) {
            UnityEngine.Debug.LogError($"Exception when handling volume data: {e.Message}");
        }
    }
    
    private void HandleNetworkStatus(SocketIOResponse response) {
        try {
            var data = response.GetValue<Dictionary<string, object>>();
            if (data.ContainsKey("status")) {
                UnityEngine.Debug.Log($"Network status: {data["status"]}");
                bool status = Convert.ToBoolean(data["status"]);
                // Enqueue the update to be done on the main thread
                actions.Enqueue(() => {
                    if (SettingsPage.Instance != null) {
                        SettingsPage.Instance.SetNetworkStatus(status);
                    }
                });
                
            } else {
                UnityEngine.Debug.LogError("Received network status without status field");
            }
        } catch (Exception e) {
            UnityEngine.Debug.LogError($"Exception when handling network status: {e.Message}");
        }
    }

    public void SetNetworkStatus(bool status)
    {
        UnityEngine.Debug.Log($"Sending new network status: {status}");
        if (status == true)
        {
            socket.Emit("enable_network");
        }
        else
        {
            socket.Emit("disable_network");
        }
    }
    
    public void get_network_status()
    {
        UnityEngine.Debug.Log($"Requesting network status");
        socket.Emit("get_network_status");
    }
    
    public void HandleDefaultWeatherCity(SocketIOResponse response)
    {
        try
        {
            var data = response.GetValue<Dictionary<string, object>>();
            if (data.ContainsKey("city"))
            {
                string defaultCity = data["city"].ToString();
                UnityEngine.Debug.Log($"Received default weather city: {defaultCity}");
                // Enqueue the update to be done on the main thread
                actions.Enqueue(() => SettingsPage.Instance.SetDefaultWeatherCity(defaultCity));
            }
            else
            {
                UnityEngine.Debug.LogError("Received default weather city without city field");
            }
        }
        catch (Exception ex)
        {
            UnityEngine.Debug.LogError("Error processing default weather city: " + ex.Message);
        }
    }

    public void SetDefaultWeatherCity(string cityName)
    {
        UnityEngine.Debug.Log($"Setting default weather city to: {cityName}");
        socket.Emit("set_default_weather_city", cityName);
    }
    
    public void get_default_weather_city()
    {
        UnityEngine.Debug.Log($"Requesting default weather city");
        socket.Emit("get_default_weather_city");
    }


}