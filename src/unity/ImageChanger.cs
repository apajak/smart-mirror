using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class ImageChanger : MonoBehaviour
{
    public Image targetImage;
    public Image targetImage_2;

    public Image targetImage_3;
    public Image targetFiveImage0;
    public Image targetFiveImage1;
    public Image targetFiveImage2;
    public Image targetFiveImage3;
    public Image targetFiveImage4;

    void Start()
    {
        SetImage("01d");
    }

    public void SetImage(string imageName)
    {
        Debug.Log($"Trying to load image: {imageName}@2x");
        Sprite newSprite = Resources.Load<Sprite>($"WeatherIcons/{imageName}@2x");

        if (newSprite != null)
        {
            if (targetImage != null)
            {
                targetImage.sprite = newSprite;
                Debug.Log("targetImage loaded successfully.");
            }
            else
            {
                Debug.LogError("targetImage is not assigned in the inspector.");
            }

            if (targetImage_2 != null)
            {
                targetImage_2.sprite = newSprite;
                Debug.Log("targetImage_2 loaded successfully.");
            }
            if (targetImage_3 != null)
            {
                targetImage_3.sprite = newSprite;
                Debug.Log("targetImage_3 loaded successfully.");
            }
            else
            {
                Debug.LogError("targetImage_2 is not assigned in the inspector.");
            }
        }
        else
        {
            Debug.LogError("Image not found! Make sure the image is in the 'Resources/WeatherIcons' directory and named correctly.");
        }
    }

    public void SetFiveImage(List<string> imageNames)
    {
        if (imageNames == null || imageNames.Count != 5)
        {
            Debug.LogError("The list of image names must contain exactly 5 elements.");
            return;
        }

        Image[] targetImages = { targetFiveImage0, targetFiveImage1, targetFiveImage2, targetFiveImage3, targetFiveImage4 };

        for (int i = 0; i < imageNames.Count; i++)
        {
            Debug.Log($"Trying to load image: {imageNames[i]}@2x");
            Sprite newSprite = Resources.Load<Sprite>($"WeatherIcons/{imageNames[i]}@2x");
            if (newSprite != null)
            {
                targetImages[i].sprite = newSprite;
                Debug.Log($"Image {i} loaded successfully.");
            }
            else
            {
                Debug.LogError($"Image {imageNames[i]} not found! Make sure the image is in the 'Resources/WeatherIcons' directory and named correctly.");
            }
        }
    }
}
