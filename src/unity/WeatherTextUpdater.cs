using UnityEngine;
using TMPro; // Import nécessaire pour travailler avec TextMeshPro
using System.Collections.Generic;

public class WeatherTextUpdater : MonoBehaviour
{
    public TMP_Text cityNameText;
    public TMP_Text cityNameText_2;
    public TMP_Text cityNameText_3;
    public TMP_Text cityNameText_4;
    public TMP_Text temperatureInfoText;
    public TMP_Text temperatureInfoText_3;
    public TMP_Text temperatureInfoText_4;
    
    // vars for five days forecast
    public TMP_Text day1;
    public TMP_Text day2;
    public TMP_Text day3;
    public TMP_Text day4;
    public TMP_Text day5;
    public TMP_Text temp1;
    public TMP_Text temp2;
    public TMP_Text temp3;
    public TMP_Text temp4;
    public TMP_Text temp5;

    // Utilisez Awake pour initialiser des données juste avant que le processus de démarrage commence.
    private void Awake()
    {
        // Initialisation avec des valeurs par défaut
        UpdateCityName("Bordeaux");
        UpdateTemperatureInfo(20.0f);
        
        // Initialisation des valeurs par défaut pour les prévisions sur 5 jours
        UpdateFiveDayForecast(
            new List<string> { "Lun", "Mar", "Mer", "Jeu", "Ven" }, 
            new List<float> { 20.0f, 21.0f, 22.0f, 23.0f, 24.0f }
        );
    }

    public void UpdateCityName(string cityName)
    {
        Debug.Log($"Updating city name to: {cityName}");
        if (cityNameText != null)
        {
            cityNameText.text = cityName;
            cityNameText_3.text = cityName;
            cityNameText_4.text = cityName;
        }
        else
        {
            Debug.LogError("City Name Text component is not assigned!");
        }
    }
    public void UpdateCityName_2(string cityName)
    {
        Debug.Log($"Updating city name to: {cityName}");
        if (cityNameText_2 != null)
        {
            cityNameText_2.text = cityName;
        }
        else
        {
            Debug.LogError("City Name Text component is not assigned!");
        }
    }

    public void UpdateTemperatureInfo(float temp)
    {
        Debug.Log($"Updating temperature info to: {temp}°C");
        if (temperatureInfoText != null)
        {
            temperatureInfoText.text = $"{temp}°C";
            temperatureInfoText_3.text = $"{temp}°C";
            temperatureInfoText_4.text = $"{temp}°C";
        }
        else
        {
            Debug.LogError("Temperature Info Text component is not assigned!");
        }
    }

    public void UpdateFiveDayForecast(List<string> days, List<float> temps)
    {
        if (days.Count != 5 || temps.Count != 5)
        {
            Debug.LogError("The list of days and temperatures must contain exactly 5 elements.");
            return;
        }

        // Mettre à jour les noms des jours
        day1.text = days[0];
        day2.text = days[1];
        day3.text = days[2];
        day4.text = days[3];
        day5.text = days[4];

        // Mettre à jour les températures
        temp1.text = $"{temps[0]}°C";
        temp2.text = $"{temps[1]}°C";
        temp3.text = $"{temps[2]}°C";
        temp4.text = $"{temps[3]}°C";
        temp5.text = $"{temps[4]}°C";
    }


}