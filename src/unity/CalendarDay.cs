using System.Collections.Generic;

[System.Serializable]
public class CalendarDay
{
    public string date;
    public List<string> events;
}