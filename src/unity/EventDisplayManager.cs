using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using TMPro;

public class EventDisplayManager : MonoBehaviour
{
    public GameObject dayButtonPrefab;
    public GameObject eventTextPrefab;
    public Transform eventsParent;
    
    private List<CalendarDay> days = new List<CalendarDay>();

    void Start()
    {
        DisplayEvents();
    }

    public void DisplayEvents()
    {
        foreach (Transform child in eventsParent) {
            Destroy(child.gameObject);
        }

        foreach (var day in days)
        {
            GameObject newDayButton = Instantiate(dayButtonPrefab, eventsParent);
            TextMeshProUGUI dateText = newDayButton.GetComponentInChildren<TextMeshProUGUI>();
            dateText.text = day.date;
            Transform eventListContainer = newDayButton.transform.Find("EventListContainer");
            eventListContainer.gameObject.SetActive(false);

            foreach (var ev in day.events)
            {
                GameObject newEvent = Instantiate(eventTextPrefab, eventListContainer);
                newEvent.GetComponent<TextMeshProUGUI>().text = ev;
            }

            newDayButton.GetComponent<Button>().onClick.AddListener(() => ToggleEvents(eventListContainer.gameObject, newDayButton));
        }
    }

    private void ToggleEvents(GameObject eventList, GameObject clickedButton)
    {
        bool isActive = !eventList.activeSelf;
        eventList.SetActive(isActive);

        bool foundClicked = false;
        foreach (Transform dayButtonTransform in eventsParent)
        {
            if (dayButtonTransform.gameObject == clickedButton)
            {
                foundClicked = true;
                continue;
            }
            if (foundClicked)
            {
                dayButtonTransform.gameObject.SetActive(!isActive);
            }
            else
            {
                Transform otherEventList = dayButtonTransform.Find("EventListContainer");
                otherEventList.gameObject.SetActive(false);
            }
        }
    }

    public void UpdateCalendar(List<CalendarDay> newDays)
    {
        days = newDays;
        DisplayEvents();
    }
}
