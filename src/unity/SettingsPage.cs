using UnityEngine;
using UnityEngine.UI;
using TMPro; 

public class SettingsPage : MonoBehaviour {
    public Slider brightnessSlider;
    
    public Slider volumeSlider;
    public Toggle autoBrightnessToggle;
    public SocketManager socketManager;
    public Toggle networkToggle;
    public TMP_InputField weatherCityInputField;
    public static SettingsPage Instance;

    void Awake() {
        if (Instance == null) {
            Instance = this;
        } else {
            Destroy(gameObject);
        }
    }

    void Start() {
        if (socketManager == null) {
            Debug.LogError("SocketManager is not assigned on the SettingsPage");
            return;
        }
        // Set up the slider listener
        if (brightnessSlider != null) {
            brightnessSlider.onValueChanged.AddListener(HandleBrightnessChange);
        } else {
            Debug.LogError("BrightnessSlider is not assigned in the inspector");
        }
        if (autoBrightnessToggle != null) {
            autoBrightnessToggle.onValueChanged.AddListener(HandleAutoBrightnessToggle);
        } else {
            Debug.LogError("AutoBrightnessToggle is not assigned in the inspector");
        }
        if (volumeSlider != null) {
            volumeSlider.onValueChanged.AddListener(HandleVolumeChange);
        } else {
            Debug.LogError("VolumeSlider is not assigned in the inspector");
        }
        if (networkToggle != null) {
            networkToggle.onValueChanged.AddListener(HandleNetworkToggle);
        } else {
            Debug.LogError("NetworkToggle is not assigned in the inspector");
        }
        if (weatherCityInputField != null) {
            weatherCityInputField.onEndEdit.AddListener(HandleWeatherCityChange);
        } else {
            Debug.LogError("WeatherCityInputField is not assigned in the inspector");
        }

        socketManager.GetBrightness();
        Debug.Log("Requesting brightness");
    }

    public void SetBrightness(float brightness) {
        if (brightnessSlider != null) {
            brightnessSlider.value = brightness;
            Debug.Log("Brightness slider set to: " + brightness);
        } else {
            Debug.LogError("BrightnessSlider is not assigned in the inspector");
        }
    }
    public void SetVolume(float volume) {
        if (volumeSlider != null) {
            volumeSlider.value = volume;
            Debug.Log("Volume slider set to: " + volume);
        } else {
            Debug.LogError("VolumeSlider is not assigned in the inspector");
        }
    }
    
    private void HandleBrightnessChange(float newValue) {
        Debug.Log($"Slider changed to: {newValue}");
        // Disable auto brightness toggle when manually adjusting brightness
        if (autoBrightnessToggle != null) {
            autoBrightnessToggle.isOn = false;
        } else {
            Debug.LogError("AutoBrightnessToggle is not assigned in the inspector");
        }
        // Send the new brightness value to the server
        if (socketManager != null) {
            socketManager.SetBrightness(newValue);
        } else {
            Debug.LogError("SocketManager is not assigned on the SettingsPage");
        }
    }
    public void SetAutoBrightness(bool isOn) {
        if (autoBrightnessToggle != null) {
            autoBrightnessToggle.isOn = isOn;
            Debug.Log("Auto-brightness toggle set to: " + isOn);
        } else {
            Debug.LogError("AutoBrightnessToggle is not assigned in the inspector");
        }
    }
    private void HandleAutoBrightnessToggle(bool isOn) {
        Debug.Log($"Auto-brightness toggled: {(isOn ? "On" : "Off")}");
        if (socketManager != null) {
            socketManager.SetAutoBrightness(isOn);
        }
    }
    private void HandleVolumeChange(float newValue) {
        Debug.Log($"Volume changed to: {newValue}");
        if (socketManager != null) {
            socketManager.SetVolume(newValue);
        }
    }
    private void HandleNetworkToggle(bool isOn) {
        Debug.Log($"Network toggled: {(isOn ? "On" : "Off")}");
        if (socketManager != null) {
            socketManager.SetNetworkStatus(isOn);
        }
    }
    
    public void SetNetworkStatus(bool isOn) {
        if (networkToggle != null) {
            networkToggle.isOn = isOn;
            Debug.Log("Network toggle set to: " + isOn);
        } else {
            Debug.LogError("NetworkToggle is not assigned in the inspector");
        }
    }
    
    private void HandleWeatherCityChange(string cityName) {
        Debug.Log($"Weather city changed to: {cityName}");
        if (socketManager != null) {
            socketManager.SetDefaultWeatherCity(cityName);
        }
    }
    
    public void SetDefaultWeatherCity(string cityName) {
        if (weatherCityInputField != null) {
            weatherCityInputField.text = cityName;
            Debug.Log("Weather city input field set to: " + cityName);
        } else {
            Debug.LogError("WeatherCityInputField is not assigned in the inspector");
        }
    }
}