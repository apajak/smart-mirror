from src.api.weather.weather import Weather as weather

def weather_test():
    try :
        print("Weather API")
        Weather = weather()
        # currentWeather test
        current_weather = Weather.get_CurrentWeather("bordeaux")
        current_weather.toString()
        # fiveDayForecast test
        # five_day_forecast = Weather.get_FiveDayForecast("bordeaux")
        # five_day_forecast.toString()
        # # directGeocoding test
        # direct_geocoding = Weather.get_DirectGeocoding("bordeaux")
        # direct_geocoding.toString()
        # # reverseGeocoding test
        # reverse_geocoding = Weather.get_ReverseGeocoding(44.837789, -0.57918)
        # reverse_geocoding.toString()
        print("Done")
    except Exception as e:
        print(e)

if __name__ == "__main__":
    weather_test()