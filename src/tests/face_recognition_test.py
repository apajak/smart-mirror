from src.ai.face_recognition import faceRecognition as fr
import cv2, time

def face_recognition_test():
    cap = cv2.VideoCapture(0)
    face_recognition = fr.FaceRecognition()
    counter = 0
    start_time = time.time()
    while True:
        frame = cap.read()[1]
        face_recognition.run_detection(frame, display=True, verbose=True)
        if face_recognition.FaceDetected:
            print(face_recognition.FaceName)
        counter += 1
        if counter == 30:
            time_elapsed = time.time() - start_time
            break
    print("\nDone\n")
    print(f"FPS : {counter / time_elapsed}")
    print(f"time elapsed : {time_elapsed}")
    print(f"Face detected : {face_recognition.FaceDetected}")
    print(f"Face name : {face_recognition.FaceName}")     
        
if __name__ == "__main__":
    face_recognition_test()