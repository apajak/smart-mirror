from src.ai.body_detection.orientation_detection import OrientationDetection as od
import time
import cv2
import numpy as np

def body_dectection_test():
    orientation_detection = od()
    counter = 0
    cap = cv2.VideoCapture(0)
    display = True
    while True:
        print(f"orientation : {orientation_detection.rotation_degree}")
        success, frame = cap.read()
        orientation_detection.process_frame(frame, display=display, verbose=True)

if __name__ == "__main__":
    body_dectection_test()