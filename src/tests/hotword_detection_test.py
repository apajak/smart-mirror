from src.ai.vocal_recognition.hot_word_detection import hotword_detection

def hotword_detection_test():
    detector = hotword_detection.HotwordDetector(["Smart-Mirror"], [0.8])
    def test_callback(keyword):
        print("Hotword Detected: %s" % keyword)

    print("Listening...")
    detector.listen(test_callback)

if __name__ == "__main__":
    hotword_detection_test()