import cv2
from src.ai.head_detection import FacePoseEstimator

cap = cv2.VideoCapture(0)
face_pose_estimator = FacePoseEstimator()
counter = 0
while True:
    frame = cap.read()[1]
    pitch, yaw, roll = face_pose_estimator.estimate_pose(frame)
    if pitch is not None:
        print(f"Pitch: {pitch:.2f}, Yaw: {yaw:.2f}, Roll: {roll:.2f}")

cap.release()
cv2.destroyAllWindows()