from src.ai.gesture_recognition.hand_detection import HandRecognizer
import cv2

def hand_recognizer_test():
    hand_recognizer = HandRecognizer()
    cap = cv2.VideoCapture(0)

    while True:
        _, frame = cap.read()

        hand_recognizer.run_detection(frame)
        
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
        
if __name__ == "__main__":
    hand_recognizer_test()