import os
from datetime import datetime, timedelta
from google_auth_oauthlib.flow import InstalledAppFlow
from googleapiclient.discovery import build
from google.auth.transport.requests import Request
import pickle
import json

class GoogleCalendar:
    def __init__(self, client_secret_file, api_name='calendar', api_version='v3', scopes=None):
        self.client_secret_file = client_secret_file
        self.api_name = api_name
        self.api_version = api_version
        self.scopes = scopes or ['https://www.googleapis.com/auth/calendar']
        self.service = self.create_service()

    def create_service(self):
        cred = None
        token_dir = 'token_files'
        pickle_file = f'token_{self.api_name}_{self.api_version}.pickle'
        pickle_path = os.path.join(token_dir, pickle_file)

        if not os.path.exists(token_dir):
            os.mkdir(token_dir)

        if os.path.exists(pickle_path):
            with open(pickle_path, 'rb') as token:
                cred = pickle.load(token)

        if not cred or not cred.valid:
            if cred and cred.expired and cred.refresh_token:
                cred.refresh(Request())
            else:
                flow = InstalledAppFlow.from_client_secrets_file(self.client_secret_file, self.scopes)
                # Use run_console instead of run_local_server for headless authentication
                cred = flow.run_console()

            with open(pickle_path, 'wb') as token:
                pickle.dump(cred, token)

        service = build(self.api_name, self.api_version, credentials=cred)
        return service

    def get_events(self, days=7, calendar_id='primary'):
        now = datetime.utcnow()
        end_of_day = datetime.combine(now.date(), datetime.max.time())
        end = end_of_day + timedelta(days=days)

        now_str = now.isoformat() + 'Z'
        end_str = end.isoformat() + 'Z'

        events_result = self.service.events().list(
            calendarId=calendar_id,
            timeMin=now_str,
            timeMax=end_str,
            singleEvents=True,
            orderBy='startTime'
        ).execute()

        events = events_result.get('items', [])
        return self.format_events(events)
    def format_events(self, events):
        final_list = []
        date_actual = None
        build_list = []

        for event in events:
            date_event = event['start'].get('dateTime', event['start'].get('date'))
            date_event = datetime.strptime(date_event, '%Y-%m-%dT%H:%M:%S%z').strftime('%Y-%m-%d %H:%M')
            summary = event.get('summary', 'Sans titre')

            event_dict = {
                "date": date_event,
                "summary": summary
            }

            if date_actual and date_event[0:10] == date_actual:
                build_list.append(event_dict)
            else:
                if build_list:
                    final_list.append({"date": date_actual, "events": build_list})
                build_list = [event_dict]
                date_actual = date_event[0:10]

        if build_list:
            final_list.append({"date": date_actual, "events": build_list})

        return final_list

if __name__ == '__main__':
    calendar = GoogleCalendar('./client_secret.json')
    events = calendar.get_events()
    print(json.dumps(events))  # Print the events in JSON format

