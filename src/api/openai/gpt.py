import openai

# Configuration de votre clé API d'OpenAI
openai.api_key = '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'

# Chemin du fichier pour sauvegarder le fil de la discussion
file_path = './discussion_history.txt'


class GPT:
    def __init__(self):
        self.session_messages = []

# Fonction pour ajuster l'historique des messages en fonction de la limite de tokens
    def adjust_messages_for_token_limit(self, new_user_input):
        total_tokens = len(new_user_input.split())  # Estimation initiale avec les mots
        messages_to_keep = []

        # Calculer le total de tokens à partir de la fin jusqu'à respecter la limite
        for message in reversed(self.session_messages):
            message_content = message["content"]
            message_tokens = len(message_content.split())
            if total_tokens + message_tokens > 4096:  # Limite de tokens pour GPT-3.5 Turbo
                break
            total_tokens += message_tokens
            messages_to_keep.insert(0, message)  # Ajouter le message au début de la nouvelle liste

        return messages_to_keep

    # Fonction pour interroger GPT-3.5 avec la nouvelle interface de l'API, en prenant en compte l'historique de la conversation
    def ask_gpt(self, prompt, discussion=False):
        # Ajuster l'historique des messages en fonction de la limite de tokens
        adjusted_messages = self.adjust_messages_for_token_limit(prompt)
        adjusted_messages.append({"role": "user", "content": prompt})  # Ajouter le nouveau message de l'utilisateur

        response = openai.chat.completions.create(
            model="gpt-3.5-turbo",
            messages=adjusted_messages,  # Utiliser l'historique ajusté des messages pour la requête
        )

        gpt_response = response.choices[0].message.content.strip()
        if discussion:
            add_to_discussion("assistant", gpt_response)  # Ajouter la réponse de GPT à la session
        # Mettre à jour session_messages avec la liste ajustée + la nouvelle réponse de GPT
        self.session_messages.clear()
        self.session_messages.extend(adjusted_messages)
        self.session_messages.append({"role": "assistant", "content": gpt_response})

        return gpt_response


# Fonction pour ajouter un message à la discussion et au fichier
def add_to_discussion(speaker, message):
    with open(file_path, 'a') as file:
        file.write(f"{speaker}: {message}\n")

# Fonction principale pour démarrer le chat
def chat(GPT):
    print("Bienvenue dans le chat GPT! Tapez 'quitter' pour sortir.")
    while True:
        user_input = input("Vous: ")
        if user_input.lower() == 'quitter':
            print("Fin de la conversation.")
            break
        gpt_response = GPT.ask_gpt(user_input, True)
        print("GPT:", gpt_response)


if __name__ == "__main__":
    chat(GPT())