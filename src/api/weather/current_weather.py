#!/usr/bin/env python3 
# -*- coding: utf-8 -*-
# ----------------------------------------------------------------------------
# Created By  : Pajak Alexandre
# Created Date: 01/10/2021
# version ='1.0'
# ---------------------------------------------------------------------------
""" Module for Manage OpenWeather Current Weather. """
import os
from dataclasses import dataclass
from datetime import datetime
from typing import Any
# ---------------------------------------------------------------------------
# Imports
# ---------------------------------------------------------------------------
from typing import List
import requests
import dotenv
from .direct_geocoding import Direct_Geocoding
import json

# global variables
dotenv.load_dotenv()
API_KEY = os.getenv( "API_KEY" )


@dataclass
class Clouds:
    """Class to manage Clouds data"""
    all: int

    @staticmethod
    def from_dict(obj: Any) -> "Clouds":
        _all = int(obj.get("all"))
        return Clouds(_all)


@dataclass
class Coord:
    """Class to manage Coord data"""
    lon: float
    lat: float

    @staticmethod
    def from_dict(obj: Any) -> "Coord":
        """Convert dict to Coord object"""
        _lon = float(obj.get("lon"))
        _lat = float(obj.get("lat"))
        return Coord(_lon, _lat)


@dataclass
class Main:
    """Class to manage Main data"""
    temp: float
    feels_like: float
    temp_min: float
    temp_max: float
    pressure: int
    humidity: int

    @staticmethod
    def from_dict(obj: Any) -> "Main":
        """Convert dict to Main object"""
        _temp = float(obj.get("temp"))
        _feels_like = float(obj.get("feels_like"))
        _temp_min = float(obj.get("temp_min"))
        _temp_max = float(obj.get("temp_max"))
        _pressure = int(obj.get("pressure"))
        _humidity = int(obj.get("humidity"))
        return Main(
            _temp,
            _feels_like,
            _temp_min,
            _temp_max,
            _pressure,
            _humidity,
        )


@dataclass
class Sys:
    """Class to manage Sys data"""
    type: int
    id: int
    country: str
    sunrise: int
    sunset: int

    @staticmethod
    def from_dict(obj: Any) -> "Sys":
        """Convert dict to Sys object"""
        _type = int(obj.get("type"))
        _id = int(obj.get("id"))
        _country = str(obj.get("country"))
        _sunrise = int(obj.get("sunrise"))
        _sunset = int(obj.get("sunset"))
        return Sys(_type, _id, _country, _sunrise, _sunset)


@dataclass
class Weather:
    """Class to manage Weather data"""
    id: int
    main: str
    description: str
    icon: str

    @staticmethod
    def from_dict(obj: Any) -> "Weather":
        """Convert dict to Weather object"""
        _id = int(obj.get("id"))
        _main = str(obj.get("main"))
        _description = str(obj.get("description"))
        _icon = str(obj.get("icon"))
        return Weather(_id, _main, _description, _icon)


@dataclass
class Wind:
    """Class to manage Wind data"""
    speed: float
    deg: int

    @staticmethod
    def from_dict(obj: Any) -> "Wind":
        """Convert dict to Wind object"""
        _speed = float(obj.get("speed"))
        _deg = int(obj.get("deg"))
        return Wind(_speed, _deg)


@dataclass
class Root:
    """Class to manage Root data"""
    coord: Coord
    weather: List[Weather]
    base: str
    main: Main
    visibility: int
    wind: Wind
    clouds: Clouds
    dt: int
    sys: Sys
    timezone: int
    id: int
    name: str
    cod: int

    @staticmethod
    def from_dict(obj: Any) -> "Root":
        """Convert dict to Root object"""
        _coord = Coord.from_dict(obj.get("coord"))
        _weather = [Weather.from_dict(y) for y in obj.get("weather")]
        _base = str(obj.get("base"))
        _main = Main.from_dict(obj.get("main"))
        _visibility = int(obj.get("visibility"))
        _wind = Wind.from_dict(obj.get("wind"))
        _clouds = Clouds.from_dict(obj.get("clouds"))
        _dt = int(obj.get("dt"))
        _sys = Sys.from_dict(obj.get("sys"))
        _timezone = int(obj.get("timezone"))
        _id = int(obj.get("id"))
        _name = str(obj.get("name"))
        _cod = int(obj.get("cod"))
        return Root(
            _coord,
            _weather,
            _base,
            _main,
            _visibility,
            _wind,
            _clouds,
            _dt,
            _sys,
            _timezone,
            _id,
            _name,
            _cod,
        )


class CurrentWeather:
    """Class to manage Current Weather"""
    weather: Root
    api_key = API_KEY
    city_name: str
    units: str
    lang: str

    def __init__(self, city_name, units= "metric", lang= "fr") -> None:
        """Constructor"""
        self.city_name = city_name
        self.units = units
        self.lang = lang
        self.weather = self.get_current_weather(city_name, units, lang)

    def get_current_weather(self, city_name="bordeaux", units="metric", lang="fr") -> Root:
        """Get current weather from city name"""
        city = Direct_Geocoding(city_name)
        url = f"https://api.openweathermap.org/data/2.5/weather?lat={city.get_Lat()}&lon={city.get_Lon()}&units={units}&lang={lang}&appid={self.api_key}"
        response = requests.get(url)
        if response.status_code == 200:
            return Root.from_dict(response.json())
        else:
            print("Error: ", response.status_code)
            return None

    def toString(self):
        """Print current weather"""
        print(f"La ville est {self.weather.name}")
        print(f"l'icon est {self.weather.weather[0].icon}")
        print(f"La temperature actuelle est de {self.weather.main.temp}°C")
        print(f"La temperature ressentie est de {self.weather.main.feels_like}°C")
        print(f"La temperature minimale est de {self.weather.main.temp_min}°C")
        print(f"La temperature maximale est de {self.weather.main.temp_max}°C")
        print(f"La pression est de {self.weather.main.pressure}hPa")
        print(f"L'humidité est de {self.weather.main.humidity}%")
        print(f"La vitesse du vent est de {self.weather.wind.speed}m/s")
        print(f"La direction du vent est de {self.weather.wind.deg}°")
        print(f"Le niveau de nuage est de {self.weather.clouds.all}%")
        print(f"Le pays est {self.weather.sys.country}")
        print(
            f"Le lever du soleil est à {datetime.fromtimestamp(self.weather.sys.sunrise).strftime('%H:%M:%S')}"
        )
        print(
            f"Le coucher du soleil est à {datetime.fromtimestamp(self.weather.sys.sunset).strftime('%H:%M:%S')}"
        )
        print(f"Le code de la ville est {self.weather.id}")
        print(f"Le code du pays est {self.weather.sys.id}")
        print(f"Le type du pays est {self.weather.sys.type}")
        print(
            f"La date de la requête est {datetime.fromtimestamp(self.weather.dt).strftime('%d/%m/%Y %H:%M:%S')}"
        )

    def to_json(self):
        """Serialize weather data to a JSON string."""
        weather_data = {
            "city_name": self.weather.name,
            "icon": self.weather.weather[0].icon,
            "temp": self.weather.main.temp,
            "feels_like": self.weather.main.feels_like,
            "temp_min": self.weather.main.temp_min,
            "temp_max": self.weather.main.temp_max,
            "pressure": self.weather.main.pressure,
            "humidity": self.weather.main.humidity,
            "wind_speed": self.weather.wind.speed,
            "wind_deg": self.weather.wind.deg,
            "clouds": self.weather.clouds.all,
            "country": self.weather.sys.country,
            "sunrise": datetime.fromtimestamp(self.weather.sys.sunrise).strftime('%H:%M:%S'),
            "sunset": datetime.fromtimestamp(self.weather.sys.sunset).strftime('%H:%M:%S'),
            "id": self.weather.id,
            "sys_id": self.weather.sys.id,
            "sys_type": self.weather.sys.type,
            "dt": datetime.fromtimestamp(self.weather.dt).strftime('%d/%m/%Y %H:%M:%S')
        }
        return json.dumps(weather_data)

    def get_weather(self):
        """Get weather"""
        return self.weather

    def get_icon(self):
        """Get icon"""
        return self.weather.weather[0].icon

    def get_city_name(self):
        """Get city name"""
        return self.city_name

    def get_units(self):
        """Get units methode"""
        return self.units

    def get_lang(self):
        """Get lang methode"""
        return self.lang

    def get_temp(self):
        """Get temperature"""
        return self.weather.main.temp

    def get_temp_min(self):
        return self.weather.main.temp_min

    def get_temp_max(self):
        """Get temperature max"""
        return self.weather.main.temp_max

    def get_feels_like(self):
        """Get temperature feels like"""
        return self.weather.main.feels_like

    def get_pressure(self):
        """Get pressure"""
        return self.weather.main.pressure

    def get_humidity(self):
        """Get humidity"""
        return self.weather.main.humidity

    def get_wind_speed(self):
        """Get wind speed"""
        return self.weather.wind.speed

    def get_wind_deg(self):
        """Get wind deg"""
        return self.weather.wind.deg

    def get_clouds(self):
        """Get clouds"""
        return self.weather.clouds.all

    def get_country(self):
        """Get country"""
        return self.weather.sys.country

    def get_sunrise(self):
        """Get sunrise time"""
        return self.weather.sys.sunrise

    def get_sunset(self):
        """Get sunset time"""
        return self.weather.sys.sunset

    def get_id(self):
        """Get id"""
        return self.weather.id

    def get_sys_id(self):
        """Get sys id"""
        return self.weather.sys.id

    def get_sys_type(self):
        """Get sys type"""
        return self.weather.sys.type

    def get_dt(self):
        """Get date time"""
        return self.weather.dt

# --------------------------------------------------------------
# Example Usage
#---------------------------------------------------------------
# if __name__ == "__main__":
#     currentWeather = CurrentWeather("bordeaux", "metric", "fr")
#     print(currentWeather.toString())
#     pressure = currentWeather.get_pressure()
#     print(pressure)
# --------------------------------------------------------------
