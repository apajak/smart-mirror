#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# ----------------------------------------------------------------------------
# Created By  : Pajak Alexandre
# Created Date: 01/10/2021
# version ='1.0'
# ---------------------------------------------------------------------------
""" Module for Manage all OpenWeather API. """
# ---------------------------------------------------------------------------
# Imports
# ---------------------------------------------------------------------------
from src.api.weather import current_weather, direct_geocoding,five_day_forecast,reverse_geocoding

class Weather:
    """Class to manage all OpenWeather API"""
    def get_CurrentWeather(self,city_name,unit="metric",language="fr"):
        """Get Current Weather from city name"""
        return current_weather.CurrentWeather(city_name,unit,language)

    def get_FiveDayForecast(self,cityName):
        """Get Five Day Forecast from city name"""
        return five_day_forecast.FiveDayForecast(cityName)

    def get_DirectGeocoding(self,cityName):
        """Get City data from his name."""
        return direct_geocoding.Direct_Geocoding(cityName)

    def get_ReverseGeocoding(self,lat, lon):
        """Get City data from his name."""
        return reverse_geocoding.Reverse_Geocoding(lat, lon)

# if __name__=="__main__":
#     print("Weather API")
#     weather = current_weather.CurrentWeather("Bordeaux")
#     weather.toString()
