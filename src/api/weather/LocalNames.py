#!/usr/bin/env python3 
# -*- coding: utf-8 -*-
#----------------------------------------------------------------------------
# Created By  : Pajak Alexandre
# Created Date: 01/10/2021
# version ='1.0'
# ---------------------------------------------------------------------------
""" Module for get Local Names from a city. """
# ---------------------------------------------------------------------------
# Imports
# ---------------------------------------------------------------------------
from typing import Any

class LocalNames:
    """Class to manage Local Names data"""
    af: str
    ar: str
    ascii: str
    az: str
    bg: str
    ca: str
    da: str
    de: str
    el: str
    en: str
    eu: str
    fa: str
    feature_name: str
    fi: str
    fr: str
    gl: str
    he: str
    hi: str
    hr: str
    hu: str
    id: str
    it: str
    ja: str
    la: str
    lt: str
    mk: str
    nl: str
    no: str
    pl: str
    pt: str
    ro: str
    ru: str
    sk: str
    sl: str
    sr: str
    th: str
    tr: str
    vi: str
    zu: str

    def __init__(
        self,
        af,
        ar,
        ascii,
        az,
        bg,
        ca,
        da,
        de,
        el,
        en,
        eu,
        fa,
        feature_name,
        fi,
        fr,
        gl,
        he,
        hi,
        hr,
        hu,
        id,
        it,
        ja,
        la,
        lt,
        mk,
        nl,
        no,
        pl,
        pt,
        ro,
        ru,
        sk,
        sl,
        sr,
        th,
        tr,
        vi,
        zu,
    ):
        """Constructor for LocalNames class"""
        self.af = af
        self.ar = ar
        self.ascii = ascii
        self.az = az
        self.bg = bg
        self.ca = ca
        self.da = da
        self.de = de
        self.el = el
        self.en = en
        self.eu = eu
        self.fa = fa
        self.feature_name = feature_name
        self.fi = fi
        self.fr = fr
        self.gl = gl
        self.he = he
        self.hi = hi
        self.hr = hr
        self.hu = hu
        self.id = id
        self.it = it
        self.ja = ja
        self.la = la
        self.lt = lt
        self.mk = mk
        self.nl = nl
        self.no = no
        self.pl = pl
        self.pt = pt
        self.ro = ro
        self.ru = ru
        self.sk = sk
        self.sl = sl
        self.sr = sr
        self.th = th
        self.tr = tr
        self.vi = vi
        self.zu = zu

    def from_dict(obj: Any) -> "LocalNames":
        """Constructor for LocalNames class from a dict"""
        _af = str(obj.get("af"))
        _ar = str(obj.get("ar"))
        _ascii = str(obj.get("ascii"))
        _az = str(obj.get("az"))
        _bg = str(obj.get("bg"))
        _ca = str(obj.get("ca"))
        _da = str(obj.get("da"))
        _de = str(obj.get("de"))
        _el = str(obj.get("el"))
        _en = str(obj.get("en"))
        _eu = str(obj.get("eu"))
        _fa = str(obj.get("fa"))
        _feature_name = str(obj.get("feature_name"))
        _fi = str(obj.get("fi"))
        _fr = str(obj.get("fr"))
        _gl = str(obj.get("gl"))
        _he = str(obj.get("he"))
        _hi = str(obj.get("hi"))
        _hr = str(obj.get("hr"))
        _hu = str(obj.get("hu"))
        _id = str(obj.get("id"))
        _it = str(obj.get("it"))
        _ja = str(obj.get("ja"))
        _la = str(obj.get("la"))
        _lt = str(obj.get("lt"))
        _mk = str(obj.get("mk"))
        _nl = str(obj.get("nl"))
        _no = str(obj.get("no"))
        _pl = str(obj.get("pl"))
        _pt = str(obj.get("pt"))
        _ro = str(obj.get("ro"))
        _ru = str(obj.get("ru"))
        _sk = str(obj.get("sk"))
        _sl = str(obj.get("sl"))
        _sr = str(obj.get("sr"))
        _th = str(obj.get("th"))
        _tr = str(obj.get("tr"))
        _vi = str(obj.get("vi"))
        _zu = str(obj.get("zu"))
        return LocalNames(
            _af,
            _ar,
            _ascii,
            _az,
            _bg,
            _ca,
            _da,
            _de,
            _el,
            _en,
            _eu,
            _fa,
            _feature_name,
            _fi,
            _fr,
            _gl,
            _he,
            _hi,
            _hr,
            _hu,
            _id,
            _it,
            _ja,
            _la,
            _lt,
            _mk,
            _nl,
            _no,
            _pl,
            _pt,
            _ro,
            _ru,
            _sk,
            _sl,
            _sr,
            _th,
            _tr,
            _vi,
            _zu,
        )
    def toString(self):
        """Returns a string representation of the class"""
        return str(self.__dict__)

    def get_LocalNames(self, lang: str):
        """Returns the local name for the given language"""
        if lang == "af":
            return self.af
        elif lang == "ar":
            return self.ar
        elif lang == "ascii":
            return self.ascii
        elif lang == "az":
            return self.az
        elif lang == "bg":
            return self.bg
        elif lang == "ca":
            return self.ca
        elif lang == "da":
            return self.da
        elif lang == "de":
            return self.de
        elif lang == "el":
            return self.el
        elif lang == "en":
            return self.en
        elif lang == "eu":
            return self.eu
        elif lang == "fa":
            return self.fa
        elif lang == "feature_name":
            return self.feature_name
        elif lang == "fi":
            return self.fi
        elif lang == "fr":
            return self.fr
        elif lang == "gl":
            return self.gl
        elif lang == "he":
            return self.he
        elif lang == "hi":
            return self.hi
        elif lang == "hr":
            return self.hr
        elif lang == "hu":
            return self.hu
        elif lang == "id":
            return self.id
        elif lang == "it":
            return self.it
        elif lang == "ja":
            return self.ja
        elif lang == "la":
            return self.la
        elif lang == "lt":
            return self.lt
        elif lang == "mk":
            return self.mk
        elif lang == "nl":
            return self.nl
        elif lang == "no":
            return self.no
        elif lang == "pl":
            return self.pl
        elif lang == "pt":
            return self.pt
        elif lang == "ro":
            return self.ro
        elif lang == "ru":
            return self.ru
        elif lang == "sk":
            return self.sk
        elif lang == "sl":
            return self.sl
        elif lang == "sr":
            return self.sr
        elif lang == "th":
            return self.th
        elif lang == "tr":
            return self.tr
        elif lang == "vi":
            return self.vi
        elif lang == "zu":
            return self.zu
        else:
            return self.en
