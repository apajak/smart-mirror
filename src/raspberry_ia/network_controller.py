import subprocess

class WiFiController:
    def __init__(self):
        self.check_nmcli()

    def check_nmcli(self):
        """ Vérifie si nmcli est installé """
        try:
            subprocess.run(["nmcli", "-v"], check=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            print("nmcli is available.")
        except subprocess.CalledProcessError:
            raise Exception("nmcli is not installed. Please install NetworkManager.")

    def enable_wifi(self):
        """ Active le WiFi """
        try:
            subprocess.run(["nmcli", "radio", "wifi", "on"], check=True)
            print("WiFi enabled successfully.")
        except subprocess.CalledProcessError as e:
            print("Failed to enable WiFi:", e)

    def disable_wifi(self):
        """ Désactive le WiFi """
        try:
            subprocess.run(["nmcli", "radio", "wifi", "off"], check=True)
            print("WiFi disabled successfully.")
        except subprocess.CalledProcessError as e:
            print("Failed to disable WiFi:", e)

    def wifi_status(self):
        """ Vérifie l'état actuel du WiFi et retourne True si activé, False sinon """
        result = subprocess.run(["nmcli", "radio", "wifi"], capture_output=True, text=True)
        # La sortie normale de "nmcli radio wifi" est soit "enabled" soit "disabled"
        if 'enabled' in result.stdout.strip():
            return True
        else:
            return False

# Exemple d'utilisation
# if __name__ == "__main__":
#     wifi_controller = WiFiController()
#     wifi_controller.enable_wifi()
#     # Attendre quelques secondes pour voir le WiFi activé
#     input("Press Enter to continue...")
#     wifi_controller.disable_wifi()
