import RPi.GPIO as GPIO
import time

# Déclaration de la broche GPIO utilisée pour contrôler le transistor
GPIO_pin = 26

# Configuration de la bibliothèque RPi.GPIO
GPIO.setmode(GPIO.BCM)
GPIO.setup(GPIO_pin, GPIO.OUT)

# Boucle principale
while True:
    # Allumage de la LED pendant 1 seconde
    GPIO.output(GPIO_pin, GPIO.HIGH)
    time.sleep(1)

    # Extinction de la LED pendant 1 seconde
    GPIO.output(GPIO_pin, GPIO.LOW)
    time.sleep(1)