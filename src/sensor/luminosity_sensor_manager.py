#!/usr/bin/python
#
#           bh1750.py
# Read data from a BH1750 digital light sensor.
#
# Author : Carelle KALA & Alexandre Pajak
# Date   : 07/02/2024
# Version: 2.0
#
# ---------------------------------------------------------------------
import smbus
import time

class LuminositySensor:
    def __init__(self, addr=0x23):
        self.addr = addr
        self.bus = smbus.SMBus(1)

    def convertToNumber(self, data):
        # Convertit les données brutes en un nombre représentant la luminosité
        result = (data[1] + (256 * data[0])) / 1.2
        return result

    def readLight(self):
        # Lit les données de luminosité via I2C
        data = self.bus.read_i2c_block_data(self.addr, 0x20)
        lightLevel = self.convertToNumber(data)
        # Arrondit le niveau de luminosité à deux décimales
        return round(lightLevel, 2)

# Exemple d'utilisation
# capteur = CapteurLuminosite()
# lightLevel = capteur.readLight()
# print("Niveau de luminosité:", lightLevel, "lx")
