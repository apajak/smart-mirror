# RASPBERRY pi 3b sensors Code:
## pinout of raspberry pi 3b
![pinout](./pics/GPIO-Pinout-Diagram.png)
## luminosity sensor
https://pinout.xyz/
https://www.raspberrypi-spy.co.uk/2015/03/bh1750fvi-i2c-digital-light-intensity-sensor/
https://raspberry-lab.fr/Composants/Capteur-presence-HC-SR501-Raspberry-Francais/
https://pimylifeup.com/raspberry-pi-dht11-sensor/
https://www.instructables.com/Raspberry-Pi-Tutorial-How-to-Use-a-RGB-LED/
````
vin: 3v
gnd: gnd
sda: GPIO 2 (BCM)
scl: GPIO 3 (BCM)
addr: None
````
[code](./luminosity_sensor_manager.py)
## proximity sensor
````
vin: 5v
gnd: gnd
out: GPIO 7 (BCM)
````
[code](./proximity_sensor_manager.py)

## temperature sensor
````
vin: 3v
gnd: gnd
sda: GPIO 17 (BCM)

NOTE: 10k pull-up resistor between 3v and sda
````

## Led
````
vin: 3v (long leg)
left: GPIO 14 (BCM) (short leg)
midle: GPIO 15 (BCM) (short leg)
right: GPIO 18 (BCM) (short leg)

NOTE: 220 ohm resistor between GPIO and short leg
````