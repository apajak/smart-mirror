#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# ----------------------------------------------------------------------------
# Created By  : BOUFFANAIS Hugo, Pajak Alexandre
# Created Date: 10/04/2023
# version ='1.0'
# ---------------------------------------------------------------------------
import RPi.GPIO as GPIO
import time
import threading

class ProximitySensor:
    value = None
    sleep = False
    isRunning = False

    def __init__(self, pin: int = 7, sleep_time: int = 10):
        self.pin = pin
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(self.pin, GPIO.IN)
        self.sleep_time = sleep_time

    def read_proximity(self):
        print(f"Reading Proximity Sensor {GPIO.input(self.pin)}")
        return GPIO.input(self.pin)

    def start(self):
        self.isRunning = True
        print("Start Proximity Sensor")
        counter = 0
        while self.isRunning:
            if self.read_proximity():
                print("Someone is here. waking up")
                self.value = 1
                self.sleep = False
                counter = 0
            else:
                self.value = 0
                counter += 1
                print(f"remaining time: {self.sleep_time - counter}")
                if counter >= self.sleep_time:
                    print("No one is here. going to sleep mode")
                    self.sleep = True
                    counter = 0
            time.sleep(1)

    def threaded_start(self):
        self.thread = threading.Thread(target=self.start)
        self.thread.start()

    def stop(self):
        self.isRunning = False
        print("Stop Proximity Sensor")

    def __del__(self):
        GPIO.cleanup()

#
# TEST:
def main():
    print("Proximity Sensor Test (CTRL+C to exit)")
    proximity_sensor = ProximitySensor()
    proximity_sensor.threaded_start()
    try:
        while True:
            print(f"Proximity: {str(proximity_sensor.value)}")
            time.sleep(1)
    except KeyboardInterrupt:
        proximity_sensor.stop()

if __name__ == "__main__":
    main()