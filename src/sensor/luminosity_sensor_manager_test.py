#!/usr/bin/python
#
#           bh1750_test.py
# Emulate data from a BH1750 digital light sensor.
#
# Author : Alexandre Pajak
# Date   : 19/05/2024
# Version: 2.0
#
# ---------------------------------------------------------------------
import random
import time

class LuminositySensor:
    def __init__(self, addr=0x23):
        self.addr = addr
        # Normally would initialize the bus here, but not needed for simulation
        self.bus = None

    def convertToNumber(self, data):
        # Convertit les données brutes en un nombre représentant la luminosité
        result = (data[1] + (256 * data[0])) / 1.2
        return result

    def readLight(self):
        # Generate fake data to simulate reading from the sensor
        # Let's say the sensor typically reads values between 0 and 54612 raw units
        # Simulating a random luminosity level within a reasonable range
        simulated_raw_data = random.randint(0, 500)
        # Round the level of luminosity to two decimals to emulate the real sensor's precision
        return round(simulated_raw_data, 2)

# Example usage:
# sensor = LuminositySensor()
# light_level = sensor.readLight()
# print("Simulated luminosity level:", light_level, "lx")
