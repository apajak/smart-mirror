import threading
from gpiozero import RGBLED
from gpiozero.pins.rpigpio import RPiGPIOFactory
from time import sleep

class LEDController(threading.Thread):
    def __init__(self, led, initial_color=(0, 0, 0)):
        super().__init__()
        self.led = led
        self.color = initial_color
        self.running = False

    def run(self):
        self.running = True
        while self.running:
            self.led.color = self.color
            sleep(0.1)

    def change_color(self, color):
        self.color = color

    def stop(self):
        self.running = False
        self.led.off()

class RGBLEDManager:
    def __init__(self):
        pin_factory = RPiGPIOFactory()
        self.leds = {
            "internet": RGBLED(red=17, green=27, blue=22, pin_factory=pin_factory),
            "camera": RGBLED(red=10, green=9, blue=11, pin_factory=pin_factory),
            "microphone": RGBLED(red=5, green=6, blue=13, pin_factory=pin_factory),
            "activity": RGBLED(red=19, green=26, blue=21, pin_factory=pin_factory)
        }
        self.controllers = {
            "internet": LEDController(self.leds["internet"], (0, 0, 1)),
            "camera": LEDController(self.leds["camera"], (0, 1, 0)),
            "microphone": LEDController(self.leds["microphone"], (1, 0.5, 0)),
            "activity": LEDController(self.leds["activity"], (0, 1, 0))
        }
        for controller in self.controllers.values():
            controller.start()

    def set_color(self, name, color):
        if name in self.controllers:
            self.controllers[name].change_color(color)
        else:
            print(f"LED '{name}' n'existe pas.")

    def set_activity_color(self, color):
        if color == "red":
            self.controllers["activity"].change_color((1, 0, 0))
        elif color == "green":
            self.controllers["activity"].change_color((0, 1, 0))
        else:
            print("Invalid color for activity LED. Choose 'red' or 'green'.")

    def off(self, name):
        if name in self.controllers:
            self.controllers[name].stop()
        else:
            print(f"LED '{name}' n'existe pas.")

    def all_off(self):
        for controller in self.controllers.values():
            controller.stop()

# Utilisation de la classe
if __name__ == "__main__":
    led_manager = RGBLEDManager()

    # Modifier la couleur de la LED d'internet en bleu
    led_manager.set_color("internet", (0, 0, 1))
    sleep(1)

    # Changer la couleur de la LED d'activité en rouge
    led_manager.set_activity_color("red")
    sleep(1)

    # Éteindre toutes les LEDs
    led_manager.all_off()
