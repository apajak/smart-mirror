import adafruit_dht
import board
import time
class Temperature:
    def __init__(self):
        self.sensor = adafruit_dht.DHT11(board.D4)

    def get_temperature(self):
        temperature = self.sensor.temperature
        return temperature

    def get_humidity(self):
        humidity = self.sensor.humidity
        return humidity


if __name__ == "__main__":
    dh11 = Temperature()
    while True:
        print(f"Temperature: {dh11.get_temperature()}")
        print(f"Humidity: {dh11.get_humidity()}")
        time.sleep(1)